// Copyright Epic Games, Inc. All Rights Reserved.

#include "ShipyardKeeper.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ShipyardKeeper, "ShipyardKeeper" );

DEFINE_LOG_CATEGORY(LogShipyardKeeper)
 