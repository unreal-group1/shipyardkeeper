// Fill out your copyright notice in the Description page of Project Settings.


#include "Animations/SK_AnimNotify.h"

void USK_AnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	OnNotifiedFTypeSignature.Broadcast(MeshComp);

	Super::Notify(MeshComp, Animation);
}
