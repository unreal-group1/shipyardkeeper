// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "SK_ContinueIfPossible.generated.h"


UCLASS()
class SHIPYARDKEEPER_API USK_ContinueIfPossible : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	USK_ContinueIfPossible();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
