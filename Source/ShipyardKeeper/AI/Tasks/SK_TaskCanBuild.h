// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "SK_TaskCanBuild.generated.h"

UCLASS()
class SHIPYARDKEEPER_API USK_TaskCanBuild : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	USK_TaskCanBuild();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FBlackboardKeySelector CanBuildKey;
};
