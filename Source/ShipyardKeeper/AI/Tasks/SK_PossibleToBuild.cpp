// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Tasks/SK_PossibleToBuild.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "SK_Slipway.h"
#include "Managers/SK_ShipBuilder.h"
#include "SK_GameMode.h"

USK_PossibleToBuild::USK_PossibleToBuild()
{
	NodeName = "PossibleToBuild";
}

EBTNodeResult::Type USK_PossibleToBuild::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const auto BlackBoard = OwnerComp.GetBlackboardComponent();

	const UWorld* World = GetWorld();
	if (!World )return EBTNodeResult::Aborted;

	const auto GM = World->GetAuthGameMode<ASK_GameMode>();
	if (!GM) return EBTNodeResult::Aborted;

	const auto& BuildMeneger = GM->GetShipBuilder();

	FVector WorkLocation;
	TObjectPtr<ASK_Slipway> SlipwayActor;
	int32 Index = INDEX_NONE;

	if (BuildMeneger->TryGetWork(WorkLocation, Index, SlipwayActor))
	{
		BlackBoard->SetValueAsVector(WorkLocationKey.SelectedKeyName, WorkLocation);
		BlackBoard->SetValueAsInt(WorkplaceIndexKey.SelectedKeyName, Index);
		BlackBoard->SetValueAsObject(SlipwayActorKey.SelectedKeyName, SlipwayActor);
		BlackBoard->SetValueAsBool(CanBuildKey.SelectedKeyName, true);
		return EBTNodeResult::Succeeded;
	}
	BlackBoard->SetValueAsBool(CanBuildKey.SelectedKeyName, false);
	return EBTNodeResult::Succeeded;
}
