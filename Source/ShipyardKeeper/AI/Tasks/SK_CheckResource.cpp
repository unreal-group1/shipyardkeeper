// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Tasks/SK_CheckResource.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Managers/SK_ShipBuilder.h"
#include "SK_GameMode.h"
#include "SK_PlatformGameInstance.h"
#include "Actors/SK_MachineActorBase.h"
#include "AI/Pawn/SK_AIPawnBase.h"

USK_CheckResource::USK_CheckResource()
{
	NodeName = "TryGetRightResource";
}

EBTNodeResult::Type USK_CheckResource::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const auto BlackBoard = OwnerComp.GetBlackboardComponent();

	const auto World = GetWorld();
	if (World)
	{
		const auto GM = World->GetAuthGameMode<ASK_GameMode>();
		const auto GI = World->GetGameInstance<USK_PlatformGameInstance>();
		if (GM && GI)
		{
			const auto BuildManager = GM->GetShipBuilder();
			if (BuildManager)
			{
				const TArray<FResource>& RequiredResources = BuildManager->GetRequiredResourse();

				for (const auto& Resourse : RequiredResources)
				{
					if (!GI->IsEnough(Resourse.ResourcesRowName, Resourse.ResourceQuantity))
					{
						const auto& MachineActors = GM->GetMachineActors();
						for (const auto& ResourseName : GM->GetSelectionPriority())
						{
							const auto& Machines = MachineActors.FindByPredicate([&](const FMachineTypes& Types)
								{
									return Types.OutResourceRowName == ResourseName;

								});

							if (Machines)
							{
								FVector Location;
								for (const auto& Machine : Machines->MachineActors)
								{
									if (Machine->CanWork(Location))
									{
										const auto Pawn = Cast<ASK_AIPawnBase>(BlackBoard->GetValueAsObject(SelfActorKey.SelectedKeyName));
										if(Pawn) Machine->UpdateVacancy(false, Pawn);
										BlackBoard->SetValueAsVector(WorkLocationKey.SelectedKeyName, Location);
										BlackBoard->SetValueAsBool(CanGetRightResourceKey.SelectedKeyName, true);
										BlackBoard->SetValueAsObject(FoundMachineKey.SelectedKeyName, Machine);
										return EBTNodeResult::Succeeded;
									}

								}
							}
						}
					}
				}
			}
		}
	}

	BlackBoard->SetValueAsBool(CanGetRightResourceKey.SelectedKeyName, false);
	return EBTNodeResult::Succeeded;
}
