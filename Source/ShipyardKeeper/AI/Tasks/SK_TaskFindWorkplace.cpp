// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Tasks/SK_TaskFindWorkplace.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "SK_GameMode.h"
#include "Actors/SK_MachineActorBase.h"
#include "AI/Pawn/SK_AIPawnBase.h"

USK_TaskFindWorkplace::USK_TaskFindWorkplace()
{
	NodeName = "FindWorkplace";
}

EBTNodeResult::Type USK_TaskFindWorkplace::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const auto BlackBoard = OwnerComp.GetBlackboardComponent();

	const UWorld* World = GetWorld();
	if (World)
	{
		const auto GM = World->GetAuthGameMode<ASK_GameMode>();
		if (GM)
		{
			//CheckResourse
			const auto& MachineActors = GM->GetMachineActors();
			for (const auto& ResourseName : GM->GetSelectionPriority())
			{
				const auto& Machines = MachineActors.FindByPredicate([&](const FMachineTypes& Types)
					{
						return Types.OutResourceRowName == ResourseName;

					});

				if (Machines)
				{
					FVector Location;
					for (const auto& Machine : Machines->MachineActors)
					{
						if (Machine->CanWork(Location))
						{
							const auto Pawn = Cast<ASK_AIPawnBase>(BlackBoard->GetValueAsObject(SelfActorKey.SelectedKeyName));
							if (Pawn) Machine->UpdateVacancy(false, Pawn);
							BlackBoard->SetValueAsVector(WorkLocationKey.SelectedKeyName, Location);
							BlackBoard->SetValueAsBool(CanWorkKey.SelectedKeyName, true);
							BlackBoard->SetValueAsObject(FoundMachineKey.SelectedKeyName, Machine);
							return EBTNodeResult::Succeeded;
						}

					}
				}
				
			}
			BlackBoard->SetValueAsBool(CanWorkKey.SelectedKeyName, false);
		}
	}

	return EBTNodeResult::Type();
}