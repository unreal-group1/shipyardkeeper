// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Tasks/SK_TaskCanBuild.h"
#include "SK_GameMode.h"
#include "SK_Types.h"
#include "SK_PlatformGameInstance.h"
#include "Managers/SK_ShipBuilder.h"
#include "BehaviorTree/BlackboardComponent.h"

USK_TaskCanBuild::USK_TaskCanBuild()
{
	NodeName = "CanBuild";
}

EBTNodeResult::Type USK_TaskCanBuild::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const auto BlackBoard = OwnerComp.GetBlackboardComponent();

	const UWorld* World = GetWorld();
	if (World)
	{
		const auto GM = World->GetAuthGameMode<ASK_GameMode>();
		if (!GM) return EBTNodeResult::Aborted;

		const auto GI = World->GetGameInstance<USK_PlatformGameInstance>();
		if (!GI) return EBTNodeResult::Aborted;

		const auto& BuildManager = GM->GetShipBuilder();

		if (!BuildManager->HaveTask())
		{
			BlackBoard->SetValueAsBool(CanBuildKey.SelectedKeyName, false);
			return EBTNodeResult::Succeeded;
		}

		const TArray<FResource>& RequiredResources = BuildManager->GetRequiredResourse();

		for (const auto& Resourse : RequiredResources)
		{
			if (!GI->IsEnough(Resourse.ResourcesRowName, Resourse.ResourceQuantity))
			{
				BlackBoard->SetValueAsBool(CanBuildKey.SelectedKeyName, false);
				return EBTNodeResult::Succeeded;
			}
		}

		BlackBoard->SetValueAsBool(CanBuildKey.SelectedKeyName, true);
		return EBTNodeResult::Succeeded;
	}
	return EBTNodeResult::Type();
}
