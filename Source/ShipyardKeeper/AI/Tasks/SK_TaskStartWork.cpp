// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Tasks/SK_TaskStartWork.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Actors/SK_MachineActorBase.h"
#include "AI/Pawn/SK_AIPawnBase.h"

USK_TaskStartWork::USK_TaskStartWork()
{
	NodeName = "StartWork";
}

EBTNodeResult::Type USK_TaskStartWork::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const auto BlackBoard = OwnerComp.GetBlackboardComponent();
	const auto MachineActor = Cast<ASK_MachineActorBase>(BlackBoard->GetValueAsObject(FoundMachineKey.SelectedKeyName));
	auto SelfPawn = Cast<ASK_AIPawnBase>(BlackBoard->GetValueAsObject(SelfActorKey.SelectedKeyName));

	if (SelfPawn && MachineActor)
	{
		SelfPawn->SetIsWorking(true);
		SelfPawn->StartWork(MachineActor);
	}

	return EBTNodeResult::Succeeded;
}
