// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Tasks/SK_ContinueIfPossible.h"

USK_ContinueIfPossible::USK_ContinueIfPossible()
{
	NodeName = "ContinueWorking";
}

EBTNodeResult::Type USK_ContinueIfPossible::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	return EBTNodeResult::InProgress;
}
