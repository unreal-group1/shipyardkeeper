// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Tasks/SK_TaskStopWorking.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Actors/SK_MachineActorBase.h"
#include "AI/Pawn/SK_AIPawnBase.h"

USK_TaskStopWorking::USK_TaskStopWorking()
{
	NodeName = "StopWork";
}

EBTNodeResult::Type USK_TaskStopWorking::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const auto BlackBoard = OwnerComp.GetBlackboardComponent();
	const auto MachineActor = Cast<ASK_MachineActorBase>(BlackBoard->GetValueAsObject(FoundMachineKey.SelectedKeyName));
	auto SelfPawn = Cast<ASK_AIPawnBase>(BlackBoard->GetValueAsObject(SelfActorKey.SelectedKeyName));

	if (MachineActor && SelfPawn)
	{
		SelfPawn->SetIsWorking(false);
	}

	return EBTNodeResult::InProgress;
}
