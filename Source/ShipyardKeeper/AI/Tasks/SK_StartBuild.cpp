// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Tasks/SK_StartBuild.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AI/Pawn/SK_AIPawnBase.h"

USK_StartBuild::USK_StartBuild()
{
	NodeName = "StartBuild";
}

EBTNodeResult::Type USK_StartBuild::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const auto BlackBoard = OwnerComp.GetBlackboardComponent();
	const auto NPCPawn = Cast<ASK_AIPawnBase>(BlackBoard->GetValueAsObject(SelfActorKey.SelectedKeyName));
	if (NPCPawn)
	{
		NPCPawn->StartBuild();
	}

	return EBTNodeResult::InProgress;
}
