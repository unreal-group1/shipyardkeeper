// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "SK_StartBuild.generated.h"

UCLASS()
class SHIPYARDKEEPER_API USK_StartBuild : public UBTTaskNode
{
	GENERATED_BODY()

public:
	USK_StartBuild();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FBlackboardKeySelector SelfActorKey;
};
