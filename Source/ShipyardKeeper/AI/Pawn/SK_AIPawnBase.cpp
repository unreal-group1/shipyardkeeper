// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Pawn/SK_AIPawnBase.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Managers/SK_ShipBuilder.h"
#include "Components/SceneComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/Character.h"

#include "SK_GameMode.h"
#include "SK_Slipway.h"
#include "Actors/SK_MachineActorBase.h"
#include "SK_PlatformGameInstance.h"

#include "SK_DisplayPopupWidgetComponent.h"
#include "SK_PopupHintWidgetComponent.h"

#include "SKW_PopupWithProgressBarSimulate.h"
#include "SKW_NPSWorkProgressBar.h"

DEFINE_LOG_CATEGORY_STATIC(ASK_AIPawnBaseLog, All, All);

ASK_AIPawnBase::ASK_AIPawnBase()
{
	PrimaryActorTick.bCanEverTick = false;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	if (SceneComponent)
	{
		SetRootComponent(SceneComponent);
	}

	SkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshComponent"));
	SkeletalMeshComponent->SetupAttachment(SceneComponent);

	DisplayPopupWidgetComponent = CreateDefaultSubobject<USK_DisplayPopupWidgetComponent>(TEXT("DisplayPopupWidgetComponent"));
	if (DisplayPopupWidgetComponent)
	{
		DisplayPopupWidgetComponent->AttachToComponent(SceneComponent,
			FAttachmentTransformRules::KeepRelativeTransform);
	}

	ProgressBarWidgetComponent = CreateDefaultSubobject<USK_PopupHintWidgetComponent>(TEXT("ProgressBarWidgetComponent"));
	if (ProgressBarWidgetComponent)
	{
		ProgressBarWidgetComponent->AttachToComponent(SceneComponent,
			FAttachmentTransformRules::KeepRelativeTransform);
	}
}

void ASK_AIPawnBase::StartBuild()
{
	GetWorldTimerManager().SetTimer(BuildTickHandle, this, &ASK_AIPawnBase::BuildTick, 1.f, false);
	PlayMontage(SlipwayAnimMontage);
	SetActorRotation(FRotator(0, -90.0, 0));

}

void ASK_AIPawnBase::StartWork(const TObjectPtr<ASK_MachineActorBase>& InMachineActor)
{
	MachineActor = InMachineActor;
	if (MachineActor) MachineActor->UpdateIsAtTheWorkplace(true);

	SetActorRotation(FRotator(0.0, 180.0, 0.0));
	SetActorLocation(MachineActor->GetWorkLocation());
	InitWorkTick();
}

void ASK_AIPawnBase::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld())
	{
		const auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
		if (!GM) return;

		ShipBuilder = GM->GetShipBuilder();
	}

	if (ProgressBarWidgetComponent)
	{
		if (ProgressBarClass)
		{
			ProgressBarWidgetComponent->SetWidgetClass(ProgressBarClass);
		}
	}

}

void ASK_AIPawnBase::BuildTick()
{
	if (!ShipBuilder->IsPossibleToBuild() || ShipBuilder->IsBuildComplited())
	{
		FirstDelay_Build();
	}
	else
	{
		ShipBuilder->AddEndurance();
		if (!ShipBuilder->IsBuildComplited())
		{
			StartBuild();
		}
		else
		{
			FirstDelay_Build();
		}
	}
}

void ASK_AIPawnBase::InitWorkTick()
{
	if (!IsWorking)
	{
		const auto AIController = GetController<AAIController>();
		if (AIController)
		{
			MachineActor->UpdateVacancy(true, nullptr);
			if (MachineActor) MachineActor->UpdateIsAtTheWorkplace(false);
			AIController->GetBlackboardComponent()->SetValueAsBool(FName("HaveWork"), false);
			AIController->GetBlackboardComponent()->SetValueAsBool(FName("CanGetRResource"), false);
			return;
		}
	}

	auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	const auto& Info = MachineActor->GetLevelInfo();
	WorkFrequency = Info.CreateProductionTime;

	if (GI 
		&& GI->TrySpendResource(MachineActor->GetResourseInName(), Info.QuantityResourceIn))
	{
		GetWorldTimerManager().SetTimer(WorkTickHandle, this, &ASK_AIPawnBase::WorkTick, WorkFrequency, false);
		PlayAnim();
		MachineActor->PlayAnim();

		if (ProgressBarWidgetComponent
			&& MachineActor)
		{
			auto ResourceOutName = MachineActor->GetResourseOutName();
			auto Resource = GI->GetResourceTypeByName(ResourceOutName);

			if (Resource)
			{
				ProgressBarWidgetComponent->SetWidgetClass(ProgressBarClass,
					Resource->ShowingData.Image);

				auto ProgressBarSimulate = Cast<USKW_PopupWithProgressBarSimulate>(ProgressBarWidgetComponent->GetWidget());
				if (ProgressBarSimulate)
				{
					ProgressBarSimulate->SetImageFromTexture(Resource->ShowingData.Image);

					ProgressBarSimulate->SetTimeSimulate(WorkFrequency);
					ProgressBarSimulate->StartSimulate();
				}
			}	
			ProgressBarWidgetComponent->Show();
		}
	}
	else
	{
		GetWorldTimerManager().SetTimer(FirstDelayWorkHandle, this, &ASK_AIPawnBase::FirstDelay_Work, FirstDelayWork, false);
	}
}

void ASK_AIPawnBase::WorkTick()
{
	auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	if (GI)
	{
		const auto& Info = MachineActor->GetLevelInfo();

		MachineActor->CreateProducts();

		UE_LOG(ASK_AIPawnBaseLog, Display, TEXT("Create %f %s from %f %s"), Info.QuantityResourceOut, *MachineActor->GetResourseOutName().ToString(),
			Info.QuantityResourceIn, *MachineActor->GetResourseInName().ToString());

		if (ProgressBarWidgetComponent)
		{
			ProgressBarWidgetComponent->Hide();

			auto ProgressBarSimulate = Cast<USKW_PopupWithProgressBarSimulate>(ProgressBarWidgetComponent->GetWidget());
			if (ProgressBarSimulate)
			{
				ProgressBarSimulate->StopSimulate();
			}
		}

		if (DisplayPopupWidgetComponent)
		{
			auto ResourceType = GI->GetResourceTypeByName(MachineActor->GetResourseOutName());
			if (ResourceType)
			{
				DisplayPopupWidgetComponent->SetImageAndText(
					ResourceType->ShowingData.Image,
					FString::Printf(TEXT("%.0f"), 
						Info.QuantityResourceOut));
			}

			DisplayPopupWidgetComponent->Show();
		}

		GetWorldTimerManager().SetTimer(FirstDelayWorkHandle, this, &ASK_AIPawnBase::FirstDelay_Work, FirstDelayWork, false);
	}
}

void ASK_AIPawnBase::FirstDelay_Work()
{
	if (!IsWorking)
	{
		const auto AIController = GetController<AAIController>();
		if (AIController)
		{
			MachineActor->UpdateVacancy(true, nullptr);
			if (MachineActor) MachineActor->UpdateIsAtTheWorkplace(false);
			AIController->GetBlackboardComponent()->SetValueAsBool(FName("HaveWork"), false);
			AIController->GetBlackboardComponent()->SetValueAsBool(FName("CanGetRResource"), false);
			return;
		}
	}

	auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	const auto& Info = MachineActor->GetLevelInfo();
	WorkFrequency = Info.CreateProductionTime;

	if (GI && GI->IsEnough(MachineActor->GetResourseInName(), Info.QuantityResourceIn))
	{
		InitWorkTick();
	}
	else
	{
		if (HaveAnotherWork())
		{
			const auto AIController = GetController<AAIController>();
			if (AIController)
			{
				MachineActor->UpdateVacancy(true, nullptr);
				if (MachineActor) MachineActor->UpdateIsAtTheWorkplace(false);
				AIController->GetBlackboardComponent()->SetValueAsBool(FName("HaveWork"), false);
				AIController->GetBlackboardComponent()->SetValueAsBool(FName("CanGetRResource"), false);
				return;
			}
		}
		else
		{
			GetWorldTimerManager().SetTimer(FirstDelayWorkHandle, this, &ASK_AIPawnBase::SecondDelay_Work, SecondDelayWork, false);
		}

	}
}

void ASK_AIPawnBase::SecondDelay_Work()
{
	if (!IsWorking)
	{
		const auto AIController = GetController<AAIController>();
		if (AIController)
		{
			MachineActor->UpdateVacancy(true,nullptr);
			if (MachineActor) MachineActor->UpdateIsAtTheWorkplace(false);
			AIController->GetBlackboardComponent()->SetValueAsBool(FName("HaveWork"), false);
			AIController->GetBlackboardComponent()->SetValueAsBool(FName("CanGetRResource"), false);
			return;
		}
	}

	auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	const auto& Info = MachineActor->GetLevelInfo();
	WorkFrequency = Info.CreateProductionTime;

	if (GI && GI->IsEnough(MachineActor->GetResourseInName(), Info.QuantityResourceIn))
	{
		InitWorkTick();
	}
	else
	{
		if (HaveAnotherWork())
		{
			const auto AIController = GetController<AAIController>();
			if (AIController)
			{
				MachineActor->UpdateVacancy(true,nullptr);
				if (MachineActor) MachineActor->UpdateIsAtTheWorkplace(false);
				AIController->GetBlackboardComponent()->SetValueAsBool(FName("HaveWork"), false);
				AIController->GetBlackboardComponent()->SetValueAsBool(FName("CanGetRResource"), false);
				return;
			}
		}
		else
		{
			GetWorldTimerManager().SetTimer(FirstDelayWorkHandle, this, &ASK_AIPawnBase::SecondDelay_Work, SecondDelayWork, false);
		}
	
	}
}

void ASK_AIPawnBase::FirstDelay_Build()
{
	GetWorldTimerManager().SetTimer(FirstDelayBuildHandle, this, &ASK_AIPawnBase::SecondDelay_Build, FirstDelayBuild, false);
}

void ASK_AIPawnBase::SecondDelay_Build()
{
	if (!ShipBuilder->IsPossibleToBuild() || ShipBuilder->IsBuildComplited())
	{
		if (HaveAnotherWork())
		{
			const auto AIController = GetController<AAIController>();
			if (AIController)
			{
				AIController->GetBlackboardComponent()->SetValueAsBool(FName("CanBuild"), false);

				const uint32 Index = AIController->GetBlackboardComponent()->GetValueAsInt(FName("WorkplaceIndex"));
				const auto SlipwayActor = Cast<ASK_Slipway>(AIController->GetBlackboardComponent()->GetValueAsObject(FName("SlipwayActor")));

				ShipBuilder->ClearWorkplace(SlipwayActor, Index);
			}
		}
		else
		{
			GetWorldTimerManager().SetTimer(SecondDelayBuildHandle, this, &ASK_AIPawnBase::SecondDelay_Build, SecondDelayBuild, false);
		}
	}
	else
	{
		GetWorldTimerManager().SetTimer(SecondDelayBuildHandle, this, &ASK_AIPawnBase::StartBuild, SecondDelayBuild, false);
	}
	
}

bool ASK_AIPawnBase::HaveAnotherWork()
{
	const auto World = GetWorld();
	if (World)
	{
		const auto GM = World->GetAuthGameMode<ASK_GameMode>();
		const auto GI = World->GetGameInstance<USK_PlatformGameInstance>();
		const auto BuildManager = GM->GetShipBuilder();
		if (GM && GI)
		{
			const auto& MachineActors = GM->GetMachineActors();
			
			if (BuildManager)
			{
				const TArray<FResource>& RequiredResources = BuildManager->GetRequiredResourse();
				for (const auto& Resourse : RequiredResources)
				{
					if (!GI->IsEnough(Resourse.ResourcesRowName, Resourse.ResourceQuantity))
					{

						for (const auto& ResourseName : GM->GetSelectionPriority())
						{
							const auto& Machines = MachineActors.FindByPredicate([&](const FMachineTypes& Types)
								{
									return Types.OutResourceRowName == ResourseName;

								});

							if (Machines)
							{
								FVector Location;
								for (const auto& Machine : Machines->MachineActors)
								{
									if (Machine->CanWork(Location))
									{
										return true;
									}

								}
							}
						}

					}
				}
				for (const auto& ResourseName : GM->GetSelectionPriority())
				{
					const auto& Machines = MachineActors.FindByPredicate([&](const FMachineTypes& Types)
						{
							return Types.OutResourceRowName == ResourseName;

						});

					if (Machines)
					{
						FVector Location;
						for (const auto& Machine : Machines->MachineActors)
						{
							if (Machine->CanWork(Location))
							{
								return true;
							}

						}
					}
				}

				if (!BuildManager->HaveTask())
				{
					return false;
				}

				for (const auto& Resourse : RequiredResources)
				{
					if (!GI->IsEnough(Resourse.ResourcesRowName, Resourse.ResourceQuantity))
					{
						return false;
					}
				}

				if (BuildManager->IsCanWork())
				{
					return true;
				}
			}
		}
	}
	return false;
}

void ASK_AIPawnBase::PlayAnim()
{
	const auto AnimInfo = MachineActor->GetAnimInfo();
	PlayMontage(AnimInfo.StartPawnAnimMontage);

	GetWorldTimerManager().SetTimer(AnimTimerHandle, this, &ASK_AIPawnBase::AnimTick, 1.f, true);

	NumOfLoop = WorkFrequency - 2.f;
	CurrentCycle = 0;
}

void ASK_AIPawnBase::AnimTick()
{
	const auto AnimInfo = MachineActor->GetAnimInfo();
	PlayMontage(AnimInfo.RepeatPawnAnimMontage);

	CurrentCycle++;
	if (CurrentCycle >= NumOfLoop)
	{
		GetWorldTimerManager().ClearTimer(AnimTimerHandle);
		GetWorldTimerManager().SetTimer(FinishAnimTimerHandle, this, &ASK_AIPawnBase::AnimEnd, 1.f, false);
	}
}

void ASK_AIPawnBase::AnimEnd()
{
	const auto AnimInfo = MachineActor->GetAnimInfo();
	PlayMontage(AnimInfo.FinishPawnAnimMontage);
}

void ASK_AIPawnBase::PlayMontage(const TObjectPtr<UAnimMontage>& Montage)
{
	const auto AnimInstance = SkeletalMeshComponent->GetAnimInstance();
	if (AnimInstance)
	{
		AnimInstance->Montage_Play(Montage);
	}
}
