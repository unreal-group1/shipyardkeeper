// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SK_AIPawnBase.generated.h"

class UBehaviorTree;
class UAnimMontage;
class USK_ShipBuilder;
class ASK_MachineActorBase;
class USkeletalMeshComponent;

class USK_PopupHintWidgetComponent;
class USK_DisplayPopupWidgetComponent;
class USKW_NPSWorkProgressBar;

UCLASS()
class SHIPYARDKEEPER_API ASK_AIPawnBase : public APawn
{
	GENERATED_BODY()

public:
	ASK_AIPawnBase();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
	USceneComponent* SceneComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
	UBehaviorTree* BehaviorTreeAsset;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	USK_DisplayPopupWidgetComponent* DisplayPopupWidgetComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	USK_PopupHintWidgetComponent* ProgressBarWidgetComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
	TSubclassOf<USKW_NPSWorkProgressBar> ProgressBarClass;

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite,Category = "Components")
	USkeletalMeshComponent* SkeletalMeshComponent;

	void StartBuild();

	void StartWork(const TObjectPtr<ASK_MachineActorBase>& MachineActor);
	void SetIsWorking(bool Value) { IsWorking = Value; }
protected:
	virtual void BeginPlay() override;

	void BuildTick();
	void InitWorkTick();
	void WorkTick();

	void FirstDelay_Work();
	void SecondDelay_Work();

	void FirstDelay_Build();
	void SecondDelay_Build();
	
	bool HaveAnotherWork();
	void PlayAnim();
	void AnimTick();
	void AnimEnd();
	void PlayMontage(const TObjectPtr<UAnimMontage>& Montage);
protected:
	UPROPERTY(EditAnywhere,Category = "Delay")
	float FirstDelayWork{ 0.5f };

	UPROPERTY(EditAnywhere, Category = "Delay")
	float SecondDelayWork{ 10.0f };

	UPROPERTY(EditAnywhere, Category = "Delay")
	float FirstDelayBuild{ 0.5f };

	UPROPERTY(EditAnywhere, Category = "Delay")
	float SecondDelayBuild{ 10.0f };

	UPROPERTY(EditDefaultsOnly,Category = "Anim")
	TObjectPtr<UAnimMontage> SlipwayAnimMontage;
private:
	TObjectPtr<UAnimMontage> AnimMontage;

	UPROPERTY()
	TObjectPtr<USK_ShipBuilder> ShipBuilder;

	UPROPERTY()
	TObjectPtr<ASK_MachineActorBase> MachineActor;

	FTimerHandle BuildTickHandle;
	FTimerHandle WorkTickHandle;

	FTimerHandle FirstDelayWorkHandle;
	FTimerHandle SecondDelayWorkHandle;

	FTimerHandle FirstDelayBuildHandle;
	FTimerHandle SecondDelayBuildHandle;

	FTimerHandle AnimTimerHandle;
	FTimerHandle FinishAnimTimerHandle;

	float WorkFrequency{ 0.f };
	bool IsWorking{ false };
	uint32 NumOfLoop{ 0 };
	uint32 CurrentCycle{ 0 };
};
