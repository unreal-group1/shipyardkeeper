// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "SK_Types.h"
#include "SK_GameMode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnNextShipProjectChange, FName, NewShipProject);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSlipwayUpgrade, TEnumAsByte<ESlipwayLevel>, NewLevel);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBuildNewMachine);

class ASK_MachineActorBase;
class ASK_Slipway;
class USK_ShipBuilder;
class USKW_Game;
class USK_MiniGame;
class USK_TaskWidget;
class ASK_ShipActor;
class USK_TaskComponent;
class ASK_TileActorBase;
class ASK_StorageActor;

USTRUCT()
struct FMachineTypes
{
	GENERATED_BODY()

	FMachineTypes() {};
	FMachineTypes(const TObjectPtr<ASK_MachineActorBase>& Actor,const FName& ResourseName) : OutResourceRowName(ResourseName) 
	{
		MachineActors.Add(Actor);
	};

	FName OutResourceRowName;
	TArray<TObjectPtr<ASK_MachineActorBase>> MachineActors;
};

UCLASS(minimalapi)
class ASK_GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASK_GameMode();

	FOnNextShipProjectChange OnNextShipProjectChange;
	FOnSlipwayUpgrade OnSlipwayUpgrade;
	FOnBuildNewMachine OnBuildNewMachine;

	const FUint32Point GetGridDim() const { return GridDim; }

	const uint32 GetCurrentPrice(bool bIsRow, const uint32 Index) const;

	//bool GetLevelInfo(const uint32 Level, FMachineLevelInfo& LevelInfo);

	void AddMachine(const TObjectPtr<ASK_MachineActorBase>& MachineActor,const FName& ResourseOutRowName);

	const TArray<FName>& GetSelectionPriority() { return SelectionPriority; }
	const TArray<FMachineTypes>& GetMachineActors() { return MachineActors; }

	const FName& GetCurrentProject();
	void SetCurrentProject(const FName& ShipName);

	const TObjectPtr<USK_ShipBuilder>& GetShipBuilder() { return ShipBuilder; }

	void InitManager();

	const TEnumAsByte<ESlipwayLevel>& GetSlipwayLevel() { return CurrentSlipwayLevel; }
	void TryUpSlipway(const int32 Cost);

	const TSubclassOf<ASK_ShipActor>& GetShipActorClass() { return ShipActorClass; }
	void InitTaskComponent();

	void SetMachineTiles(const TArray<TObjectPtr<ASK_TileActorBase>>& Tiles) { MachineTiles = Tiles; }
	const TArray<TObjectPtr<ASK_TileActorBase>>& GetMachineTiles() { return MachineTiles; }

	void SetStorageActors(const TObjectPtr<ASK_StorageActor>& Tiles);
	const TArray<TObjectPtr<ASK_StorageActor>>& GetStorageActors() { return StorageActors; }

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly,Category = "Classes")
	TSubclassOf<ASK_ShipActor> ShipActorClass;

	//Number of tiles for machines WxH
	UPROPERTY(EditDefaultsOnly,Category = "Tiles")
	FUint32Point GridDim{ 2, 2 };

	//Cost of adding a new line
	UPROPERTY(EditDefaultsOnly, Category = "Tiles")
	TArray<uint32> CostOfExpansionRow;

	UPROPERTY(EditDefaultsOnly, Category = "Tiles")
	TArray<uint32> CostOfExpansionCol;

	//Procedure for checking available works
	UPROPERTY(EditDefaultsOnly, Category = "AI")
	TArray<FName> SelectionPriority;

	UPROPERTY(VisibleAnywhere,Category = "Components")
	USK_TaskComponent* TaskComponent;

	FTimerHandle ResourceUpdate;
	void SetResourceTest();

	void UpSlipway();

	UPROPERTY()
	TArray<TObjectPtr<ASK_TileActorBase>> MachineTiles;

	UPROPERTY()
	TArray<TObjectPtr<ASK_StorageActor>> StorageActors;
private:
	UPROPERTY()
	TArray<FMachineTypes> MachineActors;

	UPROPERTY()
	TObjectPtr<USK_ShipBuilder> ShipBuilder;

	UPROPERTY()
	FName CurrentProject;

	TEnumAsByte<ESlipwayLevel> CurrentSlipwayLevel = ESlipwayLevel::One_Level;

	UWorld* World;
};