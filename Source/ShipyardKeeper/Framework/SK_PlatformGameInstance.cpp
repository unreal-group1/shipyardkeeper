// Fill out your copyright notice in the Description page of Project Settings.

#include "SK_PlatformGameInstance.h"
#include "SK_Types.h"
#include "UI/Widgets/Slipway/SK_UpSlipwayWidget.h"

#include "Kismet/GameplayStatics.h"

#pragma region ResourceManagement
bool USK_PlatformGameInstance::TryToBuy(const uint32 Cost)
{
	if (GameCurrency >= Cost)
	{
		GameCurrency = FMath::Clamp(GameCurrency - Cost, 0, GameCurrency);
		if (Cost != 0)
		{
			OnSubAmountCurrencyChange.Broadcast(GameCurrency);
		}
		return true;
	}

	return false;
}

void USK_PlatformGameInstance::AddToBank(const uint32 Amount)
{
	GameCurrency += Amount;
	if (Amount != 0)
	{
		OnAddAmountCurrencyChange.Broadcast(GameCurrency);
	}
}

uint32 USK_PlatformGameInstance::GetAmountCurrency()
{
	return GameCurrency;
}

void USK_PlatformGameInstance::AddResourceToStorage(const FName ResourceName, const double Quantity)
{
	const auto Index = Storage.IndexOfByPredicate([&](const FStorage& Info) {

		return Info.ResourceName == ResourceName;
		});

	if (Index != INDEX_NONE)
	{
		Storage[Index].Quantity += Quantity;

		OnResourceCountChange.Broadcast(Storage[Index].ResourceName, 
			Storage[Index].Quantity);
	}
	else
	{
		Storage.Add(FStorage(ResourceName, Quantity));

		OnResourceCountChange.Broadcast(ResourceName,
			Quantity);
	}
}

bool USK_PlatformGameInstance::TrySpendResource(const FName ResourceName, const double Quantity)
{
	const auto Index = Storage.IndexOfByPredicate([&](const FStorage& Info) {

		return Info.ResourceName == ResourceName;
		});

	if (Index != INDEX_NONE && Storage[Index].Quantity >= Quantity)
	{
		Storage[Index].Quantity -= Quantity;

		OnResourceCountChange.Broadcast(ResourceName,
			Storage[Index].Quantity);

		return true;
	}

	return false;
}

double USK_PlatformGameInstance::GetResourceQuantity(const FName ResourceName)
{
	const auto Index = Storage.IndexOfByPredicate([&](const FStorage& Info) 
	{
		return Info.ResourceName == ResourceName;
	});

	if (Index != INDEX_NONE)
	{
		return Storage[Index].Quantity;
	}
	return -1.0;
}

FResourceType* USK_PlatformGameInstance::GetResourceTypeByName(const FName ResourceName)
{
	const auto Value = ResourcesTable->FindRow<FResourceType>(ResourceName, "", false);
	if (Value)
	{
		return Value;
	}
	else
	{
		return nullptr;
	}
}

TArray<FResourceType*> USK_PlatformGameInstance::GetResourceTypesWithOriginalNames()
{
	TArray<FResourceType*> AllRows;

	ResourcesTable->GetAllRows<FResourceType>(TEXT("Resource"), AllRows);
	auto RowNames = ResourcesTable->GetRowNames();

	for (int i = 0; i < AllRows.Num(); i++)
	{
		AllRows[i]->ShowingData.RowName = FText::FromString(RowNames[i].ToString());
	}

	return AllRows;
}

bool USK_PlatformGameInstance::IsEnough(const FName& ResourceName, const double Quantity)
{
	const auto Index = Storage.IndexOfByPredicate([&](const FStorage& Info) {

		return Info.ResourceName == ResourceName;
		});

	if (Index != INDEX_NONE)
	{
		return Storage[Index].Quantity >= Quantity;
	}
	return false;
}

bool USK_PlatformGameInstance::IsPossibleToBuild(const TArray<FResource>& Resourses, double QuantityForTick)
{
	for (const auto& Resourse : Resourses)
	{
		const auto Index = Storage.IndexOfByPredicate([&](const FStorage& Info) {//TMap

			return Info.ResourceName == Resourse.ResourcesRowName;
			});

		if (Index == INDEX_NONE || Storage[Index].Quantity == 0) continue;

		if (Storage[Index].Quantity <= QuantityForTick)
		{
			QuantityForTick -= Storage[Index].Quantity;
			if (QuantityForTick == 0) return true;
		}
		else
		{
			return true;
		}
	}

	return false;
}

bool USK_PlatformGameInstance::SpendResources(const TArray<FResource>& RequiredResourses, TArray<FResource>& NestedResourses, double QuantityForTick)
{
	for (const auto& Resourse : RequiredResourses)
	{
		const auto Index = Storage.IndexOfByPredicate([&](const FStorage& Info)
			{//TMap
			return Info.ResourceName == Resourse.ResourcesRowName && Resourse.ResourceQuantity != 0;
			});

		if (Index == INDEX_NONE || Storage[Index].Quantity == 0) continue;

		if (Storage[Index].Quantity >= QuantityForTick)
		{
			Storage[Index].Quantity -= QuantityForTick;

			OnResourceCountChange.Broadcast(Storage[Index].ResourceName,
				Storage[Index].Quantity);

			auto ResIndex = NestedResourses.IndexOfByPredicate([&](const FResource& Resource)
				{
					return Resource.ResourcesRowName == Storage[Index].ResourceName;
				});

			NestedResourses[ResIndex].ResourceQuantity += QuantityForTick;

			return true;
		}
		else
		{
			auto ResIndex = NestedResourses.IndexOfByPredicate([&](const FResource& Resource)
				{
					return Resource.ResourcesRowName == Storage[Index].ResourceName;
				});

			NestedResourses[ResIndex].ResourceQuantity += Storage[Index].Quantity;
			QuantityForTick -= Storage[Index].Quantity;
			Storage[Index].Quantity = 0.0;


			OnResourceCountChange.Broadcast(Storage[Index].ResourceName,
				Storage[Index].Quantity);
		}
	}

	return false;
}

bool USK_PlatformGameInstance::GetStorageBuyName(const FName& RowName, FStorageType& StorageInfo)
{
	if (!StoragesTable)
	{
		//ToDo UE_LOG
		return false;
	}

	const auto Info = StoragesTable->FindRow<FStorageType>(RowName, "", false);
	if (Info)
	{
		StorageInfo = *Info;
		return true;
	}

	return false;
}

FStorageType* USK_PlatformGameInstance::GetStorageByName(const FName& RowName)
{
	FStorageType StorageType;
	if (StoragesTable)
	{
		if (StoragesTable->GetRowMap().Num() > 0)
		{
			auto FirstRow = StoragesTable->FindRow<FStorageType>(RowName, TEXT("StorageType"));
			return FirstRow;
		}
		else
		{
			return nullptr;
		}
	}
	else
	{
		return nullptr;
	}
}

bool USK_PlatformGameInstance::GetMachineBuyName(const FName& RowName, FMachineType& MachineInfo)
{
	if (!MachinesTable)
	{
		//ToDo UE_LOG
		return false;
	}

	const auto Info = MachinesTable->FindRow<FMachineType>(RowName, "", false);
	if (Info)
	{
		MachineInfo = *Info;
		return true;
	}

	return false;
}

bool USK_PlatformGameInstance::GetShipsBuyName(const FName& RowName, FShipTypes& ShipInfo)
{
	if (!ShipsTable)
	{
		//ToDo UE_LOG
		return false;
	}

	const auto Info = ShipsTable->FindRow<FShipTypes>(RowName, "", false);
	if (Info)
	{
		ShipInfo = *Info;
		return true;
	}

	return false;
}

FShipTypes* USK_PlatformGameInstance::GetShipsBuyName(const FName& RowName)
{
	FShipTypes ShipType;
	if (ShipsTable)
	{
		if (ShipsTable->GetRowMap().Num() > 0)
		{
			auto FirstRow = ShipsTable->FindRow<FShipTypes>(RowName, TEXT("ShipType"));
			return FirstRow;
		}
		else
		{
			return nullptr;
		}
	}
	else
	{
		return nullptr;
	}
}

FShipTypes* USK_PlatformGameInstance::GetFirstShipType()
{
	FShipTypes ShipType;
	if (ShipsTable)
	{
		if (ShipsTable->GetRowMap().Num() > 0)
		{
			auto ShipArray = ShipsTable->GetRowMap().Array();
			auto FirstRowName = ShipArray[0].Key;
			auto FirstRow = ShipsTable->FindRow<FShipTypes>(FirstRowName, TEXT("ShipType"));
			return FirstRow;
		}
		else
		{
			return nullptr;
		}
	}
	else
	{
		return nullptr;
	}
}

void USK_PlatformGameInstance::SetResourceNum()
{
	auto Index = Storage.IndexOfByPredicate([&](const FStorage& Item) {
		return Item.ResourceName == FName("Log");
		});

	if (Index != INDEX_NONE)
	{
		Storage[Index].Quantity = 10;
	}
	else
	{
		Storage.Add(FStorage(FName("Log"), 10));
	}

	Index = Storage.IndexOfByPredicate([&](const FStorage& Item) {
		return Item.ResourceName == FName("Wool");
		});

	if (Index != INDEX_NONE)
	{
		Storage[Index].Quantity = 0;
	}
	else
	{
		Storage.Add(FStorage(FName("Wool"), 0));
	}

	Index = Storage.IndexOfByPredicate([&](const FStorage& Item) {
		return Item.ResourceName == FName("Hemp");
		});

	if (Index != INDEX_NONE)
	{
		Storage[Index].Quantity = 0;
	}
	else
	{
		Storage.Add(FStorage(FName("Hemp"), 0));
	}
}

const int32 USK_PlatformGameInstance::GetSlipwayUpPrice(const TEnumAsByte<ESlipwayLevel>& CurrentLevel)
{
	const auto Index = CurrentLevel.GetIntValue();
	if (SlipwayPrice.IsValidIndex(Index))
	{
		return SlipwayPrice[Index];
	}
	return INDEX_NONE;
}

void USK_PlatformGameInstance::ReturnResources(const TArray<FResource>& Resource)
{
	for (const auto& Res : Resource)
	{
		AddResourceToStorage(Res.ResourcesRowName, Res.ResourceQuantity);
	}
}

#pragma endregion ResourceManagement

#pragma region BuyWorkers
const float USK_PlatformGameInstance::GetCurrentSpawnPrice()
{
	auto NextIndex = CurrentIndexSpawnPrice;
	NextIndex++;
	if (SpawnPrice.Num() > 0
		&& SpawnPrice.IsValidIndex(NextIndex))
	{
		return SpawnPrice[CurrentIndexSpawnPrice];
	}
	
	return CurrentIndexSpawnPrice;
}
const float USK_PlatformGameInstance::GetNextPrice()
{
	if (SpawnPrice.IsValidIndex(CurrentIndexSpawnPrice))
	{
		return SpawnPrice[CurrentIndexSpawnPrice];
	}

	return -1;
}
const uint32 USK_PlatformGameInstance::SetNewIndexSpawnPrice()
{
	CurrentIndexSpawnPrice++;
	return 	CurrentIndexSpawnPrice;
}

const TObjectPtr<ASK_NPSSpawner>& USK_PlatformGameInstance::GetNPSSpawner()
{
	if (GetWorld()
		&& !NPSSpawner)
	{
		NPSSpawner = Cast<ASK_NPSSpawner>(UGameplayStatics::GetActorOfClass(GetWorld(),
			ASK_NPSSpawner::StaticClass()));
	}

	return NPSSpawner;
}
#pragma endregion BuyWorkers

#pragma region UI

FName USK_PlatformGameInstance::GetCurrentLevelName() const
{
	if (!CurrentLevelName.IsNone())
	{
		return CurrentLevelName;
	}
	UE_LOG(LogTemp, Error, TEXT("SK_GameInstance: CurrentLevel is empty"));

	return GetMenuLevelName();
}

void USK_PlatformGameInstance::SetCurrentLevelName(const FName NewCurrentLevelName)
{
	if (!NewCurrentLevelName.IsNone())
	{
		CurrentLevelName = NewCurrentLevelName;
		return;
	}
	UE_LOG(LogTemp, Error, TEXT("SK_GameInstance: NewCurrentLevelName is empty"));
}

TArray<FName> USK_PlatformGameInstance::GetLevelsNames() const
{
	if (LevelsNames.Num() <= 0)
	{
		UE_LOG(LogTemp, Error, TEXT("SK_GameInstance: LevelsNames is empty"));
		return LevelsNames;
	}
	else
	{
		return LevelsNames;
	}
}

FName USK_PlatformGameInstance::GetMenuLevelName() const
{
	if (!MenuLevelName.IsNone())
	{
		return MenuLevelName;
	}
	UE_LOG(LogTemp, Error, TEXT("SK_GameInstance: MenuLevelName is empty"));
	return EmptyLevelName;
}
TArray<ASK_TileActorBase*> USK_PlatformGameInstance::GetMachineTiles()
{
	if (!TileSpawner)
	{
		GetTileSpawner();
	}

	if (TileSpawner)
	{
		return TileSpawner->GetMachineTiles();
	}

	return TArray<ASK_TileActorBase*>();
}

TArray<FShipTypes*> USK_PlatformGameInstance::GetShipTypesWithOriginalNames()
{
	TArray<FShipTypes*> AllRows;

	ShipsTable->GetAllRows<FShipTypes>(TEXT("ShipType"), AllRows);
	auto RowNames = ShipsTable->GetRowNames();

	for (int i = 0; i < AllRows.Num(); i++)
	{
		AllRows[i]->ShowingData.RowName = FText::FromString(RowNames[i].ToString());
	}

	return AllRows;
}

TArray<ASK_TileActorBase*> USK_PlatformGameInstance::GetStorageTiles()
{
	if (!TileSpawner)
	{
		GetTileSpawner();
	}

	if (TileSpawner)
	{
		return TileSpawner->GetStorageTiles();
	}

	return TArray<ASK_TileActorBase*>();
}

ASK_TileSpawner* USK_PlatformGameInstance::GetTileSpawner()
{
	if (TileSpawner)
	{
		return TileSpawner;
	}
	else
	{
		if (GetWorld())
		{
			TileSpawner = Cast<ASK_TileSpawner>(
				UGameplayStatics::GetActorOfClass(GetWorld(),
					ASK_TileSpawner::StaticClass()));
			return TileSpawner;
		}

		return nullptr;
	}
}

TArray<FMachineType*> USK_PlatformGameInstance::GetMachineTypesWithOriginalNames()
{
	TArray<FMachineType*> AllRows;

	MachinesTable->GetAllRows<FMachineType>(TEXT("MachineType"), AllRows);
	auto RowNames = MachinesTable->GetRowNames();

	for (int i = 0; i < AllRows.Num(); i++)
	{
		AllRows[i]->MachineName = FText::FromString(RowNames[i].ToString());
	}

	return AllRows;
}
#pragma endregion UI

#pragma region GlobalMap
FGlobalMapElement* USK_PlatformGameInstance::GetGlobalMapElementByName(const FName& RowName)
{
	FGlobalMapElement GlobalMapElement;
	if (GlobalMapElementsTable)
	{
		if (GlobalMapElementsTable->GetRowMap().Num() > 0)
		{
			auto FirstRow = GlobalMapElementsTable->FindRow<FGlobalMapElement>(RowName, TEXT("GlobalMapElement"));
			return FirstRow;
		}
		else
		{
			return nullptr;
		}
	}
	else
	{
		return nullptr;
	}
}
#pragma endregion GlobalMap