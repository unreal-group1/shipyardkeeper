// Fill out your copyright notice in the Description page of Project Settings.


#include "Framework/SK_AIControllerBase.h"
#include "AI/Pawn/SK_AIPawnBase.h"

void ASK_AIControllerBase::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	const auto AICharacter = Cast<ASK_AIPawnBase>(InPawn);
	if (AICharacter)
	{
		RunBehaviorTree(AICharacter->BehaviorTreeAsset);
	}
}
