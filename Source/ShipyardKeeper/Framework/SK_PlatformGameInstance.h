// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintPlatformLibrary.h"

#include "SK_NPSSpawner.h"
#include "SK_TileSpawner.h"
#include "SK_TileActorBase.h"
#include "SK_Types.h"
#include "SK_PlatformGameInstance.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnResourceCountChange, FName, ResourceName, double, NewQuantity);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAddAmountCurrencyChange, int32, NewAmountCurrency);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSubAmountCurrencyChange, int32, NewAmountCurrency);

class USK_UpSlipwayWidget;

UCLASS()
class SHIPYARDKEEPER_API USK_PlatformGameInstance : public UPlatformGameInstance
{
	GENERATED_BODY()
	
#pragma region ResourceManagement
public:
    UPROPERTY(BlueprintAssignable)
    FOnResourceCountChange OnResourceCountChange;
    UPROPERTY(BlueprintAssignable)
    FOnAddAmountCurrencyChange OnAddAmountCurrencyChange;
    UPROPERTY(BlueprintAssignable)
    FOnSubAmountCurrencyChange OnSubAmountCurrencyChange;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DataSettings")
    TArray<FText> RawResourceNames;

    bool TryToBuy(const uint32 Cost);
    void AddToBank(const uint32 Amount);
    uint32 GetAmountCurrency();

    void AddResourceToStorage(const FName ResourceName, const double Quantity);
    bool TrySpendResource(const FName ResourceName, const double Quantity);
    double GetResourceQuantity(const FName ResourceName);
    FResourceType* GetResourceTypeByName(const FName ResourceName);
    TArray<FResourceType*> GetResourceTypesWithOriginalNames();
    bool IsEnough(const FName& ResourceName, const double Quantity);
    bool IsPossibleToBuild(const TArray<FResource>& Resourses, double QuantityForTick);
    bool SpendResources(const TArray<FResource>& RequiredResourses, TArray<FResource>& NestedResourses, double QuantityForTick);

    void SetResourceNum();
    const TArray<float>& GetSpawnPrice() { return SpawnPrice; }
    const int32 GetSlipwayUpPrice(const TEnumAsByte<ESlipwayLevel>& CurrentLevel);

    void ReturnResources(const TArray<FResource>& Resource);

    const TArray<int32>& GetStoragePrice() { return StoragePrice; }
protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DataSettings")
    UDataTable* ResourcesTable = nullptr;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DataSettings")
    UDataTable* MachinesTable = nullptr;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DataSettings")
    UDataTable* StoragesTable = nullptr;

    UPROPERTY(EditDefaultsOnly,Category = "Price")
    TArray<float> SpawnPrice;

    UPROPERTY(EditDefaultsOnly, Category = "Price")
    TArray<int32> SlipwayPrice;

    UPROPERTY(EditDefaultsOnly, Category = "Price")
    TArray<int32> StoragePrice;
private:
    uint32 GameCurrency{ 0 };

    TArray<FStorage> Storage;
#pragma endregion ResourceManagement

#pragma region BuyWorkers
public:
    const float GetCurrentSpawnPrice();
    const float GetNextPrice();
    const uint32 SetNewIndexSpawnPrice();
    const TObjectPtr<ASK_NPSSpawner>& GetNPSSpawner();

private:
    UPROPERTY()
    TObjectPtr<ASK_NPSSpawner> NPSSpawner;
    UPROPERTY()
    uint32 CurrentIndexSpawnPrice = 0;
#pragma endregion BuyWorkers

#pragma region Levels
public:
    UFUNCTION()
    FName GetCurrentLevelName() const;
    UFUNCTION()
    void SetCurrentLevelName(const FName NewCurrentLevelName);
    UFUNCTION()
    TArray<FName> GetLevelsNames() const;
    UFUNCTION()
    FName GetMenuLevelName() const;

protected:
    const FName EmptyLevelName = NAME_None;

    UPROPERTY(EditDefaultsOnly, Category = "Game")
    TArray<FName> LevelsNames;

    UPROPERTY(EditDefaultsOnly, Category = "Game", meta = (ToolTip = "Lavel names must be unique!"))
    FName MenuLevelName = EmptyLevelName;

private:
    FName CurrentLevelName = EmptyLevelName;
#pragma endregion Levels

#pragma region Tiles
public:
    UFUNCTION()
    ASK_TileSpawner* GetTileSpawner();
#pragma region MachineTiles
    public:
    UFUNCTION()
    TArray<ASK_TileActorBase*> GetMachineTiles();
    TArray<FMachineType*> GetMachineTypesWithOriginalNames();
    bool GetMachineBuyName(const FName& RowName, FMachineType& MachineInfo);

    private:
    ASK_TileSpawner* TileSpawner;
#pragma endregion MachineTiles

#pragma region StorageTiles
    public:
    UFUNCTION()
    TArray<ASK_TileActorBase*> GetStorageTiles();

    bool GetStorageBuyName(const FName& RowName, FStorageType& StorageInfo);
    FStorageType* GetStorageByName(const FName& RowName);
#pragma endregion StorageTiles
#pragma endregion Tiles

#pragma region Ships
public:
    TArray<FShipTypes*> GetShipTypesWithOriginalNames();
    bool GetShipsBuyName(const FName& RowName, FShipTypes& ShipInfo);
    FShipTypes* GetShipsBuyName(const FName& RowName);
    FShipTypes* GetFirstShipType();

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DataSettings")
    UDataTable* ShipsTable = nullptr;
#pragma endregion Ships

#pragma region GlobalMap
    protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DataSettings")
    UDataTable* GlobalMapElementsTable = nullptr;
    public:
    FGlobalMapElement* GetGlobalMapElementByName(const FName& RowName);
#pragma endregion GlobalMap
};
