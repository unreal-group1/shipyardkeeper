#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "SK_Types.generated.h"

class UStaticMesh;
class UAnimMontage;
class USkeletalMesh;
class UAnimInstance;

UENUM()
enum ETileState : uint8
{
	Blocked = 0,
	Purchased,
	Involved
};

UENUM()
enum EMachineWorkplaces : uint8
{
	One = 0,
	Two,
	Three
};

USTRUCT(BlueprintType)
struct FShowingData : public FTableRowBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, Category = "UI")
	FText RowName;

	UPROPERTY(EditDefaultsOnly, Category = "UI")
	FText PrintName;

	UPROPERTY(EditDefaultsOnly, Category = "UI")
	UTexture2D* Image;
};

USTRUCT(BlueprintType)
struct FShips
{
	GENERATED_BODY()

	FShips() {};

	FShips(const FName& ShipName, const int32 Value) : ShipRowName(ShipName), Num(Value) {};

	UPROPERTY()
	FName ShipRowName;

	UPROPERTY()
	int32 Num{ 0 };

};

#pragma region GlobalMap
USTRUCT(BlueprintType)
struct FGlobalMapElement : public FTableRowBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, Category = "Name")
	FText NameGlobalMapElement;

	UPROPERTY(EditDefaultsOnly, Category = "Sorage")
	FText StorageName;

	UPROPERTY(EditDefaultsOnly, Category = "GlobalMap")
	bool IsRoad;

	UPROPERTY(EditDefaultsOnly, Category = "Levels")
	TArray<UTexture2D*> LevelImages;
};
#pragma endregion GlobalMap

USTRUCT(BlueprintType)
struct FStorage
{
	GENERATED_BODY()

	FStorage() {};

	FStorage(const FName& InResourceName, const double InQuantity) : ResourceName(InResourceName), Quantity(InQuantity) {};

	FName ResourceName;

	double Quantity;

};

USTRUCT(BlueprintType)
struct FResourceType : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = "Shownig")
	FShowingData ShowingData;
};

USTRUCT(BlueprintType)
struct FMachineLevelInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Efficiency")
	float CreateProductionTime{ 10.f };

	UPROPERTY(EditDefaultsOnly, Category = "Info")
	uint32 CostOfTheIncrease{ 50 };

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Efficiency")
	TEnumAsByte<EMachineWorkplaces> NumberOfMachines = EMachineWorkplaces::One;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Efficiency")
	double QuantityResourceIn{ 1 };

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Efficiency")
	double QuantityResourceOut{ 1 };
};

USTRUCT(BlueprintType)
struct FAnimInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	TSubclassOf<UAnimInstance> AnimBlueprintClass;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	TObjectPtr<UAnimMontage> StartPawnAnimMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	TObjectPtr<UAnimMontage> RepeatPawnAnimMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	TObjectPtr<UAnimMontage> FinishPawnAnimMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	TObjectPtr<UAnimMontage> StartActorAnimMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	TObjectPtr<UAnimMontage> RepeatActorAnimMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	TObjectPtr<UAnimMontage> FinishActorAnimMontage;
};
USTRUCT(BlueprintType)
struct FMachineType : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = "Shownig")
	FShowingData ShowingData;

	UPROPERTY(EditDefaultsOnly,Category = "Mesh")
	USkeletalMesh* SkeletalMesh;

	UPROPERTY(EditDefaultsOnly, Category = "Name")
	FText MachineName;

	UPROPERTY(EditDefaultsOnly, Category = "Products")
	FName ResourceInRowName = NAME_None;

	UPROPERTY(EditDefaultsOnly, Category = "Products")
	FName ResourceOutRowName = NAME_None;

	UPROPERTY(EditDefaultsOnly, Category = "Mesh")
	FVector SpawnLocationOffset;

	UPROPERTY(EditDefaultsOnly, Category = "Mesh")
	FRotator SpawnRotationOffset;

	UPROPERTY(EditDefaultsOnly, Category = "Mesh")
	FVector WorkLocation;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	FAnimInfo AnimInfo;

	UPROPERTY(EditDefaultsOnly, Category = "Tiles")
	TArray<FMachineLevelInfo> MachineLevelsInfo;
};

USTRUCT(BlueprintType)
struct FStorageLevels
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = "Products")
	float TickInterval{10.f};

	UPROPERTY(EditDefaultsOnly, Category = "Products")
	double ResourceNum{5.f};
};

USTRUCT(BlueprintType)
struct FStorageType : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = "Name")
	FText StorageName;

	UPROPERTY(EditDefaultsOnly, Category = "Products")
	FName ResourceOutRowName = NAME_None;

	UPROPERTY(EditDefaultsOnly, Category = "Products")
	double InitialQuantity{ 1 };

	UPROPERTY(EditDefaultsOnly, Category = "Info")
	uint32 PricePerUnit{ 1 };

	UPROPERTY(EditDefaultsOnly, Category = "Mesh")
	USkeletalMesh* SkeletalMesh;

	UPROPERTY(EditDefaultsOnly, Category = "Mesh")
	FVector SpawnOffset;

	UPROPERTY(EditDefaultsOnly, Category = "Products")
	TArray<FStorageLevels> StorageLevels;

};

USTRUCT(BlueprintType)
struct FResource
{
	GENERATED_BODY()

	FResource() {};

	FResource(const FName& RowName, const double Value) : ResourcesRowName(RowName), ResourceQuantity(Value) {};

	UPROPERTY(EditDefaultsOnly, Category = "Resource")
	FName ResourcesRowName;

	UPROPERTY(EditDefaultsOnly, Category = "Resource")
	double ResourceQuantity;

};

UENUM()
enum ESlipwayLevel : uint8
{
	One_Level = 0,
	Two_Level = 1,
	Three_Level = 2,
	Fourth_Level = 3
};

USTRUCT(BlueprintType)
struct FShipTypes : public FTableRowBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, Category = "Shownig")
	FShowingData ShowingData;

	UPROPERTY(EditDefaultsOnly, Category = "Shownig")
	float SpawnDelay{ 5.f };

	UPROPERTY(EditDefaultsOnly, Category = "Shownig")
	float LifeSpan{ 5.f };

	UPROPERTY(EditDefaultsOnly, Category = "Mesh")
	UStaticMesh* Mesh;

	UPROPERTY(EditDefaultsOnly, Category = "Mesh")
	FVector SpawnOffset;

	UPROPERTY(EditDefaultsOnly, Category = "Products")
	uint32 Endurance;

	UPROPERTY(EditDefaultsOnly, Category = "Products")
	uint32 Reward;

	UPROPERTY(EditDefaultsOnly, Category = "Products")
	TEnumAsByte<ESlipwayLevel> RequiredSlipwayLevel;

	UPROPERTY(EditDefaultsOnly, Category = "Products")
	TArray<FResource> NeededResources;
};

UENUM()
enum EObjectOfVerification : uint8
{
	Resource = 0,
	Ship,
	SelectShip,
	Machine,
	MachineUp,
	SlipwayUp,
	MiniGame,
	HiringWorkers,
	StorageUp
};

USTRUCT(BlueprintType)
struct FTasks
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly,Category = "Category")
	TEnumAsByte<EObjectOfVerification> ObjectOfVerification;

	UPROPERTY(EditDefaultsOnly,Category = "ObjectName",
		meta = (EditCondition = "ObjectOfVerification != EObjectOfVerification::HiringWorkers && ObjectOfVerification != EObjectOfVerification::SlipwayUp"))
	FName RowName;

	UPROPERTY(EditDefaultsOnly, Category = "Text")
	FText TaskText;

	UPROPERTY(EditDefaultsOnly, Category = "Text")
	FText NarrativeText;

	UPROPERTY(EditDefaultsOnly, Category = "Reward")
	uint32 Reward{ 10 };

	UPROPERTY(EditDefaultsOnly, Category = "ObjectName", meta = (ClampMin = "1"))
	int32 Quantity;
};

UCLASS()
class SHIPYARDKEEPER_API USK_Types : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

};