// Copyright Epic Games, Inc. All Rights Reserved.

#include "SK_GameMode.h"
#include "SK_PlayerController.h"
#include "SK_CharacterBase.h"
#include "SK_GameHUD.h"
#include "SK_Slipway.h"
#include "SK_PlatformGameInstance.h"
#include "SK_Types.h"
#include "Managers/SK_ShipBuilder.h"
#include "SKW_Game.h"

#include "Actors/SK_MachineActorBase.h"

#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "Actors/Components/SK_TaskComponent.h"

DEFINE_LOG_CATEGORY_STATIC(ASK_GameModeLog, All, All);

ASK_GameMode::ASK_GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ASK_PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	/*static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}*/

	// set default controller to our Blueprinted controller
	//static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	//if(PlayerControllerBPClass.Class != NULL)
	//{
	//	PlayerControllerClass = PlayerControllerBPClass.Class;
	//}

	TaskComponent = CreateDefaultSubobject<USK_TaskComponent>(TEXT("TaskComponent"));

	if (!ASK_GameHUD::StaticClass())
	{
		UE_LOG(LogTemp, Error, TEXT("ASK_GameMode: ASK_GameHUD not have StaticClass"));
	}
	else
	{
		HUDClass = ASK_GameHUD::StaticClass();
	}

	if (GetWorld())
	{
		World = GetWorld();
	}
}

const uint32 ASK_GameMode::GetCurrentPrice(bool bIsRow, const uint32 Index) const
{
	if (bIsRow)
	{
		if (CostOfExpansionRow.IsValidIndex(Index))
		{
			return CostOfExpansionRow[Index];
		}
	}
	else
	{
		if (CostOfExpansionCol.IsValidIndex(Index))
		{
			return CostOfExpansionCol[Index];
		}
	}

	return uint32();
}

/*bool ASK_GameMode::GetLevelInfo(const uint32 Level, FMachineLevelInfo& LevelInfo)
{
	if (MachineLevelsInfo.IsValidIndex(Level))
	{
		LevelInfo = MachineLevelsInfo[Level];
		return true;
	}
	else
	{
		UE_LOG(ASK_GameModeLog, Error, TEXT("MachineLevelsInfo = Invalid index!"));
		return false;
	}
}*/

void ASK_GameMode::AddMachine(const TObjectPtr<ASK_MachineActorBase>& MachineActor, const FName& ResourseOutRowName)
{
	const auto Index = MachineActors.IndexOfByPredicate([&](const FMachineTypes& Types) {
		
		return Types.OutResourceRowName == ResourseOutRowName;
		});

	if (Index != INDEX_NONE)
	{
		MachineActors[Index].MachineActors.Add(MachineActor);
	}
	else
	{
		MachineActors.Add(FMachineTypes(MachineActor, ResourseOutRowName));
	}
	OnBuildNewMachine.Broadcast();
}
const FName& ASK_GameMode::GetCurrentProject()
{
	if (World)
	{
		if (CurrentProject.IsNone())
		{
			auto GI = World->GetGameInstance<USK_PlatformGameInstance>();
			if (GI)
			{
				CurrentProject = FName(GI->GetFirstShipType()->ShowingData.RowName.ToString());
			}
		}
	}
	return CurrentProject;
}

void ASK_GameMode::SetCurrentProject(const FName& ShipName)
{
	if (World)
	{
		auto GI = World->GetGameInstance<USK_PlatformGameInstance>();
		if (GI)
		{
			auto ValidateName = GI->GetShipsBuyName(ShipName);
			if (ValidateName)
			{
				CurrentProject = FName(ValidateName->ShowingData.RowName.ToString());
				OnNextShipProjectChange.Broadcast(CurrentProject);

				ShipBuilder->UpdateProject();
			}
		}
	}
}

void ASK_GameMode::InitManager()
{
	ShipBuilder = NewObject<USK_ShipBuilder>(GetWorld(), USK_ShipBuilder::StaticClass());
}

void ASK_GameMode::TryUpSlipway(const int32 Cost)
{
	if (!GetWorld()) return;

	auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	if (GI && GI->TryToBuy(Cost))
	{
		UpSlipway();
	}
}

void ASK_GameMode::InitTaskComponent()
{
	if (TaskComponent)
	{
		TaskComponent->Init();
	}
}

void ASK_GameMode::SetStorageActors(const TObjectPtr<ASK_StorageActor>& Tiles)
{
	StorageActors.Add(Tiles);
}

void ASK_GameMode::BeginPlay()
{
	Super::BeginPlay();

	ShipBuilder->Init();

	auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	if (GI)
	{
		auto ShipType = GI->GetFirstShipType();
		CurrentProject = FName(ShipType->ShowingData.RowName.ToString());
	}

	//GetWorldTimerManager().SetTimer(ResourceUpdate, this, &ASK_GameMode::SetResourceTest, 10.f, true);
	SetResourceTest();
}

void ASK_GameMode::SetResourceTest()
{
	auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	if (GI)
	{
		GI->SetResourceNum();
	}
}

void ASK_GameMode::UpSlipway()
{
	const auto NewLevel = CurrentSlipwayLevel.GetIntValue() + 1;

	CurrentSlipwayLevel = TEnumAsByte<ESlipwayLevel>(NewLevel);

	ShipBuilder->SlipwayLevelUpdate(NewLevel);

	OnSlipwayUpgrade.Broadcast(CurrentSlipwayLevel);
}
