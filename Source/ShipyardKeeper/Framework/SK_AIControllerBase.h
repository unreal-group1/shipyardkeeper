// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "SK_AIControllerBase.generated.h"

UCLASS()
class SHIPYARDKEEPER_API ASK_AIControllerBase : public AAIController
{
	GENERATED_BODY()

protected:
	virtual void OnPossess(APawn* InPawn) override;
};
