// Copyright Epic Games, Inc. All Rights Reserved.

#include "SK_PlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "SK_CharacterBase.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "InputActionValue.h"
#include "EnhancedInputSubsystems.h"
#include "Engine/LocalPlayer.h"
#include "Widgets/Input/SVirtualJoystick.h"
#include "GameFramework/TouchInterface.h"
DEFINE_LOG_CATEGORY(LogTemplateCharacter);

ASK_PlayerController::ASK_PlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Hand;

	bEnableClickEvents = true;
	bEnableTouchEvents = true;
}

void ASK_PlayerController::BeginPlay()
{
	Super::BeginPlay();


}
