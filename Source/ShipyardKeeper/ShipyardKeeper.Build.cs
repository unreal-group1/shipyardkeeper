// Copyright Epic Games, Inc. All Rights Reserved.

using System.Collections.Generic;
using UnrealBuildTool;

public class ShipyardKeeper : ModuleRules
{
	public ShipyardKeeper(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] 
        { 
            "Core", 
            "CoreUObject", 
            "Engine", 
            "InputCore", 
            "NavigationSystem", 
            "AIModule", 
            "Niagara", 
            "EnhancedInput",
            "SlateCore"
        });

		PublicIncludePaths.AddRange(new string[] 
		{ 
			"ShipyardKeeper",
            "ShipyardKeeper/Actors",
            "ShipyardKeeper/Actors/Spawners",
            "ShipyardKeeper/Framework",
            "ShipyardKeeper/Character",
            "ShipyardKeeper/Managers",
            "ShipyardKeeper/UI",
            "ShipyardKeeper/UI/Widgets/Elements",
            "ShipyardKeeper/UI/Menu",
            "ShipyardKeeper/UI/Menu/Framework",
            "ShipyardKeeper/UI/Menu/Widgets",
            "ShipyardKeeper/UI/Game",
            "ShipyardKeeper/UI/Game/Widgets",
            "ShipyardKeeper/UI/Game/Widgets/Tile",
            "ShipyardKeeper/UI/Game/Widgets/Tile/BuyTile",
            "ShipyardKeeper/UI/Game/Widgets/Tile/BuyNewTile",
            "ShipyardKeeper/UI/Game/Widgets/Tile/UpgradeTile",
            "ShipyardKeeper/UI/Game/Widgets/Currency",
            "ShipyardKeeper/UI/Game/Widgets/List/",
            "ShipyardKeeper/UI/Game/Widgets/Ships/",
            "ShipyardKeeper/UI/Game/Widgets/Components",
            "ShipyardKeeper/UI/Game/Widgets/Interfaces",
            "ShipyardKeeper/UI/Game/Widgets/Popup",
            "ShipyardKeeper/UI/Game/Widgets/Popup/Base/",
            "ShipyardKeeper/UI/Game/Widgets/GlobalMap",
            "ShipyardKeeper/UI/Game/Widgets/GlobalMap/Elements",
            "ShipyardKeeper/UI/Game/Widgets/Tasks",
            "ShipyardKeeper/UI/Game/Widgets/Tasks/Elements"
        });
    }
}
