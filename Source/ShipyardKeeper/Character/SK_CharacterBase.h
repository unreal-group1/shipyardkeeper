// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "InputActionValue.h"
#include "Animation/AnimSequenceBase.h"
#include "SK_CharacterBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMiniGameFinished,const int32,ResourceNumIn, const int32, ResourceNumOut);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGameStateUpdated, bool, Value);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSlipwayGameStateUpdated, bool, Value);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSlipwayMiniGameFinished, const int32, Endurance);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAnimationFinished);

DECLARE_MULTICAST_DELEGATE_OneParam(FOnGameCompletedByMName, const FName&);
class UInputComponent;
class USkeletalMeshComponent;
class UCameraComponent;
class UInputAction;
class UInputMappingContext;
class USK_MiniGame;
class ASK_MachineTile;
class UAnimSequenceBase;
class USK_DisplayPopupWidgetComponent;

USTRUCT(Blueprintable)
struct FAnimArray
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Anim")
	TArray<TObjectPtr<UAnimSequenceBase>> AnimSequence;
};
UCLASS(Blueprintable)
class ASK_CharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	ASK_CharacterBase();

	void ShowWorkWidget();
	void HideWorkWidget();

	void ShowShipWidget();
	void HideShipWidget();

	void ShowSlipwayWorkWidget();
	void HideSlipwayWorkWidget();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UInputMappingContext* DefaultMappingContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UInputAction* MoveAction;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	void UpdateMiniGameState(bool Value);
	void UpdateSlipwayMiniGState(bool Value);

	void StopMovement();
	void StartMovement();
	void SetResource(const FName& ResourseRowNameOut, const FName& ResourseRowNameIn);

	UPROPERTY(BlueprintCallable)
	FOnMiniGameFinished OnMiniGameFinished;

	UPROPERTY(BlueprintAssignable)
	FOnGameStateUpdated OnGameStateUpdated;

	UPROPERTY(BlueprintAssignable)
	FOnSlipwayGameStateUpdated OnSlipwayGameStateUpdated;

	UPROPERTY(BlueprintCallable)
	FOnSlipwayMiniGameFinished OnSlipwayMiniGameFinished;

	UPROPERTY(BlueprintAssignable)
	FOnAnimationFinished OnAnimationFinished;

	UFUNCTION(BlueprintCallable)
	const int32 GetSlipwayLevel();

	UFUNCTION(BlueprintCallable)
	const FName GetMashineName();

	UFUNCTION(BlueprintCallable)
	const FMachineLevelInfo GetLevelInfo(const int32 Index);

	UFUNCTION(BlueprintCallable)
	const int32 GetCurrentLevel();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	USK_DisplayPopupWidgetComponent* DisplayPopupWidgetComponent;

	void SetMashine(const TObjectPtr<ASK_MachineTile>& Tile) { MachineTile = Tile; }

	FOnGameCompletedByMName OnGameComplitedByMName;

	void SetMashineName(const FName& MachineName);

	UFUNCTION(BlueprintCallable)
	const ASK_ShipActor* GetCurrentShipActor();

	UFUNCTION(BlueprintCallable)
	void StopSlipwayGame();
protected:
	virtual void BeginPlay() override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void Move(const FInputActionValue& Value);

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "MiniGame")
	bool GameInProgress{ false };

	FName ResourceRowNameOut;
	FName ResourceRowNameIn;
	double ResourceQuantity{ 0.0 };

	FName CurrentMachineName{ NAME_None };

	UFUNCTION(BlueprintCallable)
	void AddResource(const int32 ResourceNumIn, const int32 ResourceNumOut);

	UFUNCTION(BlueprintCallable)
	void AddEndurance(const int32 Endurance);

	UPROPERTY(EditDefaultsOnly,BLueprintReadWrite,Category = "Animation")
	TMap <FName, FAnimArray> AnimationMap;

	void AnimationsBind();
	void OnAnimFinished(USkeletalMeshComponent* SkeletalMesh);
protected:
	UPROPERTY()
	TObjectPtr<USK_MiniGame> MiniGameWidge;

	UPROPERTY()
	TObjectPtr<ASK_MachineTile> MachineTile;
};

