// Copyright Epic Games, Inc. All Rights Reserved.

#include "SK_CharacterBase.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Camera/CameraComponent.h"
#include "UI/Game/Widgets/MiniGames/SK_MiniGame.h"
#include "GameFramework/PlayerController.h"
#include "SK_GameHUD.h"
#include "SKW_Game.h"
#include "SK_PlatformGameInstance.h"
#include "SK_GameMode.h"
#include "Actors/SK_MachineTile.h"
#include "SK_ShipBuilder.h"
#include "SK_Utils.h"
#include "Animations/SK_AnimNotify.h"

#include "SK_DisplayPopupWidgetComponent.h"

ASK_CharacterBase::ASK_CharacterBase()
{
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true; 
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); 
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; 

	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false;

	PrimaryActorTick.bCanEverTick = false;

	GetMesh()->SetCastShadow(false);

	DisplayPopupWidgetComponent = CreateDefaultSubobject<USK_DisplayPopupWidgetComponent>(TEXT("DisplayPopupWidgetComponent"));
	if (DisplayPopupWidgetComponent)
	{
		DisplayPopupWidgetComponent->AttachToComponent(GetCapsuleComponent(),
			FAttachmentTransformRules::KeepRelativeTransform);
	}
}

void ASK_CharacterBase::UpdateMiniGameState(bool Value)
{
	GameInProgress = Value; 
	OnGameStateUpdated.Broadcast(Value);
}

void ASK_CharacterBase::UpdateSlipwayMiniGState(bool Value)
{
	GameInProgress = Value;
	OnSlipwayGameStateUpdated.Broadcast(Value);
}

void ASK_CharacterBase::StopMovement()
{
	GetCharacterMovement()->DisableMovement();
}

void ASK_CharacterBase::StartMovement()
{
	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);
}

void ASK_CharacterBase::SetResource(const FName& InResourseRowNameOut, const FName& InResourseRowNameIn)
{
	ResourceRowNameOut = InResourseRowNameOut;
	ResourceRowNameIn = InResourseRowNameIn;
}

void ASK_CharacterBase::SetMashineName(const FName& MachineName)
{
	CurrentMachineName = MachineName;
}

const ASK_ShipActor* ASK_CharacterBase::GetCurrentShipActor()
{
	if (!GetWorld()) return nullptr;

	const auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
	if (!GM) return nullptr;

	const auto Manager = GM->GetShipBuilder();
	if (Manager)
	{
		return Manager->GetCurrentShipActor();
	}
	return nullptr;
}

void ASK_CharacterBase::StopSlipwayGame()
{
	const auto HUD = GetWorld()->GetFirstPlayerController()->GetHUD<ASK_GameHUD>();
	if (HUD)
	{
		auto GameWidget = HUD->GetGameWidget();
		if (GameWidget)
		{
			MiniGameWidge = GameWidget->GetMiniGameWidget();
			MiniGameWidge->SetSlipwayMGEnd();
		}
	}
	this->UpdateSlipwayMiniGState(false);
	this->StartMovement();
}

void ASK_CharacterBase::BeginPlay()
{
	Super::BeginPlay();

	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
	AnimationsBind();

	OnMiniGameFinished.AddDynamic(this, &ASK_CharacterBase::AddResource);
	OnSlipwayMiniGameFinished.AddDynamic(this, &ASK_CharacterBase::AddEndurance);
}

void ASK_CharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ASK_CharacterBase::Move);
	}
}

void ASK_CharacterBase::Move(const FInputActionValue& Value)
{
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		AddMovementInput(FVector(1.0,0.0,0.0), MovementVector.Y);
		AddMovementInput(FVector(0.0, 1.0, 0.0), MovementVector.X);
	}
}

void ASK_CharacterBase::AddResource(const int32 ResourceNumIn, const int32 ResourceNumOut)
{
	if (!GetWorld()) return;

	const auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	if (GI)
	{
		GI->TrySpendResource(ResourceRowNameIn, ResourceNumIn);
		GI->AddResourceToStorage(ResourceRowNameOut, ResourceNumOut);

		if (DisplayPopupWidgetComponent
			&& !CurrentMachineName.IsNone())
		{
			FMachineType MachineType;
			auto IsGetCurrentMachine = GI->GetMachineBuyName(CurrentMachineName, MachineType);
			if (IsGetCurrentMachine)
			{
				auto ResourceType = GI->GetResourceTypeByName(MachineType.ResourceOutRowName);
				if (ResourceType)
				{
					auto Text = FString::Printf(TEXT("%i"),
						ResourceNumOut);
					DisplayPopupWidgetComponent->SetImageAndText(
						ResourceType->ShowingData.Image,
						Text);
				}
			}

			DisplayPopupWidgetComponent->Show();
		}

		if (CurrentMachineName != NAME_None)
		{
			OnGameComplitedByMName.Broadcast(CurrentMachineName);
		}
	}
}

void ASK_CharacterBase::AddEndurance(const int32 Endurance)
{
	if (!GetWorld()) return;

	const auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
	if (!GM) return;

	const auto Manager = GM->GetShipBuilder();
	if (Manager)
	{
		Manager->AddEnduranceByValue(Endurance);
	}
}

void ASK_CharacterBase::AnimationsBind()
{
	for (const auto& AnimSequence : AnimationMap)
	{
		for (const auto& Animation : AnimSequence.Value.AnimSequence)
		{
			const auto Notify = SK_Utils::FindNotifyByClass<USK_AnimNotify>(Animation);
			if (Notify)
			{
				Notify->OnNotifiedFTypeSignature.AddUObject(this, &ThisClass::OnAnimFinished);
			}
		}
	}
}

void ASK_CharacterBase::OnAnimFinished(USkeletalMeshComponent* SkeletalMesh)
{
	OnAnimationFinished.Broadcast();
}

void ASK_CharacterBase::ShowWorkWidget()
{
	const auto HUD = GetWorld()->GetFirstPlayerController()->GetHUD<ASK_GameHUD>();
	if (HUD)
	{
		auto GameWidget = HUD->GetGameWidget();
		if (GameWidget)
		{
			MiniGameWidge = GameWidget->GetMiniGameWidget();
			MiniGameWidge->ShowWidget();
		}
	}
}

void ASK_CharacterBase::HideWorkWidget()
{
	if (MiniGameWidge) MiniGameWidge->HideWidget();
}

void ASK_CharacterBase::ShowShipWidget()
{
	const auto HUD = GetWorld()->GetFirstPlayerController()->GetHUD<ASK_GameHUD>();
	if (HUD)
	{
		auto GameWidget = HUD->GetGameWidget();
		if (GameWidget)
		{
			GameWidget->ShowShipButton();
		}
	}
}

void ASK_CharacterBase::HideShipWidget()
{
	const auto HUD = GetWorld()->GetFirstPlayerController()->GetHUD<ASK_GameHUD>();
	if (HUD)
	{
		auto GameWidget = HUD->GetGameWidget();
		if (GameWidget)
		{
			GameWidget->HideShipButton();
		}
	}
}

void ASK_CharacterBase::ShowSlipwayWorkWidget()
{
	const auto HUD = GetWorld()->GetFirstPlayerController()->GetHUD<ASK_GameHUD>();
	if (HUD)
	{
		auto GameWidget = HUD->GetGameWidget();
		if (GameWidget)
		{
			MiniGameWidge = GameWidget->GetMiniGameWidget();
			MiniGameWidge->ShowSlipwayMiniGameButton();
		}
	}
}

void ASK_CharacterBase::HideSlipwayWorkWidget()
{
	const auto HUD = GetWorld()->GetFirstPlayerController()->GetHUD<ASK_GameHUD>();
	if (HUD)
	{
		auto GameWidget = HUD->GetGameWidget();
		if (GameWidget)
		{
			MiniGameWidge = GameWidget->GetMiniGameWidget();
			MiniGameWidge->HideSlipwayMiniGameButton();
		}
	}
}

const int32 ASK_CharacterBase::GetSlipwayLevel()
{
	if (GetWorld())
	{
		const auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
		if (!GM) return -1;

		const auto Level = GM->GetSlipwayLevel();
		return Level.GetIntValue();
	}
	return -1;
}

const FName ASK_CharacterBase::GetMashineName()
{
	if (MachineTile)
	{
		return MachineTile->GetMachineTypeName();
	}

	return NAME_None;
}

const FMachineLevelInfo ASK_CharacterBase::GetLevelInfo(const int32 Index)
{
	if (MachineTile)
	{
		return MachineTile->GetLevelInfo(Index);
	}
	return FMachineLevelInfo();
}

const int32 ASK_CharacterBase::GetCurrentLevel()
{
	if (MachineTile)
	{
		return MachineTile->GetCurrentLevel();
	}

	return -1;
}
