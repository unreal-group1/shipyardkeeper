// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/SK_StorageTile.h"
#include "Actors/SK_StorageActor.h"
#include "Framework/SK_PlatformGameInstance.h"
#include "SK_DisplayPopupWidgetComponent.h"
#include "SK_PopupHintWidgetComponent.h"

#include "SKW_BasePopup.h"
#include "SKW_GetResourceProgressBar.h"
#include "SKW_PopupWithProgressBarSimulate.h"
#include "SK_GameMode.h"

DEFINE_LOG_CATEGORY_STATIC(ASK_StorageTileLog, All, All);

void ASK_StorageTile::BeginPlay()
{
	Super::BeginPlay();

	check(StorageActorClass);
}

void ASK_StorageTile::SetStorageActorsToGM()
{
	//
}

ASK_StorageTile::ASK_StorageTile()
{
	DisplayPopupWidgetComponent = CreateDefaultSubobject<USK_DisplayPopupWidgetComponent>(TEXT("DisplayPopupWidgetComponent"));
	if (DisplayPopupWidgetComponent)
	{
		DisplayPopupWidgetComponent->AttachToComponent(SceneComponent,
			FAttachmentTransformRules::KeepRelativeTransform);
	}

	ProgressBarWidgetComponent = CreateDefaultSubobject<USK_PopupHintWidgetComponent>(TEXT("ProgressBarWidgetComponent"));
	if (ProgressBarWidgetComponent)
	{
		ProgressBarWidgetComponent->AttachToComponent(SceneComponent,
			FAttachmentTransformRules::KeepRelativeTransform);
	}
}

void ASK_StorageTile::CreateStorageActor(const FStorageType&  StorageInfo)
{
	if (!GetWorld()) return;

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	const FVector Location = GetActorLocation() + StorageInfo.SpawnOffset;
	const auto StorageActor = GetWorld()->SpawnActor<ASK_StorageActor>(StorageActorClass, Location,FRotator(), SpawnParameters);
	if (StorageActor)
	{
		StorageActor->Init(StorageInfo);
		StorageActor->SetOwner(this);

		StorageActors.Add(StorageActor);
		const auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
		if (GM)
		{
			GM->SetStorageActors(StorageActor);
		}
		CurrentStorageActor = StorageActor;
	}
}

void ASK_StorageTile::SpawnStorageByName(const FName& StorageRowName)
{
	const auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	FStorageType Storage;
	if (GI)
	{
		if (GI->GetStorageBuyName(StorageRowName, Storage)) CreateStorageActor(Storage);
	}
	else
	{
		UE_LOG(ASK_StorageTileLog, Error, TEXT("Row Name not found"));
	}
}
