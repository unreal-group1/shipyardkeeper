// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SK_ClickableAndTouchableActor.h"
#include "SK_Slipway.generated.h"

class USK_UpSlipwayWidget;
class USK_SlipwayWidgetComponent;
class UStaticMesh;
class UBoxComponent;

USTRUCT()
struct FWorkplace
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "Location", meta = (MakeEditWidget = true))
	FVector Location;

	bool Employed{ false };
};

UCLASS()
class SHIPYARDKEEPER_API ASK_Slipway : public ASK_ClickableAndTouchableActor
{
	GENERATED_BODY()
	
public:	
	ASK_Slipway();

	//bool CanWork();

	bool CanWork(FVector& Workplace,int32& index,TObjectPtr<ASK_Slipway>& SlipwayActor);
	bool IsCanWork();

	void ClearWorkplace(const uint32 Index);

	void InitUpSlipwayWidget();

	void ShowWidget();
	void HideWidget();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite,Category = "Components")
	USK_SlipwayWidgetComponent* WidgetComponent;

	UPROPERTY(EditDefaultsOnly, Category = "UI")
	TSubclassOf<USK_UpSlipwayWidget> UpSlipwayWidgetClass;

	UPROPERTY(EditAnywhere,Category = "Work")
	TArray<FWorkplace> Workplaces;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widgets)
	TSubclassOf<class USKW_Ship> ShipWidgetClass;

	UPROPERTY(EditDefaultsOnly,Category = "Mesh")
	TObjectPtr<UStaticMesh> DefaultsMesh;

	UPROPERTY(EditDefaultsOnly, Category = "Mesh")
	TObjectPtr<UStaticMesh> ActiveMesh;

	UPROPERTY(VisibleAnywhere,Category = "Components")
	UBoxComponent* BoxCollision;

protected:
	UFUNCTION()
	void OnOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
