// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SK_ClickableAndTouchableActor.h"
#include "SK_Types.h"
#include "SK_StorageActor.generated.h"

class UStaticMeshComponent;
class USK_PlatformGameInstance;
class USkeletalMeshComponent;
class USK_DisplayPopupWidgetComponent;
class USKW_GetResourceProgressBar;

UCLASS()
class SHIPYARDKEEPER_API ASK_StorageActor : public ASK_ClickableAndTouchableActor
{
	GENERATED_BODY()
	
public:	
	ASK_StorageActor();

	void SetOwner(AActor* NewOwner) override;

	void Init(const FStorageType&  StorageInfo);

	bool LevelUp();

	uint8 GetCurrentLevel();

	FText GetStorageName();

	FStorageType GetStorageType();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	USK_DisplayPopupWidgetComponent* DisplayPopupWidgetComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	USK_PopupHintWidgetComponent* ProgressBarWidgetComponent;

protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	void CreateProducts();
	virtual void DoOnWidgetButton() override;

	void UpdateProgressBar();
	void RestartProgressBar();
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Component")
	USkeletalMeshComponent* SkeletalMeshComponent;
private:
	FText StorageName;

	FName ResourceOutRowName = NAME_None;

	double Quantity{ 0 };

	float CurrentTickInterval{ 5.f };

	uint8 CurrentLevel{ 0 };

	uint32 PricePerUnit{ 0 };

	FTimerHandle CreateProductTimer;

	FStorageType StorageType;

	UPROPERTY()
	TObjectPtr<USK_PlatformGameInstance> PlatformGameInstance;

	class ASK_StorageTile* CurrentTile;
	float ResourceShowTreashold = 0.02f;
};
