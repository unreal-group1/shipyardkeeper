// Fill out your copyright notice in the Description page of Project Settings.

#include "SK_TileActorBase.h"
#include "SK_PopupHintWidgetComponent.h"
#include "SKW_ButtonBuyTile.h"
#include "SKW_ButtonUpgradeMachine.h"

ASK_TileActorBase::ASK_TileActorBase()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickInterval = 1.f;
}

void ASK_TileActorBase::SetTilePosition(FTilePositionData NewTilePosition)
{
	TilePosition = NewTilePosition;
}

void ASK_TileActorBase::BeginPlay()
{
	Super::BeginPlay();
}

FTilePositionData::FTilePositionData()
{
}

FTilePositionData::FTilePositionData(ASK_TileActorBase* NewTile,
	uint8 NewIndexW, 
	uint8 NewIndexH)
{
	CurrentTile = NewTile;
	IndexW = NewIndexW;
	IndexH = NewIndexH;
}

FTilePositionData::FTilePositionData(ASK_TileActorBase* NewTile, uint8 NewIndexH)
{
	CurrentTile = NewTile;
	IndexW = -1;
	IndexH = NewIndexH;
}
