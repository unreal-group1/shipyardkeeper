// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "SKI_ResourceContaining.h"
#include "SK_Types.h"
#include "SK_ClickableAndTouchableActor.h"
#include "SK_ResourceShower.generated.h"

UCLASS()
class SHIPYARDKEEPER_API ASK_ResourceShower : public ASK_ClickableAndTouchableActor, 
	public ISKI_ResourceContaining
{
	GENERATED_BODY()
	
public:	
	ASK_ResourceShower();

	virtual void SetResourceQuantity(double NewQuantity) override;
	virtual void SetResourceType(FResourceType NewResourceType) override;
	virtual void UpdateResourceQuantity(FName ResourceName, double NewQuantity) override;

protected:
	virtual void BeginPlay() override;

private:
	FResourceType ResourceType;
	double Quantity = 0.0f;
	UPROPERTY()
	TObjectPtr <class USK_PlatformGameInstance > PlatformGameInstance;
	bool IsAlreadyBeenShown = false;
};
