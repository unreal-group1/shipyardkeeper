// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/SK_TileActorBase.h"
#include "SK_Types.h"
#include "SK_StorageTile.generated.h"

class ASK_StorageActor;
class USK_DisplayPopupWidgetComponent;
class USKW_GetResourceProgressBar;

UCLASS()
class SHIPYARDKEEPER_API ASK_StorageTile : public ASK_TileActorBase
{
	GENERATED_BODY()
	
public:
	ASK_StorageTile();

	void CreateStorageActor(const FStorageType&  StorageInfo);

	void SpawnStorageByName(const FName& StorageRowName);

	const TObjectPtr<ASK_StorageActor>& GetCurrentStorageActor() { return CurrentStorageActor; }

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	USK_DisplayPopupWidgetComponent* DisplayPopupWidgetComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	USK_PopupHintWidgetComponent* ProgressBarWidgetComponent;

	UPROPERTY()
	TArray<TObjectPtr<ASK_StorageActor>> StorageActors;
protected:
	virtual void BeginPlay() override;

	void SetStorageActorsToGM();
protected:
	UPROPERTY(EditAnywhere,Category = "StorageActor")
	TSubclassOf<ASK_StorageActor> StorageActorClass;

	UPROPERTY(EditAnywhere, Category = "StorageActor")
	TObjectPtr<ASK_StorageActor> CurrentStorageActor;
};
