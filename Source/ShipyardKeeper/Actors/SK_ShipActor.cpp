// Fill out your copyright notice in the Description page of Project Settings.

#include "SK_ShipActor.h"
#include "SK_PopupHintWidgetComponent.h"
#include "Actors/SK_DShaderBaseActor.h"

ASK_ShipActor::ASK_ShipActor()
{
	PrimaryActorTick.bCanEverTick = false;

	if (PopupHintWidgetComponent)
	{ 
		PopupHintWidgetComponent->Show();
	}

	StaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

}

void ASK_ShipActor::BeginPlay()
{
	Super::BeginPlay();
}

void ASK_ShipActor::Complited(const float Value)
{
	GetWorldTimerManager().SetTimer(ChangeLocationHandle, this, &ASK_ShipActor::SailAway, 0.03, true);
	SetLifeSpan(Value);
	ChaderBoxActor->Destroy();

	if (PopupHintWidgetComponent)
	{
		PopupHintWidgetComponent->Hide();
	}
}

void ASK_ShipActor::ChaderBoxUpdate(const float Percent)
{
	const auto NewZLocation = FMath::Lerp(GetActorLocation().Z + ShipHeight * 0.5, GetActorLocation().Z + ShipHeight * 1.5, Percent);
	FVector Location = GetActorLocation();
	Location.Z = NewZLocation;

	ChaderBoxActor->SetActorLocation(Location);
	ChaderBoxActor->ChangeLocation();
}

void ASK_ShipActor::SailAway()
{
	SetActorLocation(GetActorLocation() + FVector(5.0, 0.0, 0.0));
}

void ASK_ShipActor::SetStaticMesh(UStaticMesh* Mesh)
{
	StaticMeshComponent->SetStaticMesh(Mesh);

	const auto Box = StaticMeshComponent->GetStaticMesh()->GetBoundingBox();
	ShipHeight = Box.GetSize().Z;

	if (!GetWorld()) return;

	FVector SpawnLocation = GetActorLocation();
	SpawnLocation.Z = GetActorLocation().Z + ShipHeight*0.5;

	FActorSpawnParameters ActorSpawnParameters;
	ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	ChaderBoxActor = GetWorld()->SpawnActor<ASK_DShaderBaseActor>(ChaderBoxActorClass, SpawnLocation, FRotator::ZeroRotator, ActorSpawnParameters);

	const double X = Box.GetSize().X / 100 + 1;
	const double Y = Box.GetSize().Y / 100 + 1;
	const double Z = Box.GetSize().Z / 100;

	ChaderBoxActor->SetActorScale3D(FVector(X, Y, Z));
	ChaderBoxActor->ChangeLocation();
}
