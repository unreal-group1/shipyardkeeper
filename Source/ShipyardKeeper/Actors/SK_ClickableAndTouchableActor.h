// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "SK_ClickableAndTouchableActor.generated.h"

class UStaticMeshComponent;
class USK_PopupHintWidgetComponent;

UCLASS()
class SHIPYARDKEEPER_API ASK_ClickableAndTouchableActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ASK_ClickableAndTouchableActor();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Component")
	UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Component")
	USK_PopupHintWidgetComponent* PopupHintWidgetComponent;

	UFUNCTION()
	virtual void DoOnWidgetButton();

protected:
	UPROPERTY(VisibleAnywhere, Category = "Component")
	USceneComponent* SceneComponent;

protected:
	virtual void BeginPlay() override;
};
