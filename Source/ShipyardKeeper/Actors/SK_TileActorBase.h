// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "SK_ClickableAndTouchableActor.h"
#include "SK_Types.h"
#include "SK_TileActorBase.generated.h"

class UStaticMeshComponent;
class UWidgetComponent;
class ASK_TileActorBase;

USTRUCT(BlueprintType)
struct FTilePositionData
{
	GENERATED_USTRUCT_BODY()

	public:
	FTilePositionData();
	FTilePositionData(ASK_TileActorBase* NewTile,
		uint8 NewIndexW,
		uint8 NewIndexH);

	FTilePositionData(ASK_TileActorBase* NewTile,
		uint8 NewIndexH);

	UPROPERTY()
	ASK_TileActorBase* CurrentTile;
	UPROPERTY()
	uint8 IndexW;
	UPROPERTY()
	uint8 IndexH;
};

UCLASS()
class SHIPYARDKEEPER_API ASK_TileActorBase : public ASK_ClickableAndTouchableActor
{
	GENERATED_BODY()
	
public:	
	ASK_TileActorBase();
	void SetTilePosition(FTilePositionData NewTilePosition);

protected:
	virtual void BeginPlay() override;

private:
	ETileState TileState;
	FTilePositionData TilePosition;
};