// Created by Pavlov P.A.

#include "SK_ClickableAndTouchableActor.h"
#include "SK_PopupHintWidgetComponent.h"

#include "Components/StaticMeshComponent.h"

ASK_ClickableAndTouchableActor::ASK_ClickableAndTouchableActor()
{
	PrimaryActorTick.bCanEverTick = false;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	if (SceneComponent)
	{
		SetRootComponent(SceneComponent);
	}

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	if (StaticMeshComponent)
	{
		if (SceneComponent)
		{
			StaticMeshComponent->SetupAttachment(SceneComponent);
		}

		StaticMeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	}

	PopupHintWidgetComponent = CreateDefaultSubobject<USK_PopupHintWidgetComponent>(TEXT("PopupHintWidgetComponent"));
	if (PopupHintWidgetComponent)
	{
		PopupHintWidgetComponent->SetupAttachment(SceneComponent);
	}
}

void ASK_ClickableAndTouchableActor::DoOnWidgetButton()
{
}

void ASK_ClickableAndTouchableActor::BeginPlay()
{
	Super::BeginPlay();
}
