// Fill out your copyright notice in the Description page of Project Settings.

#include "SK_StorageActor.h"
#include "SK_PlatformGameInstance.h"
#include "SK_DisplayPopupWidgetComponent.h"
#include "SK_PopupHintWidgetComponent.h"
#include "SK_StorageTile.h"

#include "SKW_GetResourceProgressBar.h"

#include "Blueprint/UserWidget.h"
#include "Components/SkeletalMeshComponent.h"


DEFINE_LOG_CATEGORY_STATIC(ASK_StorageActorLog, All, All);

ASK_StorageActor::ASK_StorageActor()
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.TickInterval = 1.f;

	SkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshComponent"));
	SkeletalMeshComponent->SetupAttachment(SceneComponent);
}

void ASK_StorageActor::SetOwner(AActor* NewOwner)
{
	if (NewOwner)
	{
		Super::SetOwner(NewOwner);

		CurrentTile = Cast<ASK_StorageTile>(NewOwner);

		if (CurrentTile
			&& CurrentTile->ProgressBarWidgetComponent)
		{
			auto PBWidgetClass = CurrentTile->ProgressBarWidgetComponent->GetWidgetClass();
			if (PBWidgetClass)
			{
				USKW_GetResourceProgressBar* NewWidget = CreateWidget<USKW_GetResourceProgressBar>(GetWorld(),
					PBWidgetClass);
				CurrentTile->ProgressBarWidgetComponent->SetWidget(NewWidget);

				CurrentTile->ProgressBarWidgetComponent->InitPopupHint();
			}

			UpdateProgressBar();

			if (StorageType.StorageLevels[CurrentLevel].ResourceNum == 0)
			{
				if (CurrentTile
					&& CurrentTile->ProgressBarWidgetComponent)
				{
					CurrentTile->ProgressBarWidgetComponent->Hide();
				}
				GetWorldTimerManager().ClearTimer(CreateProductTimer);
				return;
			}

			CurrentTile->ProgressBarWidgetComponent->Show();
		}
	}
}

void ASK_StorageActor::BeginPlay()
{
	Super::BeginPlay();

	if (!GetWorld()) return;

	PlatformGameInstance = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	if (!PlatformGameInstance)
	{
		UE_LOG(ASK_StorageActorLog, Error, TEXT("PlatformGameInstance is nullptr!"));
	}
}

void ASK_StorageActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASK_StorageActor::CreateProducts()
{
	double ResourseQuantity = Quantity;

	if (!PlatformGameInstance->TryToBuy(PricePerUnit))
	{
		ResourseQuantity *= 0.5;
	}

	PlatformGameInstance->AddResourceToStorage(ResourceOutRowName, ResourseQuantity);
	UE_LOG(ASK_StorageActorLog, Display, TEXT("Resourse %s, Quantity %f, added to storage"), *ResourceOutRowName.ToString(), ResourseQuantity);

	if (!GetWorld()) return;
	auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();

	if (CurrentTile)
	{
		if (CurrentTile->DisplayPopupWidgetComponent)
		{
			CurrentTile->DisplayPopupWidgetComponent->Hide();
			if (ResourseQuantity > ResourceShowTreashold)
			{
				auto ResourceType = GI->GetResourceTypeByName(ResourceOutRowName);
				if (ResourceType)
				{
					CurrentTile->DisplayPopupWidgetComponent->SetImageAndText(
						ResourceType->ShowingData.Image,
						FString::Printf(TEXT("%.0f"),
							ResourseQuantity));
				}

				CurrentTile->DisplayPopupWidgetComponent->Show();
			}
		}

		RestartProgressBar();
	}
}

void ASK_StorageActor::DoOnWidgetButton()
{
	CreateProducts();
}

void ASK_StorageActor::UpdateProgressBar()
{
	if (!GetWorld()) return;
	auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	if (CurrentTile)
	{
		if (CurrentTile->ProgressBarWidgetComponent)
		{
			auto Resource = GI->GetResourceTypeByName(ResourceOutRowName);

			if (Resource)
			{
				if (CurrentTile->ProgressBarWidgetComponent->GetWidget())
				{
					auto ProgressBarSimulate = Cast<USKW_GetResourceProgressBar>(CurrentTile->ProgressBarWidgetComponent->GetWidget());
					if (ProgressBarSimulate)
					{
						ProgressBarSimulate->SetImageFromTexture(Resource->ShowingData.Image);
						ProgressBarSimulate->SetTimeSimulate(CurrentTickInterval);
						ProgressBarSimulate->StartSimulate();
					}
				}

				CurrentTile->ProgressBarWidgetComponent->Show();
			}
		}
	}
}

void ASK_StorageActor::RestartProgressBar()
{
	if (!GetWorld()) return;
	auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	if (CurrentTile)
	{
		if (CurrentTile->ProgressBarWidgetComponent)
		{
			auto Resource = GI->GetResourceTypeByName(ResourceOutRowName);

			if (Resource)
			{
				CurrentTile->ProgressBarWidgetComponent->Hide();

				if (CurrentTile->ProgressBarWidgetComponent->GetWidget())
				{
					auto ProgressBarSimulate = Cast<USKW_GetResourceProgressBar>(CurrentTile->ProgressBarWidgetComponent->GetWidget());
					if (ProgressBarSimulate)
					{
						ProgressBarSimulate->ResetTimeSimulate();
					}
				}

				CurrentTile->ProgressBarWidgetComponent->Show();
			}
		}
	}
}

void ASK_StorageActor::Init(const FStorageType&  StorageInfo)
{
	if (SkeletalMeshComponent)
	{
		SkeletalMeshComponent->SetSkeletalMesh(StorageInfo.SkeletalMesh);
	}

	if (StorageInfo.StorageLevels.IsValidIndex(0))
	{
		CurrentTickInterval = StorageInfo.StorageLevels[0].TickInterval;
		Quantity = StorageInfo.StorageLevels[0].ResourceNum;
		if (Quantity != 0)
		{
			GetWorldTimerManager().SetTimer(CreateProductTimer, this, &ASK_StorageActor::CreateProducts, CurrentTickInterval, true);
		}
		CurrentLevel = 0;
	}
	
	StorageType = StorageInfo;
	StorageName = StorageInfo.StorageName;
	ResourceOutRowName = StorageInfo.ResourceOutRowName;
	PricePerUnit = StorageInfo.PricePerUnit;

	UpdateProgressBar();
}

bool ASK_StorageActor::LevelUp()
{
	if (!GetWorld()) return false;

	const auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	if (GI)
	{
		const auto NextIndex = CurrentLevel + 1;

		if (!StorageType.StorageLevels.IsValidIndex(NextIndex)) return false;
		if (!GI->GetStoragePrice().IsValidIndex(NextIndex)) return false;

		if (GI->TryToBuy(GI->GetStoragePrice()[CurrentLevel]))
		{
			CurrentLevel++;

			CurrentTickInterval = StorageType.StorageLevels[CurrentLevel].TickInterval;
			Quantity = StorageType.StorageLevels[CurrentLevel].ResourceNum;

			GetWorldTimerManager().ClearTimer(CreateProductTimer);
			if (StorageType.StorageLevels[CurrentLevel].ResourceNum == 0)
			{
				if (CurrentTile
					&& CurrentTile->ProgressBarWidgetComponent)
				{
					CurrentTile->ProgressBarWidgetComponent->Hide();
				}

				return false;
			}
			GetWorldTimerManager().SetTimer(CreateProductTimer, this, &ASK_StorageActor::CreateProducts, CurrentTickInterval, true);

			UpdateProgressBar();
			return true;
		}
	}

	return false;
}

uint8 ASK_StorageActor::GetCurrentLevel()
{
	return CurrentLevel;
}

FText ASK_StorageActor::GetStorageName()
{
	return StorageName;
}

FStorageType ASK_StorageActor::GetStorageType()
{
	return StorageType;
}

