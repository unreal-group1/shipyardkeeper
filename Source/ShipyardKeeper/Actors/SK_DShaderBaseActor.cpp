// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/SK_DShaderBaseActor.h"

ASK_DShaderBaseActor::ASK_DShaderBaseActor()
{
	PrimaryActorTick.bCanEverTick = false;

}

void ASK_DShaderBaseActor::ChangeLocation()
{
	OnChangeLocation.Broadcast();
}

void ASK_DShaderBaseActor::BeginPlay()
{
	Super::BeginPlay();
	
}
