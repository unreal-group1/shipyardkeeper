// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SK_ResourceShowerSpawner.generated.h"

class ASK_ResourceShower;
class USK_PlatformGameInstance;

UCLASS()
class SHIPYARDKEEPER_API ASK_ResourceShowerSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	ASK_ResourceShowerSpawner();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ResourceShowerSpawner")
	int32 DistanceBetweenResourceShower = 250;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ResourceShowerSpawner")
	int32 HeightResourceShower = 250;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ResourceShowerSpawner")
	int32 HalfWallWidth = 9;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ResourceShowerSpawner")
	int32 OffsetLeftHighRow = 640;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ResourceShowerSpawner")
	int32 OffsetBottomHighRow = 180;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ResourceShower")
	TSubclassOf<ASK_ResourceShower> ResourceShowerClass;

private:
	UPROPERTY()
	TObjectPtr<USK_PlatformGameInstance> PlatformGameInstance;
	UPROPERTY()
	TArray<ASK_ResourceShower*> ResourceShowers;
};
