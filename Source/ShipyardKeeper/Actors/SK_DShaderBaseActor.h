// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SK_DShaderBaseActor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChangeLocation);

UCLASS()
class SHIPYARDKEEPER_API ASK_DShaderBaseActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ASK_DShaderBaseActor();

	UPROPERTY(BlueprintAssignable)
	FOnChangeLocation OnChangeLocation;

	void ChangeLocation();
protected:
	virtual void BeginPlay() override;


};
