// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "SK_ClickableAndTouchableActor.h"
#include "SK_Types.h"
#include "SK_MachineActorBase.generated.h"

class USkeletalMeshComponent;
class USK_PlatformGameInstance;
class ASK_MachineTile;
class ASK_AIPawnBase;
class UAnimMontage;

UCLASS()
class SHIPYARDKEEPER_API ASK_MachineActorBase : public ASK_ClickableAndTouchableActor
{
	GENERATED_BODY()
	
public:	
	ASK_MachineActorBase();

	void Init(const FMachineType& MachineType, ASK_MachineTile* MachineTile,const FRotator& LocRotation);

	bool CanWork(FVector& OutWorkLocation);

	void UpdateVacancy(bool Vacancy,const TObjectPtr<ASK_AIPawnBase>& Pawn);

	UFUNCTION(BlueprintCallable)
	const FMachineLevelInfo& GetLevelInfo() { return CurrentLevelInfo; }

	UFUNCTION(BlueprintCallable)
	const int32 GetMachineLevel();

	void SetLevelInfo(const FMachineLevelInfo& Info) { CurrentLevelInfo = Info; }
	const FName& GetResourseInName() { return ResourceInRowName; }
	const FName& GetResourseOutName() { return ResourceOutRowName; }

	void CreateProducts();

	void UpdateLocation(const FVector& NewLocation);
	void UpdateIsAtTheWorkplace(bool Value) { IsAtTheWorkplace = Value; }

	const FAnimInfo& GetAnimInfo() { return AnimInfo; }

	void PlayAnim();
	const FVector GetWorkLocation();

	const FMachineType& GetType() { return CurrentType; }

	TObjectPtr<ASK_AIPawnBase> GetCurrentAIPawn();
protected:
	UPROPERTY(EditAnywhere, Category = "NPS", meta = (MakeEditWidget = true))
	FVector WorkLocation;

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, Category = "Components")
	USkeletalMeshComponent* SkeletalMeshComponent;

protected:
	virtual void BeginPlay() override;

	void UpdateStats(const FMachineLevelInfo& Info);
	
	void AnimTick();
	void AnimEnd();
	void PlayMontage(const TObjectPtr<UAnimMontage>& Montage);
private:
	FMachineType CurrentType;
	FMachineLevelInfo CurrentLevelInfo;
	FAnimInfo AnimInfo;

	bool OnPause{ false };

	bool bIsVacancy{ true };

	FText MachineName;

	FName ResourceInRowName = NAME_None;

	FName ResourceOutRowName = NAME_None;

	UPROPERTY()
	TObjectPtr<USK_PlatformGameInstance> PlatformGameInstance;

	UPROPERTY()
	TObjectPtr<ASK_AIPawnBase> CurrentAIPawn;

	UPROPERTY()
	TObjectPtr<ASK_MachineTile> MachineTile;

	bool IsAtTheWorkplace{ false };

	FTimerHandle AnimTimerHandle;
	FTimerHandle FinishAnimTimerHandle;

	uint32 NumOfLoop{ 0 };
	uint32 CurrentCycle{ 0 };
};
