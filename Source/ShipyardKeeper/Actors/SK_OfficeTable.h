// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SK_OfficeTable.generated.h"

class UStaticMeshComponent;
class UBoxComponent;

UCLASS()
class SHIPYARDKEEPER_API ASK_OfficeTable : public AActor
{
	GENERATED_BODY()
	
public:	
	ASK_OfficeTable();

protected:
	virtual void BeginPlay() override;

protected:
	UPROPERTY(VisibleAnywhere, Category = "Components")
	USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere,Category = "Components")
	UStaticMeshComponent* MeshComponents;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UBoxComponent* BoxComponent;

private:
	UFUNCTION()
	void OnOverlaped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
