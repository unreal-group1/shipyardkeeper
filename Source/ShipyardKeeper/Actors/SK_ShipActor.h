// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SK_ClickableAndTouchableActor.h"
#include "SK_ShipActor.generated.h"

class UStaticMesh;
class ASK_DShaderBaseActor;

UCLASS()
class SHIPYARDKEEPER_API ASK_ShipActor : public ASK_ClickableAndTouchableActor
{
	GENERATED_BODY()
	
public:	
	ASK_ShipActor();

	virtual void BeginPlay() override;

	void Complited(const float Value);

	void ChaderBoxUpdate(const float Percent);
	void SailAway();

	void SetStaticMesh(UStaticMesh* Mesh);
private:
	UPROPERTY(EditDefaultsOnly,Category = "Shader")
	TSubclassOf<ASK_DShaderBaseActor> ChaderBoxActorClass;

	UPROPERTY()
	TObjectPtr<ASK_DShaderBaseActor> ChaderBoxActor;

	FTimerHandle ChangeLocationHandle;

	float ShipHeight{ 0.f };
};
