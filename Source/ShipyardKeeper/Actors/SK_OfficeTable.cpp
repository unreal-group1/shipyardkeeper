// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/SK_OfficeTable.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "SK_CharacterBase.h"

ASK_OfficeTable::ASK_OfficeTable()
{
	PrimaryActorTick.bCanEverTick = false;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);

	MeshComponents = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	MeshComponents->SetupAttachment(SceneComponent);

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxComponent->SetupAttachment(SceneComponent);
}

void ASK_OfficeTable::BeginPlay()
{
	Super::BeginPlay();
	
	if (BoxComponent)
	{
		BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ASK_OfficeTable::OnOverlaped);
		BoxComponent->OnComponentEndOverlap.AddDynamic(this, &ASK_OfficeTable::OnEndOverlap);
	}
}

void ASK_OfficeTable::OnOverlaped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	const auto Player = Cast<ASK_CharacterBase>(OtherActor);
	if (Player)
	{
		Player->ShowShipWidget();
	}
}

void ASK_OfficeTable::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	const auto Player = Cast<ASK_CharacterBase>(OtherActor);
	if (Player)
	{
		Player->HideShipWidget();
	}
}
