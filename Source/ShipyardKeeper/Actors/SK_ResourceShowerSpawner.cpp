// Created by Pavlov P.A.

#include "SK_ResourceShowerSpawner.h"
#include "SK_ResourceShower.h"
#include "SK_PlatformGameInstance.h"

ASK_ResourceShowerSpawner::ASK_ResourceShowerSpawner()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ASK_ResourceShowerSpawner::BeginPlay()
{
	Super::BeginPlay();
	
    if (!PlatformGameInstance)
    {
        if (GetGameInstance())
        {
            PlatformGameInstance = Cast<USK_PlatformGameInstance>(GetGameInstance());
        }
    }

    if (PlatformGameInstance)
    {
        TArray<FResourceType*> ResourceTypes = PlatformGameInstance->GetResourceTypesWithOriginalNames();

        uint8 IndexResource = 0;
        FActorSpawnParameters SpawnParameters;
        SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

        for (auto ResourceType : ResourceTypes)
        {
            if (GetWorld())
            {
                FVector Location = GetActorLocation() + FVector(HalfWallWidth, DistanceBetweenResourceShower * IndexResource, HeightResourceShower);
                ASK_ResourceShower* ResourceShower = Cast<ASK_ResourceShower>(
                    GetWorld()->SpawnActor<ASK_ResourceShower>(ResourceShowerClass, Location, FRotator(0.0f, 180.0f, 0.0f), SpawnParameters));
                if (ResourceShower)
                {
                    auto Result = PlatformGameInstance->RawResourceNames.FindByPredicate(
                    [&](const FText Item)
                    {
                        return Item.EqualTo(ResourceType->ShowingData.RowName);
                    });
                    if (Result)
                    {
                        auto NewLocation = Location;
                        NewLocation = FVector(NewLocation.X,
                            NewLocation.Y + OffsetLeftHighRow,
                            NewLocation.Z + OffsetBottomHighRow);
                        ResourceShower->SetActorLocation(NewLocation);
                    }
                    double ResourceQuantity = 0;
                    if (PlatformGameInstance->GetResourceQuantity(*ResourceType->ShowingData.RowName.ToString()) >= 0)
                    {
                        ResourceQuantity = PlatformGameInstance->GetResourceQuantity(*ResourceType->ShowingData.RowName.ToString());
                    }
                    else
                    {
                        ResourceQuantity = 0;
                    }
                    ResourceShowers.Add(ResourceShower);
                    ResourceShower->SetResourceQuantity(ResourceQuantity);
                    ResourceShower->SetResourceType(*ResourceType);
                }
            }
            IndexResource++;
        }
    }
}