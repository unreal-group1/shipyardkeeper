// Created by Pavlov P.A.

#include "SK_ResourceShower.h"
#include "SK_PopupHintWidgetComponent.h"
#include "SK_PlatformGameInstance.h"
#include "SK_ShipBuilder.h"

#include "SKW_BasePopup.h"

#include "SKI_ResourceContaining.h"
#include "SKI_HasImage.h"
#include "SKI_HasText.h"

#include "Components/Image.h"

ASK_ResourceShower::ASK_ResourceShower()
{
 	PrimaryActorTick.bCanEverTick = false;

    StaticMeshComponent->DestroyComponent();
}

void ASK_ResourceShower::SetResourceQuantity(double NewQuantity)
{
    Quantity = NewQuantity;

    if (PopupHintWidgetComponent)
    {
        if (PopupHintWidgetComponent->PopupHint)
        {
            auto Widget = Cast<ISKI_HasText>(PopupHintWidgetComponent->PopupHint);
            if (Widget)
            {
                if (NewQuantity >= 0)
                {
                    Widget->SetText(FText::FromString(FString::Printf(TEXT("%.0f"), NewQuantity)));
                }
                else
                {
                    Widget->SetText(FText::FromString(FString::Printf(TEXT("%.0f"), 0)));
                }
            }

            if (NewQuantity <= 0)
            {
                if (!IsAlreadyBeenShown)
                {
                    PopupHintWidgetComponent->Hide();
                    IsAlreadyBeenShown = true;
                }
            }
            else
            if (PopupHintWidgetComponent->PopupHint->GetVisibility() != ESlateVisibility::SelfHitTestInvisible)
            {
                PopupHintWidgetComponent->Show();
                IsAlreadyBeenShown = true;
            }
        }
    }
}

void ASK_ResourceShower::SetResourceType(FResourceType NewResourceType)
{
    ResourceType = NewResourceType;

    UImage* NewImage = NewObject<UImage>(UImage::StaticClass());
    if (NewImage)
    {
        NewImage->SetBrushFromTexture(ResourceType.ShowingData.Image, true);

        if (PopupHintWidgetComponent
            && PopupHintWidgetComponent->PopupHint)
        {
            auto Widget = Cast<ISKI_HasImage>(PopupHintWidgetComponent->PopupHint);
            if (Widget)
            {
                Widget->SetImage(NewImage);
            }
        }
    }
}

void ASK_ResourceShower::UpdateResourceQuantity(FName ResourceName, 
    double NewQuantity)
{
    if (ResourceType.ShowingData.RowName.EqualTo(FText::FromName(ResourceName)))
    {
        SetResourceQuantity(NewQuantity);

        if (PopupHintWidgetComponent
            && PopupHintWidgetComponent->PopupHint)
        {
            auto Widget = Cast<ISKI_HasText>(PopupHintWidgetComponent->PopupHint);
            if (Widget)
            {
                if (NewQuantity >= 0)
                {
                    Widget->SetText(FText::FromString(FString::Printf(TEXT("%.0f"), NewQuantity)));
                }
                else
                {
                    if (!IsAlreadyBeenShown)
                    {
                        PopupHintWidgetComponent->Hide();
                        IsAlreadyBeenShown = true;
                    }
                    else
                    {
                        PopupHintWidgetComponent->Show();
                    }
                    Widget->SetText(FText::FromString(FString::Printf(TEXT("%.0f"), 0)));
                }
            }
        }
    }
}

void ASK_ResourceShower::BeginPlay()
{
    Super::BeginPlay();

    if (!PlatformGameInstance)
    {
        if (GetGameInstance())
        {
            PlatformGameInstance = Cast<USK_PlatformGameInstance>(GetGameInstance());
        }
    }

    if (PlatformGameInstance)
    {
        PlatformGameInstance->OnResourceCountChange.AddDynamic(this, &ASK_ResourceShower::UpdateResourceQuantity);
    }

    if (PopupHintWidgetComponent)
    {
        if (PopupHintWidgetComponent->PopupHint)
        {
            auto Widget = Cast<ISKI_HasText>(PopupHintWidgetComponent->PopupHint);
            if (Widget)
            {
                Widget->SetText(FText::FromString(FString::Printf(TEXT("%.0f"), 0)));
            }
        }

        PopupHintWidgetComponent->SetWidgetSpace(EWidgetSpace::World);

        PopupHintWidgetComponent->Hide();
    }
}
