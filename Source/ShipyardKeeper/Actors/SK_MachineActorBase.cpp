// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/SK_MachineActorBase.h"
#include "Components/StaticMeshComponent.h"
#include "Framework/SK_PlatformGameInstance.h"
#include "SK_GameMode.h"
#include "Actors/SK_MachineTile.h"
#include "Components/SkeletalMeshComponent.h"
#include "AI/Pawn/SK_AIPawnBase.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"

DEFINE_LOG_CATEGORY_STATIC(ASK_MachineActorBaseLog, All, All);

ASK_MachineActorBase::ASK_MachineActorBase()
{
	PrimaryActorTick.bCanEverTick = false;

	SkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshComponent"));
	SkeletalMeshComponent->SetupAttachment(SceneComponent);
}

void ASK_MachineActorBase::Init(const FMachineType& MachineType, ASK_MachineTile* InMachineTile, const FRotator& LocRotation)
{
	if (StaticMeshComponent) StaticMeshComponent->DestroyComponent();

	MachineTile = InMachineTile;
	CurrentType = MachineType;
	WorkLocation = MachineType.WorkLocation;
	SkeletalMeshComponent->SetSkeletalMesh(MachineType.SkeletalMesh);

	MachineName = MachineType.MachineName;
	ResourceInRowName = MachineType.ResourceInRowName;
	ResourceOutRowName = MachineType.ResourceOutRowName;
	WorkLocation = LocRotation.RotateVector(WorkLocation);
	AnimInfo = MachineType.AnimInfo;

	SkeletalMeshComponent->SetAnimInstanceClass(AnimInfo.AnimBlueprintClass);
	UpdateStats(MachineType.MachineLevelsInfo[0]);

	if (GetWorld())
	{
		const auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
		if (GM)
		{
			GM->AddMachine(this, ResourceOutRowName);
		}
	}

	MachineTile->OnLevelUp.AddUObject(this, &ASK_MachineActorBase::UpdateStats);
}

bool ASK_MachineActorBase::CanWork(FVector& OutWorkLocation)
{
	if (!bIsVacancy || OnPause) return false;

	OutWorkLocation = GetActorLocation() + WorkLocation;
	return true;

}

void ASK_MachineActorBase::UpdateVacancy(bool Vacancy, const TObjectPtr<ASK_AIPawnBase>& Pawn)
{
	bIsVacancy = Vacancy;

	CurrentAIPawn = Pawn;
}

const int32 ASK_MachineActorBase::GetMachineLevel()
{
	if (MachineTile)
	{
		const int32 Level = MachineTile->GetCurrentLevel();

		return Level;
	}
	return -1;
}

void ASK_MachineActorBase::BeginPlay()
{
	Super::BeginPlay();

	if (!GetWorld()) return;

	PlatformGameInstance = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	if (!PlatformGameInstance)
	{
		UE_LOG(ASK_MachineActorBaseLog, Error, TEXT("PlatformGameInstance is nullptr!"));
	}
}

void ASK_MachineActorBase::CreateProducts()
{
	PlatformGameInstance->AddResourceToStorage(ResourceOutRowName, CurrentLevelInfo.QuantityResourceOut);
}

void ASK_MachineActorBase::UpdateLocation(const FVector& NewLocation)
{
	SetActorLocation(NewLocation);

	if (CurrentAIPawn)
	{
		if (IsAtTheWorkplace)
		{
			CurrentAIPawn->SetActorLocation(GetActorLocation() + WorkLocation);
		}
		else
		{
			auto AIController = CurrentAIPawn->GetController<AAIController>();
			if (AIController)
			{
				auto Blackboard = AIController->GetBlackboardComponent();
				if (Blackboard)
				{
					Blackboard->SetValueAsVector(FName("WorkLocation"), GetActorLocation() + WorkLocation);
				}
			}
		}
	}
}

void ASK_MachineActorBase::UpdateStats(const FMachineLevelInfo& Info)
{
	CurrentLevelInfo = Info;
}

void ASK_MachineActorBase::PlayAnim()
{
	PlayMontage(AnimInfo.StartActorAnimMontage);

	GetWorldTimerManager().SetTimer(AnimTimerHandle, this, &ASK_MachineActorBase::AnimTick, 1.f, true);

	NumOfLoop = CurrentLevelInfo.CreateProductionTime - 2.f;
	CurrentCycle = 0;
}

const FVector ASK_MachineActorBase::GetWorkLocation()
{
	return GetActorLocation() + WorkLocation;
}

TObjectPtr<ASK_AIPawnBase> ASK_MachineActorBase::GetCurrentAIPawn()
{
	return CurrentAIPawn;
}

void ASK_MachineActorBase::AnimTick()
{
	PlayMontage(AnimInfo.RepeatActorAnimMontage);

	CurrentCycle++;
	if (CurrentCycle >= NumOfLoop)
	{
		GetWorldTimerManager().ClearTimer(AnimTimerHandle);
		GetWorldTimerManager().SetTimer(FinishAnimTimerHandle, this, &ASK_MachineActorBase::AnimEnd, 1.f, false);
	}
}

void ASK_MachineActorBase::AnimEnd()
{
	PlayMontage(AnimInfo.FinishActorAnimMontage);
}

void ASK_MachineActorBase::PlayMontage(const TObjectPtr<UAnimMontage>& Montage)
{
	const auto AnimInstance = SkeletalMeshComponent->GetAnimInstance();
	if (AnimInstance)
	{
		AnimInstance->Montage_Play(Montage);
	}
}