﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "SK_Slipway.h"
#include "SK_CharacterBase.h"
#include "SK_PlatformGameInstance.h"
#include "Actors/Components/SK_SlipwayWidgetComponent.h"
#include "UI/Widgets/Slipway/SK_UpSlipwayWidget.h"
#include "SK_ShipBuilder.h"
#include "SK_GameMode.h"
#include "SK_PopupHintWidgetComponent.h"

#include "SKW_Ship.h"

#include "Components/WidgetComponent.h"
#include "Engine/StaticMesh.h"
#include "Components/BoxComponent.h"

ASK_Slipway::ASK_Slipway()
{
	PrimaryActorTick.bCanEverTick = false;

	WidgetComponent = CreateDefaultSubobject<USK_SlipwayWidgetComponent>(TEXT("WidgetComponent"));
	WidgetComponent->SetupAttachment(SceneComponent);

	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxCollision->SetupAttachment(SceneComponent);
}

bool ASK_Slipway::CanWork(FVector& Workplace, int32& index,TObjectPtr<ASK_Slipway>& SlipwayActor)
{
	for (int32 i = 0; i < Workplaces.Num(); i++)
	{
		if (!Workplaces[i].Employed) 
		{
			Workplaces[i].Employed = true;
			Workplace = Workplaces[i].Location + GetActorLocation();
			index = i;
			SlipwayActor = this;
			return true;
		}
	}

	return false;
}

bool ASK_Slipway::IsCanWork()
{
	for (int32 i = 0; i < Workplaces.Num(); i++)
	{
		if (!Workplaces[i].Employed)
		{
			return true;
		}
	}

	return false;
}

void ASK_Slipway::ClearWorkplace(const uint32 Index)
{
	Workplaces[Index].Employed = false;
}

void ASK_Slipway::InitUpSlipwayWidget()
{
	WidgetComponent->SetWidgetClass(UpSlipwayWidgetClass);
	WidgetComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WidgetComponent->SetHiddenInGame(true);
}

void ASK_Slipway::ShowWidget()
{
	if (!GetWorld()) return;

	const auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
	if (GM)
	{
		const auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
		if (GI)
		{
			auto UpWidget = Cast<USK_UpSlipwayWidget>(WidgetComponent->GetWidget());
			if (UpWidget)
			{
				const auto Index = GI->GetSlipwayUpPrice(GM->GetSlipwayLevel());
				if (Index == INDEX_NONE) return;

				UpWidget->SetPrice(Index);
				WidgetComponent->SetPrice(Index);
			}
		}
	}
	WidgetComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	WidgetComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	WidgetComponent->SetHiddenInGame(false);
}

void ASK_Slipway::HideWidget()
{
	if (StaticMeshComponent && ActiveMesh)
	{
		StaticMeshComponent->SetStaticMesh(ActiveMesh);
	}

	WidgetComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WidgetComponent->SetHiddenInGame(true);
}

void ASK_Slipway::BeginPlay()
{
	Super::BeginPlay();

	const UWorld* World = GetWorld();
	if (World)
	{
		auto GM = World->GetAuthGameMode<ASK_GameMode>();
		if (!GM) return;

		auto SM = GM->GetShipBuilder();
		if (!SM) return;
		SM->SetShipWidgetClass(ShipWidgetClass);
		SM->AddSlipway(this);
		InitUpSlipwayWidget();

		if (StaticMeshComponent && DefaultsMesh)
		{
			StaticMeshComponent->SetStaticMesh(DefaultsMesh);
		}
	}

	BoxCollision->OnComponentBeginOverlap.AddDynamic(this, &ASK_Slipway::OnOverlapped);
	BoxCollision->OnComponentEndOverlap.AddDynamic(this, &ASK_Slipway::EndOverlap);
}

void ASK_Slipway::OnOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	const auto Player = Cast<ASK_CharacterBase>(OtherActor);
	if (Player)
	{
		Player->ShowSlipwayWorkWidget();
	}
}

void ASK_Slipway::EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	const auto Player = Cast<ASK_CharacterBase>(OtherActor);
	if (Player)
	{
		Player->HideWorkWidget();
	}
}
