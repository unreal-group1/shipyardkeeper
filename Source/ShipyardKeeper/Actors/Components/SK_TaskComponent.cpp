// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/Components/SK_TaskComponent.h"
#include "UI/Widgets/SK_TaskWidget.h"
#include "SK_GameMode.h"
#include "SK_CharacterBase.h"
#include "UI/Widgets/SK_TaskWidget.h"
#include "SK_GameHUD.h"
#include "SKW_Game.h"
#include "SK_PlatformGameInstance.h"
#include "SK_ShipBuilder.h"
#include "Actors/SK_MachineActorBase.h"
#include "Actors/SK_MachineTile.h"
#include "SK_StorageActor.h"

DEFINE_LOG_CATEGORY_STATIC(USK_TaskComponentLog, All, All);

USK_TaskComponent::USK_TaskComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.TickInterval = 2.5f;
}

void USK_TaskComponent::SetCurrentTask(const int32 NewIndex)
{
	CurrentTask = NewIndex;

	if (!Tasks.IsValidIndex(CurrentTask)) return;

	FText ObjectName;
	if (Tasks[CurrentTask].RowName != NAME_None)
	{
		GetObjectNameIfPossible(Tasks[CurrentTask].RowName, ObjectName);
	}

	TaskWidget->SetNewTask(Tasks[CurrentTask].Quantity, Tasks[CurrentTask].TaskText, ObjectName);
}

void USK_TaskComponent::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld())
	{
		const auto Player = GetWorld()->GetFirstPlayerController()->GetPawn<ASK_CharacterBase>();
		if (Player)
		{
			Player->OnGameComplitedByMName.AddUObject(this, &USK_TaskComponent::SetUsedMashineName);
		}
		const auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
		if (GI)
		{
			GI->OnResourceCountChange.AddDynamic(this, &USK_TaskComponent::ResourceChange);
		}
	}
}

bool USK_TaskComponent::GetObjectNameIfPossible(const FName& RowName, FText& ObjectName)
{
	if (!Tasks.IsValidIndex(CurrentTask)) return false;

	if (!GetWorld()) return false;
	const auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();

	switch (Tasks[CurrentTask].ObjectOfVerification.GetValue())
	{
	case EObjectOfVerification::MiniGame:
	{
		FMachineType MachineType;
		if (GI->GetMachineBuyName(RowName, MachineType))
		{
			ObjectName = MachineType.MachineName;
			return true;
		}
		return false;
	}
		break;

	case EObjectOfVerification::Resource:
	{
		auto ResourceType = GI->GetResourceTypeByName(RowName);
		if (ResourceType)
		{
			ObjectName = ResourceType->ShowingData.PrintName;
			return true;
		}
		return false;
	}
	break;
	case EObjectOfVerification::Ship:
	{
		FShipTypes ShipType;
		if (GI->GetShipsBuyName(RowName, ShipType))
		{
			ObjectName = ShipType.ShowingData.PrintName;
			return true;
		}
		return false;
	}
	break;
	case EObjectOfVerification::Machine:
	{
		FMachineType MachineType;
		if (GI->GetMachineBuyName(RowName, MachineType))
		{
			ObjectName = MachineType.ShowingData.PrintName;
			return true;
		}
		return false;
	}
		break;
	case EObjectOfVerification::MachineUp:
	{
		FMachineType MachineType;
		if (GI->GetMachineBuyName(RowName, MachineType))
		{
			ObjectName = MachineType.ShowingData.PrintName;
			return true;
		}
		return false;
	}
	break;
	case EObjectOfVerification::SlipwayUp:
	{
		ObjectName = SlipwayNameText;
		return true;
	}
	break;
	case EObjectOfVerification::StorageUp:
	{

		FStorageType StorageType;
		if (GI->GetStorageBuyName(RowName, StorageType))
		{
			ObjectName = StorageType.StorageName;
			return true;
		}
		return false;
	}
	break;
	default:
		break;
	}

	return false;
}

void USK_TaskComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (CurrentState != EComponentState::Checking) return;
	RunCheck();

	UE_LOG(USK_TaskComponentLog, Error, TEXT("Tick"));
}

void USK_TaskComponent::RunCheck()
{
	if (!Tasks.IsValidIndex(CurrentTask) || TaskWidget->GetVisibility() == ESlateVisibility::Collapsed) return;

	switch (Tasks[CurrentTask].ObjectOfVerification.GetValue())
	{
	case EObjectOfVerification::MiniGame:
		if (CheckUsedMashine())
		{
			CurrentState = EComponentState::Expectation;
			GetWorld()->GetTimerManager().SetTimer(CompleteHandle, this, &USK_TaskComponent::CompleteTask, 2.f, false);
		}
		break;
	case EObjectOfVerification::Resource:
		if(CheckResources()) 
		{
			CurrentState = EComponentState::Expectation;
			GetWorld()->GetTimerManager().SetTimer(CompleteHandle, this, &USK_TaskComponent::CompleteTask, 2.f, false);
		}
		break;
	case EObjectOfVerification::Ship:
		if(CheckShip()) 
		{
			CurrentState = EComponentState::Expectation;
			GetWorld()->GetTimerManager().SetTimer(CompleteHandle, this, &USK_TaskComponent::CompleteTask, 2.f, false);
		}
		break;
	case EObjectOfVerification::Machine:
		if (CheckMashine()) 
		{
			CurrentState = EComponentState::Expectation;
			GetWorld()->GetTimerManager().SetTimer(CompleteHandle, this, &USK_TaskComponent::CompleteTask, 2.f, false);
		}
		break;
	case EObjectOfVerification::MachineUp:
		if (CheckMachineLevel())
		{
			CurrentState = EComponentState::Expectation;
			GetWorld()->GetTimerManager().SetTimer(CompleteHandle, this, &USK_TaskComponent::CompleteTask, 2.f, false);
		}

	case EObjectOfVerification::SlipwayUp:
		if (CheckSlipwayLevel())
		{
			CurrentState = EComponentState::Expectation;
			GetWorld()->GetTimerManager().SetTimer(CompleteHandle, this, &USK_TaskComponent::CompleteTask, 2.f, false);
		}
		break;
	case EObjectOfVerification::StorageUp:
		if (CheckStorageLevel())
		{
			CurrentState = EComponentState::Expectation;
			GetWorld()->GetTimerManager().SetTimer(CompleteHandle, this, &USK_TaskComponent::CompleteTask, 2.f, false);
		}
		break;
	default:
		break;
	}	
}

bool USK_TaskComponent::CheckMiniGame()
{

	return false;
}

void USK_TaskComponent::SetUsedMashineName(const FName& MashineName)
{
	if (!UsedMashineNames.Contains(MashineName))
	{
		UsedMashineNames.Add(MashineName);
	}
}

bool USK_TaskComponent::CheckUsedMashine()
{
	return UsedMashineNames.Contains(Tasks[CurrentTask].RowName);
}

void USK_TaskComponent::CompleteTask()
{
	if (!TaskWidget) return;

	TaskWidget->TaskCompleted(Tasks[CurrentTask].Reward);
}

void USK_TaskComponent::UpdateTaskWidget()
{
	FText ObjectName;
	if (Tasks[CurrentTask].RowName != NAME_None)
	{
		GetObjectNameIfPossible(Tasks[CurrentTask].RowName, ObjectName);
	}

	TaskWidget->ObjectTextUpdate(Tasks[CurrentTask].Quantity,CurrentNum, ObjectName, Tasks[CurrentTask].TaskText);

}

void USK_TaskComponent::Init()
{
	const auto HUD = GetWorld()->GetFirstPlayerController()->GetHUD<ASK_GameHUD>();
	if (HUD)
	{
		auto GameWidget = HUD->GetGameWidget();
		if (GameWidget)
		{
			GameWidget->CreateTaskWidget();
			TaskWidget = GameWidget->GetTaskWidget();
			if (!TaskWidget)
			{
				UE_LOG(USK_TaskComponentLog, Error, TEXT("TaskWidget is nullptr"));
			}
		}
	}
	InitTaskWidget();
	SetCurrentTask(0);
	TaskWidget->ShowTextF(Tasks[CurrentTask].NarrativeText);
	GetWorld()->GetTimerManager().SetTimer(ShowTextHandle, this, &USK_TaskComponent::HideText, TextDelay, false);
}

void USK_TaskComponent::TrySetNextTask()
{
	ShowText();
}

bool USK_TaskComponent::CheckResources()
{
	if (!GetWorld()) return false;

	const auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	if (GI)
	{
		if (CurrentNum >= Tasks[CurrentTask].Quantity)
		{
			return true;
		}
		else
		{
			UpdateTaskWidget();
		}
	}
	return false;
}

bool USK_TaskComponent::CheckShip()
{
	if (GetWorld())
	{
		const auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
		if (GM)
		{
			const auto Builder = GM->GetShipBuilder();
			for (const auto& Ship : Builder->GetShips())
			{
				if (Ship.ShipRowName == Tasks[CurrentTask].RowName)
				{
					if (Ship.Num >= Tasks[CurrentTask].Quantity)
					{
						return true;
					}

					CurrentNum = Ship.Num;
					UpdateTaskWidget();
					return false;
				}
			}
		}
	}
	return false;
}

void USK_TaskComponent::ResourceChange(FName ResourceName, double NewQuantity)
{
	if (ResourceName == Tasks[CurrentTask].RowName)
	{
		if (LastValue < NewQuantity)
		{
			LastValue = NewQuantity;
			CurrentNum++;
		}
		else
		{
			LastValue = NewQuantity;
		}
	}
}

bool USK_TaskComponent::CheckMashine()
{
	if (GetWorld())
	{
		const auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
		if (GM)
		{
			const auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
			FMachineType MachineType;
			if (!GI->GetMachineBuyName(Tasks[CurrentTask].RowName, MachineType)) return false;

			const auto ROutName = MachineType.ResourceOutRowName;

			for (const auto& Mashine : GM->GetMachineActors())
			{
				for (const auto& Actors : Mashine.MachineActors)
				{
					if (Actors->GetResourseOutName() == ROutName)
					{

						if (Mashine.MachineActors.Num() >= Tasks[CurrentTask].Quantity)
						{
							return true;
						}
						else
						{
							CurrentNum = Mashine.MachineActors.Num();
							UpdateTaskWidget();
						}
					}
				}
			}
		}

	}
	return false;
}

void USK_TaskComponent::HideWidget()
{
	TaskWidget->SetVisibility(ESlateVisibility::Collapsed);
}

void USK_TaskComponent::InitTaskWidget()
{
	TaskWidget->SetComponent(this);
}

bool USK_TaskComponent::CheckMachineLevel()
{
	if (!GetWorld()) return false;

	const auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
	if (!GM) return false;

	const auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();

	for (const auto& MachineActor : GM->GetMachineTiles())
	{
		const auto MTile = Cast<ASK_MachineTile>(MachineActor);//ref on ASK_MachineTile Array
		if (MTile)
		{
			if (MTile->GetCurrentName() == Tasks[CurrentTask].RowName)
			{
				if (MTile->GetCurrentLevel() >= Tasks[CurrentTask].Quantity)
				{
					return true;
				}
				else
				{
					CurrentNum = MTile->GetCurrentLevel();
					UpdateTaskWidget();
				}
			}
		}
	}

	return false;
}

bool USK_TaskComponent::CheckSlipwayLevel()
{
	if (!GetWorld()) return false;

	const auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
	if (GM)
	{
		if (GM->GetSlipwayLevel().GetIntValue() + 1 >= Tasks[CurrentTask].Quantity)
		{
			return true;
		}
		else
		{
			CurrentNum = GM->GetSlipwayLevel().GetIntValue() + 1;
			UpdateTaskWidget();
		}
	}

	return false;
}

bool USK_TaskComponent::CheckStorageLevel()
{
	const auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
	if (GM)
	{
		for (const auto& Storage : GM->GetStorageActors())
		{
			if (FName(*Storage->GetStorageName().ToString()) == Tasks[CurrentTask].RowName)
			{
				if (Storage->GetCurrentLevel() >= Tasks[CurrentTask].Quantity)
				{
					return true;
				}
				else
				{
					CurrentNum = Storage->GetCurrentLevel();
					UpdateTaskWidget();
				}
			}
		}
	}

	return false;
}

void USK_TaskComponent::ShowText()
{
	if (!GetWorld()) return;
	
	if (!Tasks.IsValidIndex(CurrentTask + 1))
	{
		if (!GetWorld()) return;

		GetWorld()->GetTimerManager().SetTimer(HideWidgetHandle, this, &USK_TaskComponent::HideWidget, 1.0f, false);
		return;
	}
	else
	{
		CurrentTask++;
		GetWorld()->GetTimerManager().SetTimer(ShowTextHandle, this, &USK_TaskComponent::HideShowText, TextDelay, false);
		TaskWidget->ShowTextF(Tasks[CurrentTask].NarrativeText);

		GetWorld()->GetTimerManager().SetTimer(SetNextTaskHandle, this, &USK_TaskComponent::SetNextTask, 0.9f, false);
	}
	
}

void USK_TaskComponent::HideShowText()
{
	TaskWidget->ShowTextR();
}

void USK_TaskComponent::SetNextTask()
{
	CurrentState = EComponentState::Checking;
	LastValue = -1;
	CurrentNum = 0;
	SetCurrentTask(CurrentTask);
}

void USK_TaskComponent::HideText()
{
	TaskWidget->ShowTextR();
}
