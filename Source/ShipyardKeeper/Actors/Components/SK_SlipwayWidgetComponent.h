// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "SK_SlipwayWidgetComponent.generated.h"

UCLASS()
class SHIPYARDKEEPER_API USK_SlipwayWidgetComponent : public UWidgetComponent
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
	void OnCliked();

	void SetPrice(const int32 Price);

private:
	int32 CurrentPrice;
};
