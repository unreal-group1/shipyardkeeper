// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SK_Types.h"
#include "SK_TaskComponent.generated.h"

class USK_TaskWidget;

UENUM()
enum EComponentState : uint8
{
	Checking,
	Expectation
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHIPYARDKEEPER_API USK_TaskComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	USK_TaskComponent();

	void SetCurrentTask(const int32 NewIndex);

	void RunCheck();

	bool CheckMiniGame();

	void SetUsedMashineName(const FName& MashineName);
	bool CheckUsedMashine();

	void CompleteTask();
	void UpdateTaskWidget();
	void Init();

	void TrySetNextTask();
	bool CheckResources();

	bool CheckShip();
	UFUNCTION()
	void ResourceChange(FName ResourceName, double NewQuantity);

	bool CheckMashine();
	void HideWidget();

	void InitTaskWidget();

	bool CheckMachineLevel();

	bool CheckSlipwayLevel();
	bool CheckStorageLevel();

	void ShowText();
	void HideShowText();

	void SetNextTask();
	void HideText();
protected:
	virtual void BeginPlay() override;

	bool GetObjectNameIfPossible(const FName& RowName, FText& ObjectName);

	UPROPERTY(EditDefaultsOnly,Category = "UI")
	TSubclassOf<USK_TaskWidget> TaskWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = "Tasks")
	TArray<FTasks> Tasks;

	int32 CurrentTask{ 0 };

	int32 CurrentNum{ 0 };

	double LastValue{ -1.0 };
	UPROPERTY()
	TObjectPtr<USK_TaskWidget> TaskWidget;

	FTimerHandle UpdateTaskHandle;
	FTimerHandle HideWidgetHandle;
	FTimerHandle CompleteHandle;
	FTimerHandle ShowTextHandle;
	FTimerHandle SetNextTaskHandle;


	EComponentState CurrentState = EComponentState::Checking;

	UPROPERTY(EditDefaultsOnly,Category = "Task")
	FText SlipwayNameText;

	UPROPERTY(EditDefaultsOnly, Category = "Task")
	float TextDelay{ 5.f };

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	TArray<FName> UsedMashineNames;
};
