// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/Components/SK_SlipwayWidgetComponent.h"
#include "SK_GameMode.h"

void USK_SlipwayWidgetComponent::OnCliked()
{
	if (true)//Anim
	{
		auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
		if (GM)
		{
			GM->TryUpSlipway(CurrentPrice);
		}
	}
}

void USK_SlipwayWidgetComponent::SetPrice(const int32 Price)
{
	CurrentPrice = Price;
}
