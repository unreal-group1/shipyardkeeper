// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/SK_MachineTile.h"
#include "Actors/SK_MachineActorBase.h"
#include "Framework/SK_PlatformGameInstance.h"
#include "SK_GameMode.h"
#include "AI/Pawn/SK_AIPawnBase.h"
#include "SK_CharacterBase.h"
#include "SK_PopupHintWidgetComponent.h"

#include "SKW_ButtonUpgradeMachine.h"
#include "SKW_DestroyMachineButton.h"

#include "Materials/MaterialInstance.h"
#include "Engine/StaticMesh.h"
#include "Components/BoxComponent.h"

DEFINE_LOG_CATEGORY_STATIC(ASK_MachineTileLog, All, All);

ASK_MachineTile::ASK_MachineTile()
{
	PrimaryActorTick.bCanEverTick = false;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxComponent->SetupAttachment(SceneComponent);

	PopupUpgradeComponent = CreateDefaultSubobject<USK_PopupHintWidgetComponent>(TEXT("PopupUpgradeComponent"));
	if (PopupUpgradeComponent)
	{
		PopupUpgradeComponent->SetupAttachment(SceneComponent);
	}

	PopupDestroyMachinesComponent = CreateDefaultSubobject<USK_PopupHintWidgetComponent>(TEXT("PopupDestroyMachinesComponent"));
	if (PopupDestroyMachinesComponent)
	{
		PopupDestroyMachinesComponent->SetupAttachment(SceneComponent);
	}

	/*AcceptWindowComponent = CreateDefaultSubobject<USK_PopupHintWidgetComponent>(TEXT("AcceptWindowComponent"));
	if (AcceptWindowComponent)
	{
		AcceptWindowComponent->SetupAttachment(SceneComponent);
	}*/
}

void ASK_MachineTile::BeginPlay()
{
	check(MachineActorClass);

	Super::BeginPlay();

	//SpawnMachine(FName("TEST"));

	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ASK_MachineTile::OnComponentOverlap);
	BoxComponent->OnComponentEndOverlap.AddDynamic(this, &ASK_MachineTile::OnEndOverlap);

	if (StaticMeshComponent)
	{
		StaticMeshComponent->SetMaterial(0, Background);
	}

	if (PopupHintWidgetComponent)
	{
		PopupHintDefaultLocation = PopupHintWidgetComponent->GetComponentTransform().GetLocation();
	}

	if (PopupUpgradeComponent
		&& PopupUpgradeComponent->GetWidgetClass())
	{
		PopupUpgradeDefaultLocation = PopupUpgradeComponent->GetComponentTransform().GetLocation();
		auto Widget = Cast<USKW_ButtonUpgradeMachine>(PopupUpgradeComponent->GetWidget());
		if (Widget)
		{
			Widget->SetMachineTile(this);
			PopupUpgradeComponent->Hide();
		}

		PopupUpgradeComponent->SetWorldLocation(FVector::ZeroVector);
	}

	if (PopupDestroyMachinesComponent)
	{
		auto Widget = Cast<USKW_DestroyMachineButton>(PopupDestroyMachinesComponent->GetWidget());
		PopupDestroyMachinesDefaultLocation = PopupDestroyMachinesComponent->GetComponentTransform().GetLocation();
		if (Widget)
		{
			Widget->SetMachineTile(this);
			PopupDestroyMachinesComponent->Hide();
		}

		PopupDestroyMachinesComponent->SetWorldLocation(FVector::ZeroVector);
	}

	/*if (AcceptWindowComponent
		&& AcceptWindowComponent->GetWidgetClass())
	{
		auto Widget = Cast<USKW_ButtonUpgradeMachine>(AcceptWindowComponent->GetWidget());
		PopupAcceptWindowComponentLocation = AcceptWindowComponent->GetComponentTransform().GetLocation();
		if (Widget)
		{
			Widget->SetMachineTile(this);
			Widget->SetPosition(PopupAcceptWindowComponentLocation);
			AcceptWindowComponent->Hide();
		}
		AcceptWindowComponent->SetWorldLocation(PopupAcceptWindowComponentLocation);
	}*/
}

void ASK_MachineTile::CreateMachineActor(const FMachineType& MachineType)
{
	if (!GetWorld()) return;

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	const FVector Location = GetActorLocation() + MachineType.SpawnLocationOffset;
	
	const auto MachineActor = GetWorld()->SpawnActor<ASK_MachineActorBase>(MachineActorClass, Location, MachineType.SpawnRotationOffset, SpawnParameters);
	if (MachineActor)
	{
		MachineActor->Init(MachineType,this, MachineType.SpawnRotationOffset);
		MachineActors.Add(MachineActor);
	}
}

void ASK_MachineTile::SpawnMachine(const FName& MachineRowName)
{
	CurrentName = MachineRowName;
	const auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	FMachineType Machine;
	if (GI)
	{
		if (GI->GetMachineBuyName(MachineRowName, Machine))
		{
			CreateMachineActor(Machine);
			StaticMeshComponent->SetMaterial(0, WorkingTile);
		}
	}

	if (!Machine.MachineLevelsInfo.IsEmpty())
	{
		CurrentLevel = 0;
		CurrentLevelInfo = Machine.MachineLevelsInfo;
		CurrentNumberOfMachines = Machine.MachineLevelsInfo[CurrentLevel].NumberOfMachines;
	}
	else
	{
		UE_LOG(ASK_MachineTileLog, Error, TEXT("MachineLevelsInfo is empty"));
	}
}

void ASK_MachineTile::LevelUp()
{
	const auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	if (GI)
	{
		const auto NextIndex = CurrentLevel + 1;
		if (!CurrentLevelInfo.IsValidIndex(NextIndex)) return;

		if (GI->TryToBuy(CurrentLevelInfo[NextIndex].CostOfTheIncrease))
		{
			CurrentLevel++;
			OnLevelUp.Broadcast(CurrentLevelInfo[NextIndex]);

			if (CurrentNumberOfMachines != CurrentLevelInfo[NextIndex].NumberOfMachines)
			{
				CurrentNumberOfMachines = CurrentLevelInfo[NextIndex].NumberOfMachines;
				UpdateNumOfMashine(CurrentLevelInfo[NextIndex].NumberOfMachines);

				for (const auto MashineActor : MachineActors)
				{
					MashineActor->SetLevelInfo(CurrentLevelInfo[NextIndex]);
				}
			}

			UpdateUpgradeCostInWidget();
		}
	}
}

const FMachineLevelInfo ASK_MachineTile::GetLevelInfo(const int32 Index)
{
	if (CurrentLevelInfo.IsValidIndex(Index))
	{
		return CurrentLevelInfo[Index];
	}
	else
	{
		return FMachineLevelInfo();
	}
}

void ASK_MachineTile::Show()
{
	if (PopupUpgradeComponent)
	{
		if (HaveMachine())
		{
			PopupUpgradeComponent->SetWorldLocation(PopupUpgradeDefaultLocation);
			UpdateUpgradeCostInWidget();
			PopupUpgradeComponent->Show();
		}
		else
		{
			PopupUpgradeComponent->Hide();
			PopupUpgradeComponent->SetWorldLocation(FVector::ZeroVector);
		}
	}

	if (PopupDestroyMachinesComponent)
	{
		if (HaveMachine())
		{
			PopupDestroyMachinesComponent->SetWorldLocation(PopupDestroyMachinesDefaultLocation);
			UpdateUpgradeCostInWidget();
			PopupDestroyMachinesComponent->Show();
		}
		else
		{
			PopupDestroyMachinesComponent->Hide();
			PopupDestroyMachinesComponent->SetWorldLocation(FVector::ZeroVector);
		}
	}

	if (PopupHintWidgetComponent)
	{
		if (HaveMachine())
		{
			PopupHintWidgetComponent->Hide();
			PopupHintWidgetComponent->SetWorldLocation(FVector::ZeroVector);
		}
		else
		{
			PopupHintWidgetComponent->SetWorldLocation(PopupUpgradeDefaultLocation);
			PopupHintWidgetComponent->Show();
		}
	}

	//if (AcceptWindowComponent)
	//{
	//	if (HaveMachine())
	//	{
	//		AcceptWindowComponent->Hide();
	//		AcceptWindowComponent->SetWorldLocation(FVector::ZeroVector);
	//	}
	//	else
	//	{
	//		AcceptWindowComponent->SetWorldLocation(PopupAcceptWindowComponentLocation);
	//		AcceptWindowComponent->Show();
	//	}
	//}
}

void ASK_MachineTile::Hide()
{
	if (PopupUpgradeComponent)
	{
		PopupUpgradeComponent->Hide();
	}

	if (PopupHintWidgetComponent)
	{
		PopupHintWidgetComponent->Hide();
	}

	if (PopupDestroyMachinesComponent)
	{
		PopupDestroyMachinesComponent->Hide();
	}
}

void ASK_MachineTile::UpdateUpgradeCostInWidget()
{
	if (PopupUpgradeComponent)
	{
		auto Widget = Cast<USKW_ButtonUpgradeMachine>(PopupUpgradeComponent->GetWidget());

		if (Widget)
		{
			FText NewText;
			if (CurrentLevel >= CurrentLevelInfo.Num() - 1)// || !CurrentLevelInfo.IsValidIndex(CurrentLevel + 1))
			{
				NewText = FText::FromString(TEXT("MAX"));
			}
			else
			{
				NewText = FText::FromString(FString::Printf(TEXT("%i"),
					CurrentLevelInfo[CurrentLevel + 1].CostOfTheIncrease));
			}
			Widget->SetText(NewText);
		}
	}
}

void ASK_MachineTile::ClearMachines()
{
	CurrentLevel = 0;

	StaticMeshComponent->SetMaterial(0, Background);

	USK_PlatformGameInstance* GI = nullptr;
	if (GetWorld())
	{
		GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	}

	if (GetWorld())
	{
		const auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
		if (GM)
		{
			auto MachineActorsFromGM = GM->GetMachineActors();

			for (auto MachineActor : MachineActors)
			{
				auto MachineType = MachineActorsFromGM.FindByPredicate(
				[&](const FMachineTypes CurrentMachineType)
				{
					return CurrentMachineType.OutResourceRowName == MachineActor->GetResourseOutName();
				});

				auto FindedMachineActor = MachineType->MachineActors.FindByPredicate(
				[&](const ASK_MachineActorBase* MachineActorBase)
				{
					return MachineActorBase == MachineActor;
				});

				auto FindedMachineActorIndex = MachineType->MachineActors.Find(*FindedMachineActor);

				MachineType->MachineActors.RemoveAt(FindedMachineActorIndex);

				auto CurrentPawn = MachineActor->GetCurrentAIPawn();
				if (CurrentPawn)
				{
					auto NPCLocation = MachineActor->GetCurrentAIPawn()->GetActorLocation();
					CurrentPawn->Destroy();

					if (GI)
					{
						auto NPSSpawner = GI->GetNPSSpawner();
						NPSSpawner->SpawnNPCAtLocation(NPCLocation);
					}
				}

				MachineActor->Destroy();
			}
		}
	}

	MachineActors.Empty();
}

bool ASK_MachineTile::GetNextLevelInfo(FMachineLevelInfo& LevelInfo)
{
	const auto NextIndex = CurrentLevel + 1;
	if (CurrentLevelInfo.IsValidIndex(NextIndex))
	{
		LevelInfo = CurrentLevelInfo[NextIndex];
		return true;
	}
	return false;
}

void ASK_MachineTile::UpdateNumOfMashine(const TEnumAsByte<EMachineWorkplaces>& Num)
{
	const auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();	
	FMachineType Machine;
	if (GI)
	{	
		if (GI->GetMachineBuyName(CurrentName, Machine)) CreateMachineActor(Machine);
	}

	const EMachineWorkplaces Em = Num.GetValue();
	switch (Em)
	{
	case EMachineWorkplaces::Two:
		{
			if (!MachineActors.IsValidIndex(1))
			{
				UE_LOG(ASK_MachineTileLog, Error, TEXT("Is not valid mashine index"));
				return;
			}

			MachineActors[0]->UpdateLocation(GetActorLocation() + Machine.SpawnLocationOffset + SecondOne);
			MachineActors[1]->UpdateLocation(GetActorLocation() + Machine.SpawnLocationOffset + SecondTwo);
		}
		break;
	case EMachineWorkplaces::Three:
	{
		if (!MachineActors.IsValidIndex(2))
		{
			UE_LOG(ASK_MachineTileLog, Error, TEXT("Is not valid mashine index"));
			return;
		}
		MachineActors[0]->UpdateLocation(GetActorLocation() + Machine.SpawnLocationOffset + ThirdOne);
		MachineActors[1]->UpdateLocation(GetActorLocation() + Machine.SpawnLocationOffset + ThirdTwo);
		MachineActors[2]->UpdateLocation(GetActorLocation() + Machine.SpawnLocationOffset + ThirdThree);
	}
		break;
	default:
		break;
	}
}

void ASK_MachineTile::OnComponentOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (MachineActors.IsEmpty()) return;

	const auto Player = Cast<ASK_CharacterBase>(OtherActor);
	if (Player)
	{
		Player->ShowWorkWidget();
		Player->SetResource(MachineActors.Last()->GetResourseOutName(), MachineActors.Last()->GetResourseInName());
		Player->SetMashine(this);
		Player->SetMashineName(CurrentName);
	}
}

void ASK_MachineTile::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	const auto Player = Cast<ASK_CharacterBase>(OtherActor);
	if (Player)
	{
		Player->HideWorkWidget();
	}
}

void ASK_MachineTile::TestChangeMaterial()
{
if (StaticMeshComponent)
{
	StaticMeshComponent->SetMaterial(0, WorkingTile);
}
}

bool ASK_MachineTile::HaveMachine()
{
	if (MachineActors.Num() > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

FName ASK_MachineTile::GetMachineTypeName()
{
	return CurrentName;
}
