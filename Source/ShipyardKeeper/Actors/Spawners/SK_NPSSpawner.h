// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SK_NPSSpawner.generated.h"

class ASK_AIPawnBase;

UCLASS()
class SHIPYARDKEEPER_API ASK_NPSSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	ASK_NPSSpawner();

	void SpawnNPS();
	void SpawnNPCAtLocation(FVector NewLocation);
protected:
	
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly,Category = "Spawn")
	TSubclassOf<ASK_AIPawnBase> AIPawnBaseClass;

	FTimerHandle SpawnTestTimer;
};
