// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SK_TileSpawner.generated.h"

class UStaticMeshComponent;
class ASK_TileActorBase;
class UStaticMesh;
class ASK_Slipway;
class ASK_OfficeTable;

class USK_PopupHintWidgetComponent;
class USKW_ButtonBuyTile;
class UWidgetComponent;
class USK_GridExpansionWidget;
class ASK_SpawnObjectActor;

UCLASS()
class SHIPYARDKEEPER_API ASK_TileSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	ASK_TileSpawner();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
	USK_PopupHintWidgetComponent* ExpansionRowWidgetComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
	USK_PopupHintWidgetComponent* ExpansionColumnWidgetComponent;

	UFUNCTION()
	TArray<ASK_TileActorBase*> GetMachineTiles();
	UFUNCTION()
	TArray<ASK_TileActorBase*> GetStorageTiles();

protected:
	UPROPERTY(VisibleAnywhere,Category = "Components")
	UStaticMeshComponent* ShowLocationMesh;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere,Category = "Spawn")
	TSubclassOf<ASK_TileActorBase> TileMachineClass;

	UPROPERTY(EditAnywhere, Category = "Spawn")
	TSubclassOf<ASK_TileActorBase> TileStorageClass;

	UPROPERTY(EditAnywhere, Category = "Spawn")
	TSubclassOf<ASK_OfficeTable> OfficeTableClass;

	UPROPERTY(EditAnywhere, Category = "Spawn")
	TSubclassOf<ASK_Slipway> SlipwayClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<class USKW_ButtonBuyTile> ButtonBuyMachineTileClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<class USKW_ButtonBuyTile> ButtonBuyTileStorageClass;

	UPROPERTY(EditAnywhere, Category = "Spawn")
	TSubclassOf<ASK_SpawnObjectActor> ObjectActorClass;

	UPROPERTY(EditAnywhere, Category = "Spawn")
	UStaticMesh* TileMesh;

	UPROPERTY(EditAnywhere, Category = "Spawn")
	TArray<UStaticMesh*> UpperWalls;

	UPROPERTY(EditAnywhere, Category = "Spawn")
	UStaticMesh* SideWall;

	UPROPERTY(EditAnywhere, Category = "Spawn")
	UStaticMesh* OfficeFloor;

	UPROPERTY(EditAnywhere, Category = "Spawn")
	UStaticMesh* SlipwayMesh;

	UPROPERTY(EditAnywhere, Category = "Spawn")
	uint32 SlipwayQuantity{ 1 };

	//initial number of blocked tiles
	UPROPERTY(EditAnywhere, Category = "Spawn")
	uint32 BlockedTiles{ 1 };
protected:

	virtual void BeginPlay() override;

	void InitGrid();
	void SpawnTiles(const FUint32Point Dim);
	void GetPadding();
	void AddRow();
	void AddCol();

	void SpawnStorageTile(const int32 Index);
	void SpawnMachineTile(const int32 IndexW, const int32 IndexH);
	void SpawnSlipwayTile(const int32 IndexW, const int32 IndexH);

	void UpdateRowWidget();
	void UpdateColumnWidget();
	void InitWidgets();

	void SpawnOffice(const FUint32Point Dim);
	void UpdateUpperWall(const uint32 Index);
	void UpdateOfficeFloor(const uint32 Index);
	void InitOfficeFloor(const uint32 Index);
	void InitWalls(const FUint32Point Dim);

	void SpawnOfficeTable(const uint32 Index);
	void SetActiveUpWidget();

	void AddMachineTileToGM();
private:

	double XPadding{ 0.0 };
	UPROPERTY(EditAnywhere,Category = "Spawn")
	double XWallPadding{ 0.0 };
	double YPadding{ 0.0 };
	double YWallPadding{ 0.0 };
	double CornerWallPadding{ 0.0 };
	double SlipwayYPadding{ 0.0 };
	double SlipwayXPadding{ 0.0 };

	FUint32Point CurrentDim;

	UPROPERTY()
	TArray<TObjectPtr<ASK_TileActorBase>> MachineTiles;

	UPROPERTY()
	TArray<TObjectPtr<ASK_TileActorBase>> StorageTiles;

	UPROPERTY()
	TObjectPtr<ASK_SpawnObjectActor> UpperWall;

	UPROPERTY()
	TArray<TObjectPtr<ASK_SpawnObjectActor>> MovableWallActors;

	UPROPERTY()
	TObjectPtr<ASK_OfficeTable> OfficeTable;

	UPROPERTY()
	TArray<TObjectPtr<ASK_Slipway>> SlipwayActors;
};
