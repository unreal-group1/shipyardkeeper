// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/Spawners/SK_SpawnObjectActor.h"
#include "Components/StaticMeshComponent.h"

ASK_SpawnObjectActor::ASK_SpawnObjectActor()
{
	PrimaryActorTick.bCanEverTick = false;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMeshComponent->SetupAttachment(SceneComponent);
}
