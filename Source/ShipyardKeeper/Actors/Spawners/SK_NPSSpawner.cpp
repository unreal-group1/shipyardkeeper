// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/Spawners/SK_NPSSpawner.h"
#include "AI/Pawn/SK_AIPawnBase.h"

ASK_NPSSpawner::ASK_NPSSpawner()
{
	PrimaryActorTick.bCanEverTick = false;

}

void ASK_NPSSpawner::SpawnNPS()
{
	if (GetWorld())
	{
		FActorSpawnParameters SpawnParam;
		SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		if (AIPawnBaseClass)
		{
			GetWorld()->SpawnActor<ASK_AIPawnBase>(AIPawnBaseClass, 
				GetActorLocation(), 
				FRotator::ZeroRotator, 
				SpawnParam);
		}
	}
}

void ASK_NPSSpawner::SpawnNPCAtLocation(FVector NewLocation)
{
	if (GetWorld())
	{
		FActorSpawnParameters SpawnParam;
		SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		if (AIPawnBaseClass)
		{
			GetWorld()->SpawnActor<ASK_AIPawnBase>(AIPawnBaseClass,
				NewLocation,
				FRotator::ZeroRotator,
				SpawnParam);
		}
	}
}

void ASK_NPSSpawner::BeginPlay()
{
	Super::BeginPlay();

	check(AIPawnBaseClass);

	//etWorldTimerManager().SetTimer(SpawnTestTimer, this, &ASK_NPSSpawner::SpawnNPS, 2.f, false);
	
}
