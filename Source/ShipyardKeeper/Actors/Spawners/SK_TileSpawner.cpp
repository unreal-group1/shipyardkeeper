// Fill out your copyright notice in the Description page of Project Settings.

#include "SK_TileSpawner.h"
#include "SK_TileActorBase.h"
#include "SK_GameMode.h"
#include "SK_Slipway.h"
#include "Actors/Spawners/SK_SpawnObjectActor.h"
#include "Actors/SK_OfficeTable.h"

#include "SK_GameHUD.h"
#include "SK_PopupHintWidgetComponent.h"
#include "SKW_ButtonBuyTile.h"

#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMesh.h"

DEFINE_LOG_CATEGORY_STATIC(ASK_TileSpawnerLog, All, All);

ASK_TileSpawner::ASK_TileSpawner()
{
	PrimaryActorTick.bCanEverTick = false;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);

	ShowLocationMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StartingCoordinateMesh"));
	ShowLocationMesh->SetupAttachment(SceneComponent);

	ExpansionRowWidgetComponent = CreateDefaultSubobject<USK_PopupHintWidgetComponent>(TEXT("ExpansionRowWidget"));
	if (ExpansionRowWidgetComponent)
	{
		ExpansionRowWidgetComponent->SetupAttachment(SceneComponent);
		ExpansionRowWidgetComponent->SetHiddenInGame(true);
	}

	ExpansionColumnWidgetComponent = CreateDefaultSubobject<USK_PopupHintWidgetComponent>(TEXT("ExpansionColumnWidget"));
	if (ExpansionColumnWidgetComponent)
	{
		ExpansionColumnWidgetComponent->SetupAttachment(SceneComponent);
		ExpansionColumnWidgetComponent->SetHiddenInGame(true);
	}
}

TArray<ASK_TileActorBase*> ASK_TileSpawner::GetMachineTiles()
{
	return MachineTiles;
}

TArray<ASK_TileActorBase*> ASK_TileSpawner::GetStorageTiles()
{
	return StorageTiles;
}

void ASK_TileSpawner::BeginPlay()
{
	check(TileMachineClass);
	check(TileStorageClass);
	check(SlipwayClass);
	check(ObjectActorClass);
	check(TileMesh);
	check(SideWall);
	check(OfficeTableClass);
	check(SlipwayMesh);

	GetPadding();

	Super::BeginPlay();

	ShowLocationMesh->DestroyComponent();
	InitGrid();

	if (ExpansionRowWidgetComponent)
	{
		ExpansionRowWidgetComponent->SetWidgetClass(ButtonBuyMachineTileClass);
		ExpansionRowWidgetComponent->Show();
	}
	if (ExpansionColumnWidgetComponent)
	{
		ExpansionColumnWidgetComponent->SetWidgetClass(ButtonBuyTileStorageClass);
		ExpansionColumnWidgetComponent->Show();
	}
	InitWidgets();
}

void ASK_TileSpawner::SpawnOffice(const FUint32Point Dim)
{
	if (UpperWalls.IsEmpty()) return;

	UpdateUpperWall(Dim.X);
	InitOfficeFloor(Dim.X);
	SpawnOfficeTable(Dim.X);

}

void ASK_TileSpawner::UpdateUpperWall(const uint32 Index)
{
	if (!UpperWalls.IsValidIndex(Index - 2)) return;

	if (UpperWall)
	{
		UpperWall->Destroy();
		UpperWall = nullptr;
	}

	const auto GW = GetWorld();
	if (GW)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		const FVector Location = GetActorLocation() - FVector(0.0, (YPadding * (Index + 1)) * 0.5 + YWallPadding, 0.0) + FVector(XPadding * 1.05,0.0,0.0);
		auto Wall = GW->SpawnActor<ASK_SpawnObjectActor>(ObjectActorClass, Location, FRotator::ZeroRotator, SpawnParameters);
		if (Wall)
		{
			Wall->StaticMeshComponent->SetStaticMesh(UpperWalls[Index - 2]);
			UpperWall = Wall;
		}
	}

}

void ASK_TileSpawner::UpdateOfficeFloor(const uint32 Index)
{
	const auto GW = GetWorld();
	if (GW)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		const FVector Location = GetActorLocation() - FVector(0.0, (YPadding * (Index + 1)) * 0.5 + YWallPadding, 0.0) + FVector(XPadding * 1.05, 0.0, 0.0);
		auto Wall = GW->SpawnActor<ASK_SpawnObjectActor>(ObjectActorClass, Location, FRotator::ZeroRotator, SpawnParameters);
		if (Wall)
		{
			Wall->StaticMeshComponent->SetStaticMesh(UpperWalls[Index - 2]);
			UpperWall = Wall;
		}
	}
}

void ASK_TileSpawner::InitOfficeFloor(const uint32 Index)
{
	for (uint32 i = 1; i <= Index; i++)
	{
		const auto GW = GetWorld();
		if (GW)	
		{
			FActorSpawnParameters SpawnParameters;
			SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			const FVector Location = GetActorLocation() - FVector(0.0, (YPadding * i) + YWallPadding, 0) + FVector(XPadding * 0.75,0.0,0.0);
			auto Floor = GW->SpawnActor<ASK_SpawnObjectActor>(ObjectActorClass, Location, FRotator::ZeroRotator, SpawnParameters);
			if (Floor)
			{
				Floor->StaticMeshComponent->SetStaticMesh(OfficeFloor);
			}
		}
	}
}

void ASK_TileSpawner::InitWalls(const FUint32Point Dim)
{
	if (Dim.X == 0 || Dim.Y == 0) return;

	for (uint32 i = 0; i < Dim.Y; i++)
	{
		for (uint32 j = 1; j <= Dim.X; j++)
		{
			if (j == 1)
			{
				const auto GW = GetWorld();
				if (GW)
				{
					FActorSpawnParameters SpawnParameters;
					SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

					const FVector Location = GetActorLocation() - FVector(XPadding * i, YWallPadding * 0.5 + YPadding * 0.5, 0.0);
					auto Floor = GW->SpawnActor<ASK_SpawnObjectActor>(ObjectActorClass, Location, FRotator::ZeroRotator, SpawnParameters);
					if (Floor)
					{
						Floor->StaticMeshComponent->SetStaticMesh(SideWall);
					}
				}
			}
			if (j == Dim.X)
			{
				const auto GW = GetWorld();
				if (GW)
				{
					FActorSpawnParameters SpawnParameters;
					SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

					const FVector Location = GetActorLocation() - FVector(XPadding * i, YWallPadding * 1.5 + YPadding * 0.5 + YPadding * j, 0.0);
					auto Floor = GW->SpawnActor<ASK_SpawnObjectActor>(ObjectActorClass, Location, FRotator::ZeroRotator, SpawnParameters);
					if (Floor)
					{
						Floor->StaticMeshComponent->SetStaticMesh(SideWall);
					}
				}
			}
		}
	}		
}

void ASK_TileSpawner::SpawnOfficeTable(const uint32 Index)
{
	const auto GW = GetWorld();
	if (GW)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		const FVector Location = GetActorLocation() - FVector(0.0, (YPadding * (Index + 1)) * 0.5 + YWallPadding, 0.0) + FVector(XPadding *0.85, 0.0, 0.0);
		OfficeTable = GW->SpawnActor<ASK_OfficeTable>(OfficeTableClass, Location, FRotator::ZeroRotator, SpawnParameters);
	}
}

void ASK_TileSpawner::SetActiveUpWidget()
{
	SlipwayActors[0]->HideWidget();
	SlipwayActors[1]->ShowWidget();
}

void ASK_TileSpawner::AddMachineTileToGM()
{
	const auto GM = Cast<ASK_GameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (GM)
	{
		GM->SetMachineTiles(MachineTiles);
	}
}

void ASK_TileSpawner::InitGrid()
{
	const auto GM = Cast<ASK_GameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (GM)
	{
		GM->InitManager();
		const auto Dim = GM->GetGridDim();
		CurrentDim = Dim;

		SpawnTiles(Dim);
		SpawnOffice(Dim);
		InitWalls(Dim);
	}
	else
	{
		UE_LOG(ASK_TileSpawnerLog, Error, TEXT("SK_GameMode is nullptr!"));
	}
}

void ASK_TileSpawner::GetPadding()
{
	FBox Box = TileMesh->GetBoundingBox();
	XPadding = Box.GetSize().X;

	Box = TileMesh->GetBoundingBox();
	YPadding = Box.GetSize().Y;

	Box = SideWall->GetBoundingBox();
	YWallPadding = Box.GetSize().Y;

	//Box = OfficeFloorWall->GetBoundingBox();//this
	//CornerWallPadding = YWallPadding + SideWall->GetBoundingBox().GetSize().X * 0.5f + Box.GetSize().X * 0.5f;

	Box = SlipwayMesh->GetBoundingBox();
	SlipwayYPadding = Box.GetSize().Y;

	Box = SlipwayMesh->GetBoundingBox();
	SlipwayXPadding = Box.GetSize().X;
}

void ASK_TileSpawner::AddRow()
{
	CurrentDim.X++;

	for (uint32 j = 0; j <= CurrentDim.X; j++)
	{
		if (j == 0)
		{
			SpawnStorageTile(CurrentDim.Y);
			continue;
		}

		if (j == 1)
		{
			SpawnMachineTile(j, CurrentDim.Y);
			continue;
		}

		SpawnMachineTile(j, CurrentDim.Y);
	}

	UpdateRowWidget();
	UpdateColumnWidget();
}

void ASK_TileSpawner::AddCol()
{

}

void ASK_TileSpawner::SpawnTiles(const FUint32Point Dim)
{
	if (Dim.X == 0 || Dim.Y == 0)
	{
		UE_LOG(ASK_TileSpawnerLog, Warning, TEXT("Grid parameters should not be zero"));
		return;
	}

	for (uint32 i = 0; i < Dim.Y; i++)
	{
		for (uint32 j = 0; j <= Dim.X; j++)
		{
			if (j == 0)
			{
				SpawnStorageTile(i);
				continue;
			}

			if (j == 1)
			{
				SpawnMachineTile(j, i);
				continue;
			}

			SpawnMachineTile(j, i);

			if (j == Dim.X && SlipwayQuantity > i)
			{
				SpawnSlipwayTile(j, i);
			}
		}
	}

	UpdateRowWidget();
	UpdateColumnWidget();
	SetActiveUpWidget();
	AddMachineTileToGM();
}

void ASK_TileSpawner::SpawnStorageTile(const int32 Index)
{
	const auto GW = GetWorld();
	if (GW)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		const FVector Location = GetActorLocation() - FVector(XPadding * Index, 0, 0);
		const auto Tile = GW->SpawnActor<ASK_TileActorBase>(TileStorageClass, Location, FRotator::ZeroRotator, SpawnParameters);
		if (Tile)
		{
			StorageTiles.Add(Tile);
			Tile->SetTilePosition(FTilePositionData(Tile, Index));
		}
		else
		{
			UE_LOG(ASK_TileSpawnerLog, Error, TEXT("StorageTile is nullptr!"));
		}
	}
}

void ASK_TileSpawner::SpawnMachineTile(const int32 IndexW, const int32 IndexH)
{
	const auto GW = GetWorld();
	if (GW)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		const FVector Location = GetActorLocation() - FVector(XPadding * IndexH, (YPadding * IndexW) + YWallPadding, 0);
		const auto Tile = GW->SpawnActor<ASK_TileActorBase>(TileMachineClass, Location, FRotator::ZeroRotator , SpawnParameters);
		if (Tile)
		{
			MachineTiles.Add(Tile);

			Tile->SetTilePosition(FTilePositionData(Tile, IndexW, IndexH));
		}
		else
		{
			UE_LOG(ASK_TileSpawnerLog, Error, TEXT("MachineTile is nullptr!"));
		}
	} 
}

void ASK_TileSpawner::SpawnSlipwayTile(const int32 IndexW, const int32 IndexH)
{
	const auto GW = GetWorld();
	if (GW)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		const FVector YOffset = MachineTiles.Last().Get()->GetActorLocation();

		FVector Location = GetActorLocation() - FVector(SlipwayXPadding * IndexH, 0, 0);
		Location.Y = YOffset.Y - (YPadding * 0.5) - (SlipwayYPadding * 0.5) - YWallPadding;

		const auto Tile = GW->SpawnActor<ASK_Slipway>(SlipwayClass, Location, FRotator::ZeroRotator, SpawnParameters);
		if (Tile)
		{
			SlipwayActors.Add(Tile);
		}
	}
}

void ASK_TileSpawner::UpdateRowWidget()
{
	if (StorageTiles.Num() > 0)
	{
		auto StartLocation = StorageTiles.Last().Get()->GetActorLocation();
		FVector Offset = FVector(XPadding * 0.7, YPadding * 1.25 + YWallPadding, 0);
		auto NewWorldLocation = StartLocation - Offset;
		ExpansionRowWidgetComponent->SetWorldLocation(NewWorldLocation);
		UE_LOG(LogTemp, Error, TEXT("Name: %s"), *NewWorldLocation.ToString());
	}
}

void ASK_TileSpawner::UpdateColumnWidget()
{
	const auto StartLocation = MachineTiles.Last().Get()->GetActorLocation();
	const FVector Offset = FVector(0, XPadding * 0.45, 0);
	auto NewWorldLocation = StartLocation - Offset;
	ExpansionColumnWidgetComponent->SetWorldLocation(NewWorldLocation);
	UE_LOG(LogTemp, Error, TEXT("Name: %s"), *NewWorldLocation.ToString());
}

void ASK_TileSpawner::InitWidgets()
{
	if (ExpansionRowWidgetComponent)
	{
		//ExpansionRowWidgetComponent->SetWidgetClass(ButtonBuyMachineTileClass);
		auto Widget = Cast<USKW_PopupWithButton>(ExpansionRowWidgetComponent->GetWidget());
		if (Widget)
		{
			ASK_GameHUD* HUD = Cast<ASK_GameHUD>(UGameplayStatics::GetActorOfClass(GetWorld(), ASK_GameHUD::StaticClass()));
			if (HUD)
			{
				Widget->SetHUD(HUD);
			}
			//Widget->OnPurchased.AddDynamic(this, &ASK_TileSpawner::AddRow);
		}

	}
	if (ExpansionColumnWidgetComponent)
	{
		//ExpansionColumnWidgetComponent->SetWidgetClass(ButtonBuyTileStorageClass);

		auto Widget = Cast<USKW_PopupWithButton>(ExpansionColumnWidgetComponent->GetWidget());
		if (Widget)
		{
			ASK_GameHUD* HUD = Cast<ASK_GameHUD>(UGameplayStatics::GetActorOfClass(GetWorld(), ASK_GameHUD::StaticClass()));
			if (HUD)
			{
				Widget->SetHUD(HUD);
			}
			//Widget->OnPurchased.AddDynamic(this, &ASK_TileSpawner::AddRow);
		}

	}
}
