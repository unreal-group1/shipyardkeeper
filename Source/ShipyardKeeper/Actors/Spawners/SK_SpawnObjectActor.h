// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SK_SpawnObjectActor.generated.h"

class UStaticMeshComponent;

UCLASS()
class SHIPYARDKEEPER_API ASK_SpawnObjectActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ASK_SpawnObjectActor();

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* StaticMeshComponent;

protected:
	UPROPERTY(VisibleAnywhere, Category = "Components")
	USceneComponent* SceneComponent;
};
