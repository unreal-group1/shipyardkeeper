// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/SK_TileActorBase.h"
#include "SK_Types.h"
#include "SK_MachineTile.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnLevelUp,const FMachineLevelInfo&);

class ASK_MachineActorBase;
class UMaterialInstance;
class ASK_AIPawnBase;
class UBoxComponent;
class USK_PopupHintWidgetComponent;

UCLASS()
class SHIPYARDKEEPER_API ASK_MachineTile : public ASK_TileActorBase
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditDefaultsOnly,Category = "MachineActor")
	TSubclassOf<ASK_MachineActorBase> MachineActorClass;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "MachineActor", meta = (MakeEditWidget = true))
	FVector SecondOne;

	UPROPERTY(EditAnywhere, Category = "MachineActor", meta = (MakeEditWidget = true))
	FVector SecondTwo;

	UPROPERTY(EditAnywhere, Category = "MachineActor", meta = (MakeEditWidget = true))
	FVector ThirdOne;

	UPROPERTY(EditAnywhere, Category = "MachineActor", meta = (MakeEditWidget = true))
	FVector ThirdTwo;

	UPROPERTY(EditAnywhere, Category = "MachineActor", meta = (MakeEditWidget = true))
	FVector ThirdThree;

	UPROPERTY(EditDefaultsOnly,Category = "Components")
	UBoxComponent* BoxComponent;

	UPROPERTY(EditAnywhere,Category = "Materials")
	TObjectPtr<UMaterialInstance> Background;

	UPROPERTY(EditAnywhere, Category = "Materials")
	TObjectPtr<UMaterialInstance> WorkingTile;
public:
	ASK_MachineTile();

	void CreateMachineActor(const FMachineType& MachineType);

	FOnLevelUp OnLevelUp;

	void SpawnMachine(const FName& MachineRowName);

	UFUNCTION(BlueprintCallable)
	void TestChangeMaterial();

	const int32 GetCurrentLevel() { return CurrentLevel; }

	UFUNCTION()
	bool HaveMachine();

	UFUNCTION(BlueprintCallable)
	FName GetMachineTypeName();

	UFUNCTION(BlueprintCallable)
	void LevelUp();

	UFUNCTION(BlueprintCallable)
	const FMachineLevelInfo GetLevelInfo(const int32 Index);

	const FName& GetCurrentName() { return CurrentName; }
private:

	float CurrentTime{ 0.f };

	int32 CurrentLevel;
	FName CurrentName;

	TArray<FMachineLevelInfo> CurrentLevelInfo;

	TEnumAsByte<EMachineWorkplaces> CurrentNumberOfMachines;

	UPROPERTY()
	TArray<TObjectPtr<ASK_MachineActorBase>> MachineActors;

	bool GetNextLevelInfo(FMachineLevelInfo& LevelInfo);

	void UpdateNumOfMashine(const TEnumAsByte<EMachineWorkplaces>& Num);

	UFUNCTION()
	void OnComponentOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

#pragma region UI
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Component")
	USK_PopupHintWidgetComponent* PopupUpgradeComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Component")
	USK_PopupHintWidgetComponent* PopupDestroyMachinesComponent;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Component")
	//USK_PopupHintWidgetComponent* AcceptWindowComponent;

	UFUNCTION(BlueprintCallable)
	void Show();
	UFUNCTION(BlueprintCallable)
	void Hide();

	UFUNCTION(BlueprintCallable)
	void UpdateUpgradeCostInWidget();

	UFUNCTION(BlueprintCallable)
	void ClearMachines();
private:
	FVector PopupHintDefaultLocation;
	FVector PopupUpgradeDefaultLocation;
	FVector PopupDestroyMachinesDefaultLocation;
	//FVector PopupAcceptWindowComponentLocation;
#pragma endregion UI
};
