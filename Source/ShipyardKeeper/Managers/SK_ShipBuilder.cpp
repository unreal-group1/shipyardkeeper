// Fill out your copyright notice in the Description page of Project Settings.

#include "SK_ShipBuilder.h"
#include "SK_GameMode.h"
#include "SK_PlatformGameInstance.h"
#include "SK_ShipActor.h"
#include "SK_Slipway.h"
#include "SK_PopupHintWidgetComponent.h"

#include "SKW_Ship.h"

DEFINE_LOG_CATEGORY_STATIC(USK_ShipBuilderLog, All, All);

void USK_ShipBuilder::Init()
{
	const auto& World = GetWorld();
	if (!World) return;

	GameInstance = World->GetGameInstance<USK_PlatformGameInstance>();
	if (!GameInstance) return;

	const auto GameMode = World->GetAuthGameMode<ASK_GameMode>();
	if (!GameMode) return;

	CurrentSlipwayLevel = GameMode->GetSlipwayLevel();
	CurrentShipName = GameMode->GetCurrentProject();
	SpawnShip();
	InitUpLevelWidget();

	if (SlipwayActors.IsValidIndex(CurrentSlipwayLevel.GetIntValue()))
	{
		if (SlipwayActors[CurrentSlipwayLevel.GetIntValue()]->PopupHintWidgetComponent)
		{
			SlipwayActors[CurrentSlipwayLevel.GetIntValue()]->PopupHintWidgetComponent->Show();
		}
	}
}

void USK_ShipBuilder::AddSlipway(const TObjectPtr<ASK_Slipway>& SlipwayActor)
{
	SlipwayActors.Add(SlipwayActor);
}

bool USK_ShipBuilder::TryGetWork(FVector& Workplace, int32& index,TObjectPtr<ASK_Slipway>& SlipwayActor)
{
	if (!IsPossibleToBuild()) return false;

	const uint32 LastIndex = CurrentSlipwayLevel.GetIntValue() + 1;

	for (uint32 i = 0; i < LastIndex; i++)
	{
		if (SlipwayActors[i]->CanWork(Workplace, index, SlipwayActor))
		{
			return true;
		}
	}

	return false;
}

bool USK_ShipBuilder::IsCanWork()
{
	if (!IsPossibleToBuild()) return false;

	const uint32 LastIndex = CurrentSlipwayLevel;

	for (uint32 i = 0; i < LastIndex; i++)
	{
		if (SlipwayActors[i]->IsCanWork())
		{
			return true;
		}
	}

	return false;
}

bool USK_ShipBuilder::IsPossibleToBuild()
{
	UpdateRequiredResourse();
	
	return GameInstance->IsPossibleToBuild(RequiredResourse, ResourcesForTick);
}

bool USK_ShipBuilder::IsBuildComplited()
{
	if (CurrentEndurance < MaxEndurance)
	{
		for (const auto& Resourse : RequiredResourse)
		{
			if (Resourse.ResourceQuantity > 0) return false;
		}
	}
	else
	{
		for (auto& Resourse : RequiredResourse)
		{
			Resourse.ResourceQuantity = 0;
		}
	}

	if (CurrentShipActor)
	{
		CurrentShipActor->Complited(LifeSpan);
		CurrentShipActor = nullptr;
		GameInstance->AddToBank(Reward);
		
		const auto Index = Ships.IndexOfByPredicate([&](const FShips& Ship) {

			return Ship.ShipRowName == CurrentShipName;
			});

		if (Index != INDEX_NONE)
		{
			Ships[Index].Num++;
		}
		else
		{
			Ships.Add(FShips(CurrentShipName, 1));
		}
		UE_LOG(USK_ShipBuilderLog, Warning, TEXT("% i added to the bank"), Reward);
	}

	if(!bIsSpawning) InitNewSpawn();
	return true;
}

bool USK_ShipBuilder::HaveTask()
{
	return CurrentShipActor ? true : false;
}

void USK_ShipBuilder::UpdateRequiredResourse()
{
	RequiredResourse.Empty();

	if (NeededResources.Num() != NestedResources.Num()) return;

	for (int32 i = 0; i < NeededResources.Num(); i++)
	{
		RequiredResourse.Add(FResource(NeededResources[i].ResourcesRowName,
			FMath::Clamp(NeededResources[i].ResourceQuantity - NestedResources[i].ResourceQuantity,0,MAX_int32)));
	}
}

FName USK_ShipBuilder::GetCurrentShipName()
{
	if (CurrentShipName.IsNone())
	{
		auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
		if (GI)
		{
			auto ShipType = GI->GetFirstShipType();
			CurrentShipName = FName(ShipType->ShowingData.RowName.ToString());
		}
	}
	return CurrentShipName;
}

void USK_ShipBuilder::AddEndurance()
{
	if (!GameInstance->SpendResources(RequiredResourse, NestedResources, ResourcesForTick))
	{
		//UE_LOG(USK_ShipBuilderLog, Error, TEXT("SpendResources was false"));
		return;
	}

	CurrentEndurance++;
	CurrentShipActor->ChaderBoxUpdate(CurrentEndurance / MaxEndurance);
	OnEnduranceChange.Broadcast(CurrentEndurance, MaxEndurance);

	if (SlipwayActors.Num() > 0)
	{
		if (SlipwayActors.IsValidIndex(CurrentSlipwayLevel.GetIntValue()))
		{
			SetIsNotBlockedWidget();
		}
	}
}

double USK_ShipBuilder::GetMaxEndurance()
{
	return MaxEndurance;
}

double USK_ShipBuilder::GetCurrentEndurance()
{
	return CurrentEndurance;
}

void USK_ShipBuilder::ClearWorkplace(const TObjectPtr<ASK_Slipway>& InSlipway, const uint32 Index)
{
	for (const auto& Slipway : SlipwayActors)
	{
		if (Slipway == InSlipway)
		{
			InSlipway->ClearWorkplace(Index);
		}
	}
}

void USK_ShipBuilder::SlipwayLevelUpdate(const int32 NewLevel)
{
	CurrentSlipwayLevel = TEnumAsByte<ESlipwayLevel>(NewLevel);

	if (SlipwayActors.IsValidIndex(CurrentSlipwayLevel.GetIntValue()))
	{
		SlipwayActors[CurrentSlipwayLevel.GetIntValue()]->HideWidget();

		SetIsNotBlockedWidget();

		if (SlipwayActors.IsValidIndex(CurrentSlipwayLevel.GetIntValue() + 1))
		{
			SlipwayActors[CurrentSlipwayLevel.GetIntValue() + 1]->ShowWidget();
		}
	}
}

void USK_ShipBuilder::UpdateProject()
{
	if (CurrentShipName.IsNone())
	{
		SpawnShip();
	}
}

void USK_ShipBuilder::BuildCancel()
{
	if (CurrentShipActor)
	{
		CurrentShipActor->Destroy();
	}

	if (GameInstance)
	{
		GameInstance->ReturnResources(NestedResources);
	}
	NestedResources.Empty();
	bIsSpawning = true;

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(BuildCancelPauseHandle,
			this, 
			&USK_ShipBuilder::InitNewSpawn,
			DurationPauseAfterBuildCancel, 
			false);
	}
}

void USK_ShipBuilder::AddEnduranceByValue(const int32 Value)
{
	if (!CurrentShipActor) return;

	if (!GameInstance->SpendResources(RequiredResourse, NestedResources, ResourcesForTick * Value))
	{
		UE_LOG(USK_ShipBuilderLog, Error, TEXT("SpendResources was false"));
	}

	CurrentEndurance += Value;
	CurrentShipActor->ChaderBoxUpdate(CurrentEndurance/ MaxEndurance);

	OnEnduranceChange.Broadcast(CurrentEndurance, MaxEndurance);

	IsBuildComplited();
}

void USK_ShipBuilder::SpawnShip()
{
	const auto& World = GetWorld();
	if (!World) return;

	bIsSpawning = false;

	const auto GameMode = World->GetAuthGameMode<ASK_GameMode>();
	if (!GameMode) return;

	if (CurrentShipName.IsNone())
	{
		CurrentShipName = GameMode->GetCurrentProject();
	}

	FShipTypes* ShipTypes = GameInstance->GetShipsBuyName(CurrentShipName);
	if (ShipTypes)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		const FVector SpawnLocation = GetSpawnLocation(ShipTypes->RequiredSlipwayLevel);

		const auto ShipClass = GameMode->GetShipActorClass();
		if (!ShipClass) return;
		CurrentShipActor = World->SpawnActor<ASK_ShipActor>(ShipClass, SpawnLocation, FRotator::ZeroRotator, SpawnParameters);
		if (CurrentShipActor)
		{
			LifeSpan = ShipTypes->LifeSpan;
			NeededResources = ShipTypes->NeededResources;

			if (CurrentShipActor->StaticMeshComponent)
			{
				CurrentShipActor->SetStaticMesh(ShipTypes->Mesh);
			}

			if (CurrentShipActor->PopupHintWidgetComponent)
			{
				CurrentShipActor->PopupHintWidgetComponent->SetWidgetClass(ShipWidgetClass);
				CurrentShipActor->PopupHintWidgetComponent->Show();
			}

			double Amount = 0.0;
			for (const auto& Resourse : NeededResources)
			{
				Amount += Resourse.ResourceQuantity;
			}
			ResourcesForTick = Amount / ShipTypes->Endurance;
			NestedResources = NeededResources;

			for (auto& Resourse : NestedResources)
			{
				Resourse.ResourceQuantity = 0.0;
			}

			CurrentEndurance = 0.0;
			Reward = ShipTypes->Reward;
			MaxEndurance = ShipTypes->Endurance;
			UpdateRequiredResourse();
		}
	}
}

const FVector USK_ShipBuilder::GetSpawnLocation(const ESlipwayLevel& SlipwayLevel)
{
	if (SlipwayActors.IsEmpty())
	{
		UE_LOG(USK_ShipBuilderLog, Error, TEXT("SlipwayActors is empty!"));
		return FVector::ZeroVector;
	}
	switch (SlipwayLevel)
	{
	case ESlipwayLevel::One_Level:
	{
		return SlipwayActors[0]->GetActorLocation();
	}

	case ESlipwayLevel::Two_Level:
	{
		FVector SpawnLocation = SlipwayActors[0]->GetActorLocation();
		SpawnLocation.X = (SlipwayActors[1]->GetActorLocation().X + SpawnLocation.X) * 0.5 ;
		return SpawnLocation;
	}

	case ESlipwayLevel::Three_Level:	
	{
		return SlipwayActors[1]->GetActorLocation();
	}

	case ESlipwayLevel::Fourth_Level:
	{
		FVector SpawnLocation = SlipwayActors[1]->GetActorLocation();
		SpawnLocation.X = (SlipwayActors[2]->GetActorLocation().X + SpawnLocation.X) * 0.5;
		return SpawnLocation;
	}

	default:
		UE_LOG(USK_ShipBuilderLog, Error, TEXT("the required level is missing"));
		return FVector::ZeroVector;
	}
	return FVector::ZeroVector;
}

void USK_ShipBuilder::InitUpLevelWidget()
{
	//GameInstance


}

void USK_ShipBuilder::InitNewSpawn()
{
	if (!GetWorld()) return;

	bIsSpawning = true;

	const auto GameMode = GetWorld()->GetAuthGameMode<ASK_GameMode>();
	if (!GameMode) return;

	auto CurrentProject = GameMode->GetCurrentProject();
	if (CurrentShipName != CurrentProject)
	{
		OnCurrentShipProjectChange.Broadcast(CurrentProject);
	}

	CurrentShipName = CurrentProject;
	FShipTypes Type;

	if (GameInstance->GetShipsBuyName(CurrentShipName, Type))
	{
		GetWorld()->GetTimerManager().SetTimer(NewSpawnHandle, this, &USK_ShipBuilder::SpawnShip, Type.SpawnDelay, false);
	}
}

void USK_ShipBuilder::SetShipWidgetClass(TSubclassOf<class USKW_Ship> NewShipWidgetClass)
{
	ShipWidgetClass = NewShipWidgetClass;
}

void USK_ShipBuilder::SetIsNotBlockedWidget()
{
	auto PopupHintWidgetComponent = SlipwayActors[CurrentSlipwayLevel.GetIntValue()]->PopupHintWidgetComponent;
	if (PopupHintWidgetComponent)
	{
		auto Widget = Cast<USKW_Ship>(PopupHintWidgetComponent->GetWidget());
		if (Widget)
		{
			Widget->SetIsSlipwayNotBlocked(true);
		}
	}
}
