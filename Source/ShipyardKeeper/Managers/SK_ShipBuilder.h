// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "SK_Types.h"
#include "SK_ShipBuilder.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnEnduranceChange, double, Current, double, Max);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCurrentShipProjectChange, FName, NewShipProject);

class USK_PlatformGameInstance;
class ASK_ShipActor;
class ASK_Slipway;

UCLASS()
class SHIPYARDKEEPER_API USK_ShipBuilder : public UObject
{
	GENERATED_BODY()
	
public:
	FOnEnduranceChange OnEnduranceChange;
	FOnCurrentShipProjectChange OnCurrentShipProjectChange;

	void Init();

	void AddSlipway(const TObjectPtr<ASK_Slipway>& SlipwayActor);

	const TArray<FResource>& GetNestedResources() { return NestedResources; }
	const TArray<FResource>& GetRequiredResourse() { return RequiredResourse; }

	bool TryGetWork(FVector& Workplace, int32& index,TObjectPtr<ASK_Slipway>& SlipwayActor);
	bool IsCanWork();
	bool IsPossibleToBuild();
	bool IsBuildComplited();
	bool HaveTask();

	void UpdateRequiredResourse();

	FName GetCurrentShipName();

	void AddEndurance();
	double GetMaxEndurance();
	double GetCurrentEndurance();

	void ClearWorkplace(const TObjectPtr<ASK_Slipway>& Slipway, const uint32 Index);
	void SlipwayLevelUpdate(const int32 NewLevel);

	void UpdateProject();

	void AddEnduranceByValue(const int32 Value);

	const TArray<FShips>& GetShips() { return Ships; }

	
	const ASK_ShipActor* GetCurrentShipActor() { return CurrentShipActor; }
protected:

	void SpawnShip();

	const FVector GetSpawnLocation(const ESlipwayLevel& SlipwayLevel);

	void InitUpLevelWidget();
	void InitNewSpawn();
private:
	UPROPERTY()
	TObjectPtr<USK_PlatformGameInstance> GameInstance;

	UPROPERTY()
	TObjectPtr<ASK_ShipActor> CurrentShipActor;
	UPROPERTY()
	FName CurrentShipName;

	UPROPERTY()
	TArray<TObjectPtr<ASK_Slipway>> SlipwayActors;

	UPROPERTY()
	TArray<FResource> NestedResources;

	UPROPERTY()
	TArray<FResource> NeededResources;

	UPROPERTY()
	TArray<FResource> RequiredResourse;

	double MaxEndurance{ 0.0 };
	double CurrentEndurance{ 0.0 };

	TEnumAsByte<ESlipwayLevel> CurrentSlipwayLevel;

	double ResourcesForTick{ 0.0 };

	uint32 Reward{ 0 };
	FTimerHandle NewSpawnHandle;

	bool bIsSpawning{ false };

	float LifeSpan{ 5.f };

	TArray<FShips> Ships;
#pragma region UI
	public :
	void SetShipWidgetClass(TSubclassOf<class USKW_Ship> NewShipWidgetClass);

	void SetIsNotBlockedWidget();

	private:  
	TSubclassOf<class USKW_PopupWithProgressbar> ShipWidgetClass;
#pragma endregion UI
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "BuildCancel")
	float DurationPauseAfterBuildCancel = 2.0f;

	void BuildCancel();
private:
	FTimerHandle BuildCancelPauseHandle;
};
