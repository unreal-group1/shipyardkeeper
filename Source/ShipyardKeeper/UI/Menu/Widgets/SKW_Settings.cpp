// Created by Pavlov P.A.

#include "SKW_Settings.h"
#include "SKW_Button.h"
#include "SKW_Text.h"
#include "SKW_Switcher.h"

#include "SK_MenuHUD.h"

#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "GameFramework/GameUserSettings.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

void USKW_Settings::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (ResolutionScreenSwitcher
        && ResolutionScreenSwitcher->NextElementButton
        && ResolutionScreenSwitcher->NextElementButton->CustomizeButton
        && ResolutionScreenSwitcher->PreviousElementButton
        && ResolutionScreenSwitcher->PreviousElementButton->CustomizeButton)
    {
        ResolutionScreenSwitcher->NextElementButton->CustomizeButton->OnClicked.AddDynamic(this, 
            &USKW_Settings::OnApplyNextResolutionScreen);
        ResolutionScreenSwitcher->PreviousElementButton->CustomizeButton->OnClicked.AddDynamic(this, 
            &USKW_Settings::OnApplyPreviousResolutionScreen);
    }

    if (GraphicsQualitySwitcher
        && GraphicsQualitySwitcher->NextElementButton
        && GraphicsQualitySwitcher->NextElementButton->CustomizeButton
        && GraphicsQualitySwitcher->PreviousElementButton
        && GraphicsQualitySwitcher->PreviousElementButton->CustomizeButton)
    {
        GraphicsQualitySwitcher->NextElementButton->CustomizeButton->OnClicked.AddDynamic(this,
            &USKW_Settings::OnApplyNextGraphicsQuality);
        GraphicsQualitySwitcher->PreviousElementButton->CustomizeButton->OnClicked.AddDynamic(this,
            &USKW_Settings::OnApplyPreviousGraphicsQuality);
    }

    if (MenuButton
        && MenuButton->CustomizeButton)
    {
        MenuButton->CustomizeButton->OnClicked.AddDynamic(this, 
            &USKW_Settings::OpenMenu);
    }
}

void USKW_Settings::NativePreConstruct()
{
    GetCurrentScreenResolution();
    GetCurrentGraphicsQuality();
}

void USKW_Settings::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
    Super::OnAnimationFinished_Implementation(Animation);

    if (Animation == OpenMenuAnimation)
    {
        auto MenuHUD = Cast<ASK_MenuHUD>(GetHUD());
        if (MenuHUD)
        {
            SetVisibility(ESlateVisibility::Hidden);

            MenuHUD->SetNextMenuWidget();
        }
    }
}

void USKW_Settings::OpenMenu()
{
    UE_LOG(LogTemp, Warning, TEXT("USKW_Settings:OpenMenu"));

    PlayAnimation(OpenMenuAnimation);
    if (!GetWorld())
    {
        UGameplayStatics::PlaySound2D(GetWorld(), OpenMenuSound);
    }
}

void USKW_Settings::CloseMenu()
{
    UE_LOG(LogTemp, Warning, TEXT("USKW_Settings:CloseMenu"));

    SetVisibility(ESlateVisibility::SelfHitTestInvisible);
    PlayAnimation(CloseMenuAnimation);
    if (!GetWorld())
    {
        UGameplayStatics::PlaySound2D(GetWorld(), CloseMenuSound);
    }
}

#pragma region SwitcherResolutionScreen
void USKW_Settings::OnApplyNextResolutionScreen()
{
    CurrentIndexScreenResolution++;

    if (CurrentIndexScreenResolution > ScreenResolution.Num() - 1)
    {
        CurrentIndexScreenResolution = 0;
    }

    SetNewScreenResolution();
}

void USKW_Settings::OnApplyPreviousResolutionScreen()
{
    CurrentIndexScreenResolution--;

    if (CurrentIndexScreenResolution < 0)
    {
        CurrentIndexScreenResolution = ScreenResolution.Num() - 1;
    }

    SetNewScreenResolution();
}

FString USKW_Settings::GetScreenResolutionText()
{
    const FString Separator = " x ";

    return FString::FromInt(ScreenResolution[CurrentIndexScreenResolution].X)
        + Separator
        + FString::FromInt(ScreenResolution[CurrentIndexScreenResolution].Y);
}

void USKW_Settings::SetNewScreenResolution()
{
    if (ScreenResolution[CurrentIndexScreenResolution] != FIntPoint::ZeroValue)
    {
        if (GEngine)
        {
            UGameUserSettings* Settings = GEngine->GetGameUserSettings();

            if (Settings)
            {
                Settings->SetScreenResolution(ScreenResolution[CurrentIndexScreenResolution]);

                FString ScreenResolutionText = GetScreenResolutionText();

                if (ResolutionScreenSwitcher
                    && ResolutionScreenSwitcher->Element)
                {
                    ResolutionScreenSwitcher->Element->SetText(
                        ScreenResolutionText);
                }
            }
        }
    }
    UE_LOG(LogTemp, Display, TEXT("USKW_Settings:SetNewScreenResolution: CurrentIndexScreenResolution %i"), CurrentIndexScreenResolution);
}

void USKW_Settings::GetCurrentScreenResolution()
{
    if (GEngine)
    {
        UGameUserSettings* Settings = GEngine->GetGameUserSettings();

        if (Settings)
        {
            auto CurrentScreenResolution = Settings->GetScreenResolution();

            ScreenResolution[CurrentIndexScreenResolution] = CurrentScreenResolution;

            FString ScreenResolutionText = GetScreenResolutionText();

            if (ResolutionScreenSwitcher
                && ResolutionScreenSwitcher->Element
                && ResolutionScreenSwitcher->Element->CustomizeTextBlock)
            {
                ResolutionScreenSwitcher->Element->CustomizeTextBlock->SetText(
                    FText::FromString(ScreenResolutionText));
            }
        }
    }
}
#pragma endregion SwitcherResolutionScreen

#pragma region SwitcherGraphicsQuality
void USKW_Settings::OnApplyNextGraphicsQuality()
{
    CurrentGraphicsQuality++;

    if (CurrentGraphicsQuality > MaxGraphicsQuality)
    {
        CurrentGraphicsQuality = MinGraphicsQuality;
    }

    SetNewGraphicsQuality();
}

void USKW_Settings::OnApplyPreviousGraphicsQuality()
{
    CurrentGraphicsQuality--;

    if (CurrentGraphicsQuality < MinGraphicsQuality)
    {
        CurrentGraphicsQuality = MaxGraphicsQuality;
    }

    SetNewGraphicsQuality();
}

FString USKW_Settings::GetGraphicsQualityText()
{
    return GraphicsQuality[CurrentGraphicsQuality].GraphicsQualityName.ToString();
}

void USKW_Settings::SetNewGraphicsQuality()
{
    if (GEngine)
    {
        UGameUserSettings* Settings = GEngine->GetGameUserSettings();

        if (Settings)
        {
            Settings->SetOverallScalabilityLevel(CurrentGraphicsQuality);

            FString GraphicsQualityText = GetGraphicsQualityText();

            if (GraphicsQualitySwitcher
                && GraphicsQualitySwitcher->Element
                && GraphicsQualitySwitcher->Element->CustomizeTextBlock)
            {
                GraphicsQualitySwitcher->Element->CustomizeTextBlock->SetText(
                    FText::FromString(GraphicsQualityText));
            }
        }

        UE_LOG(LogTemp, Display, TEXT("USKW_Settings:SetNewGraphicsQuality: CurrentGraphicsQuality %i"), CurrentGraphicsQuality);
    }
}

void USKW_Settings::GetCurrentGraphicsQuality()
{
    if (GEngine)
    {
        UGameUserSettings* Settings = GEngine->GetGameUserSettings();

        if (Settings)
        {
            auto CurrentOverallScalabilityLevel = Settings->GetOverallScalabilityLevel();

            CurrentGraphicsQuality = CurrentOverallScalabilityLevel;

            FString GraphicsQualityText = GetGraphicsQualityText();

            if (GraphicsQualitySwitcher
                && GraphicsQualitySwitcher->Element
                && GraphicsQualitySwitcher->Element->CustomizeTextBlock)
            {
                GraphicsQualitySwitcher->Element->CustomizeTextBlock->SetText(
                    FText::FromString(GraphicsQualityText));
            }
        }
    }
}
#pragma endregion SwitcherGraphicsQuality