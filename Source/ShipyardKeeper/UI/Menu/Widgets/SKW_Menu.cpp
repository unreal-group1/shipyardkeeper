// Created by Pavlov P.A.

#include "SKW_Menu.h"
#include "SKW_Button.h"
#include "SK_PlatformGameInstance.h"
#include "SK_MenuHUD.h"

#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

void USKW_Menu::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (StartGameButton
        && StartGameButton->CustomizeButton)
    {
        StartGameButton->CustomizeButton->OnClicked.AddDynamic(this, 
            &USKW_Menu::OnStartGame);
    }

    if (SettingsGameButton
        && SettingsGameButton->CustomizeButton)
    {
        SettingsGameButton->CustomizeButton->OnClicked.AddDynamic(this, 
            &USKW_Menu::OnSettingGame);
    }

    if (QuitGameButton
        && QuitGameButton->CustomizeButton)
    {
        QuitGameButton->CustomizeButton->OnClicked.AddDynamic(this, 
            &USKW_Menu::OnQuitGame);
    }
}

void USKW_Menu::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
    Super::OnAnimationFinished_Implementation(Animation);

    if (Animation == HideAnimation)
    {
        auto GameInstance = Cast<USK_PlatformGameInstance>(GetGameInstance());
        if (!GameInstance)
        {
            UE_LOG(LogTemp, Error, TEXT("USKW_Menu: GameInstance is not USK_PlatformGameInstance"));
            return;
        }
        SetVisibility(ESlateVisibility::Hidden);

        GameInstance->SetCurrentLevelName(GameInstance->GetLevelsNames()[0]);

        UGameplayStatics::OpenLevel(this, GameInstance->GetCurrentLevelName());
    }

    if (Animation == OpenSettingsAnimation)
    {
        auto MenuHUD = Cast<ASK_MenuHUD>(GetHUD());
        if (MenuHUD)
        {
            SetVisibility(ESlateVisibility::Hidden);

            MenuHUD->SetNextSettingsWidget();
        }
    }
}

void USKW_Menu::OpenSettings()
{
    UE_LOG(LogTemp, Warning, TEXT("USKW_Menu:OpenSettings"));

    PlayAnimation(OpenSettingsAnimation);
    if (!GetWorld())
    {
        UGameplayStatics::PlaySound2D(GetWorld(), OpenSettingsSound);
    }
}

void USKW_Menu::CloseSettings()
{
    UE_LOG(LogTemp, Warning, TEXT("USKW_Menu:CloseSettings"));

    SetVisibility(ESlateVisibility::SelfHitTestInvisible);

    PlayAnimation(CloseSettingsAnimation);
    if (!GetWorld())
    {
        UGameplayStatics::PlaySound2D(GetWorld(), CloseSettingsSound);
    }
}

void USKW_Menu::OnStartGame()
{
    Hide();
}

void USKW_Menu::OnSettingGame()
{
    OpenSettings();
}

void USKW_Menu::OnQuitGame()
{
	UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(), EQuitPreference::Quit, true);
}
