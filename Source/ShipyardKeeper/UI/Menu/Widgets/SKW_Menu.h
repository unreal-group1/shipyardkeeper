// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"

#include "SKW_Base.h"
#include "SKW_Menu.generated.h"

class USKW_Button;

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_Menu : public USKW_Base
{
	GENERATED_BODY()

	public:
	UPROPERTY(meta = (BindWidgetAnim), Transient)
	UWidgetAnimation* OpenSettingsAnimation;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
	USoundBase* OpenSettingsSound;
	UFUNCTION()
	void OpenSettings();

	UPROPERTY(meta = (BindWidgetAnim), Transient)
	UWidgetAnimation* CloseSettingsAnimation;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
	USoundBase* CloseSettingsSound;
	UFUNCTION()
	void CloseSettings();

	protected:
	UPROPERTY(meta = (BindWidget))
	USKW_Button* StartGameButton;

	UPROPERTY(meta = (BindWidget))
	USKW_Button* SettingsGameButton;

	UPROPERTY(meta = (BindWidget))
	USKW_Button* QuitGameButton;

	UFUNCTION()
	virtual void NativeOnInitialized() override;
	UFUNCTION()
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;

	private:
	UFUNCTION()
	void OnStartGame();

	UFUNCTION()
	void OnSettingGame();

	UFUNCTION()
	void OnQuitGame();
};
