// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"

#include "SKW_Base.h"
#include "SKW_Settings.generated.h"

USTRUCT(BlueprintType)
struct FGraphicsQuality
{
	GENERATED_USTRUCT_BODY()

	public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FName GraphicsQualityName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	uint8 GraphicsQualityIndex;
};

class USKW_Text;
class USKW_Button;
class USKW_Switcher;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_Settings : public USKW_Base
{
	GENERATED_BODY()
	 
#pragma region SwitcherResolutionScreen
public:
	UPROPERTY(meta = (BindWidget))
	USKW_Text* ResolutionScreenFieldName;
	UPROPERTY(meta = (BindWidget))
	USKW_Switcher* ResolutionScreenSwitcher;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
	TArray<FIntPoint> ScreenResolution;

	private:
	int8 CurrentIndexScreenResolution = 0;
	UFUNCTION()
	FString GetScreenResolutionText();
	UFUNCTION()
	void SetNewScreenResolution();
	UFUNCTION()
	void GetCurrentScreenResolution();
	UFUNCTION()
	void OnApplyNextResolutionScreen();
	UFUNCTION()
	void OnApplyPreviousResolutionScreen();
#pragma endregion SwitcherResolutionScreen

#pragma region SwitcherGraphicsQuality
public:
	UPROPERTY(meta = (BindWidget))
	USKW_Text* GraphicsQualityFieldName;
	UPROPERTY(meta = (BindWidget))
	USKW_Switcher* GraphicsQualitySwitcher;
	/// <summary>
	/// Value	0:low, 1:medium, 2:high, 3:epic, 4:cinematic
	/// https://docs.unrealengine.com/5.0/en-US/API/Runtime/Engine/GameFramework/UGameUserSettings/SetOverallScalabilityLevel/
	/// </summary>
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings", meta = (ToolTip = "Value	0:low, 1:medium, 2:high, 3:epic, 4:cinematic"))
	TArray<FGraphicsQuality> GraphicsQuality;

private:
	/// <summary>
	/// Value	0:low, 1:medium, 2:high, 3:epic, 4:cinematic
	/// https://docs.unrealengine.com/5.0/en-US/API/Runtime/Engine/GameFramework/UGameUserSettings/SetOverallScalabilityLevel/
	/// </summary>
	int8 CurrentGraphicsQuality = 4;
	const int8 MinGraphicsQuality = 0;
	const int8 MaxGraphicsQuality = 4;

	UFUNCTION()
	FString GetGraphicsQualityText();
	UFUNCTION()
	void SetNewGraphicsQuality();
	UFUNCTION()
	void GetCurrentGraphicsQuality();
	UFUNCTION()
	void OnApplyNextGraphicsQuality();
	UFUNCTION()
	void OnApplyPreviousGraphicsQuality();
#pragma endregion SwitcherResolutionScreen

public:
	UPROPERTY(meta = (BindWidget))
	USKW_Button* MenuButton;

	UPROPERTY(meta = (BindWidgetAnim), Transient)
	UWidgetAnimation* OpenMenuAnimation;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
	USoundBase* OpenMenuSound;
	UFUNCTION()
	void OpenMenu();

	UPROPERTY(meta = (BindWidgetAnim), Transient)
	UWidgetAnimation* CloseMenuAnimation;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
	USoundBase* CloseMenuSound;
	UFUNCTION()
	void CloseMenu();

	UFUNCTION()
	virtual void NativeOnInitialized() override;
	virtual void NativePreConstruct() override;
	UFUNCTION()
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;
};
