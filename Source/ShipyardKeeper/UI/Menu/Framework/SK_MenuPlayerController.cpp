// Created by Pavlov P.A.

#include "SK_MenuPlayerController.h"

void ASK_MenuPlayerController::BeginPlay()
{
    Super::BeginPlay();

    SetInputMode(FInputModeUIOnly());
    bShowMouseCursor = true;
}
