// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SK_MenuGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API ASK_MenuGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
	public:
	ASK_MenuGameModeBase();
};
