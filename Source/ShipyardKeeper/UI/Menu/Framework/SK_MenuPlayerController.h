// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SK_MenuPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API ASK_MenuPlayerController : public APlayerController
{
	GENERATED_BODY()
	
	protected:
	virtual void BeginPlay() override;
};
