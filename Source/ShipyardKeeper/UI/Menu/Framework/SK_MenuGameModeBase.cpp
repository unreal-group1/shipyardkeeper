// Created by Pavlov P.A.

#include "SK_MenuGameModeBase.h"
#include "SK_MenuPlayerController.h"
#include "SK_MenuHUD.h"

ASK_MenuGameModeBase::ASK_MenuGameModeBase()
{
    if (!ASK_MenuPlayerController::StaticClass())
    {
        UE_LOG(LogTemp, Error, TEXT("ASK_MenuGameModeBase: ASK_MenuPlayerController not have StaticClass"));
    }
    else
    {
        PlayerControllerClass = ASK_MenuPlayerController::StaticClass();
    }

    if (!ASK_MenuHUD::StaticClass())
    {
        UE_LOG(LogTemp, Error, TEXT("ASK_MenuGameModeBase: ASK_MenuHUD not have StaticClass"));
    }
    else
    {
        HUDClass = ASK_MenuHUD::StaticClass();
    }
}
