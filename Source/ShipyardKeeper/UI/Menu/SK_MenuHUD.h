// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SK_MenuHUD.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API ASK_MenuHUD : public AHUD
{
	GENERATED_BODY()

	public:
	void SetNextSettingsWidget();
	void SetNextMenuWidget();
	
	protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<class USKW_Menu> MenuWidgetClass;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<class USKW_Settings> SettingsWidgetClass;

	virtual void BeginPlay() override;
	UFUNCTION()
	void OpenSettings();
	UFUNCTION()
	void OpenMenu();

	private:
	class USKW_Menu* MenuWidget;
	class USKW_Settings* SettingsWidget;

	class USKW_Base* NextWidget;
};
