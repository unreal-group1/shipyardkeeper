// Created by Pavlov P.A.

#include "SK_MenuHUD.h"
#include "SKW_Base.h"
#include "SKW_Menu.h"
#include "SKW_Settings.h"

void ASK_MenuHUD::SetNextSettingsWidget()
{
    NextWidget = SettingsWidget;

    if (NextWidget)
    {
        OpenSettings();
    }
    else
    {
        UE_LOG(LogTemp, Error, TEXT("ASK_MenuHUD:SetNextSettingsWidget: SettingsWidget is null"));
        return;
    }
}

void ASK_MenuHUD::SetNextMenuWidget()
{
    NextWidget = MenuWidget;

    if (NextWidget)
    {
        OpenMenu();
    }
    else
    {
        UE_LOG(LogTemp, Error, TEXT("ASK_MenuHUD:SetNextMenuWidget: MenuWidget is null"));
        return;
    }
}

void ASK_MenuHUD::BeginPlay()
{
    Super::BeginPlay();

    if (!GetWorld()
        || !MenuWidgetClass)
    {
        UE_LOG(LogTemp, Error, TEXT("ASK_MenuHUD:BeginPlay: World or MenuWidgetClass is empty"));
        return;
    }

    MenuWidget = CreateWidget<USKW_Menu>(GetWorld(), 
        MenuWidgetClass);

    if (!MenuWidget)
    {
        UE_LOG(LogTemp, Error, TEXT("ASK_MenuHUD:BeginPlay: MenuWidgetClass created error"));
        return;
    }

    if (MenuWidget)
    {
        MenuWidget->SetHUD(this);
        MenuWidget->AddToViewport();
        MenuWidget->Show();
    }

    SettingsWidget = CreateWidget<USKW_Settings>(GetWorld(), SettingsWidgetClass);
    if (!SettingsWidget)
    {
        UE_LOG(LogTemp, Error, TEXT("ASK_MenuHUD:BeginPlay: SettingsWidgetClass created error"));
        return;
    }

    if (SettingsWidget)
    {
        SettingsWidget->SetHUD(this);
        SettingsWidget->AddToViewport();
        SettingsWidget->SetVisibility(ESlateVisibility::Hidden);
    }
}

void ASK_MenuHUD::OpenSettings()
{
    if (!GetWorld()
        || !SettingsWidgetClass)
    {
        UE_LOG(LogTemp, Error, TEXT("ASK_MenuHUD:OpenSettings: World or SettingsWidgetClass is empty"));
        return;
    }

    if (!SettingsWidget)
    {
        SettingsWidget = CreateWidget<USKW_Settings>(GetWorld(), 
            SettingsWidgetClass);
        if (SettingsWidget)
        {
            SettingsWidget->AddToViewport();
            SettingsWidget->SetHUD(this);
        }
    }

    if (SettingsWidget)
    {
        UE_LOG(LogTemp, Warning, TEXT("ASK_MenuHUD:OpenSettings"));
        SettingsWidget->CloseMenu();
    }
}

void ASK_MenuHUD::OpenMenu()
{
    if (!GetWorld()
        || !MenuWidgetClass)
    {
        UE_LOG(LogTemp, Error, TEXT("ASK_MenuHUD:OpenMenu: World or MenuWidgetClass is empty"));
        return;
    }

    if (!MenuWidget)
    {
        MenuWidget = CreateWidget<USKW_Menu>(GetWorld(), MenuWidgetClass);
        if (MenuWidget)
        {
            MenuWidget->AddToViewport();
            MenuWidget->SetHUD(this);
        }
    }

    if (MenuWidget)
    {
        UE_LOG(LogTemp, Warning, TEXT("ASK_MenuHUD:OpenMenu"));
        MenuWidget->CloseSettings();
    }
}
