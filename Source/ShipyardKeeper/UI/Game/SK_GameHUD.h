// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SK_GameHUD.generated.h"

//Match
UENUM(BlueprintType)
enum class EWidgetOnScreen : uint8
{
	Game = 0,
	GlobalMap,
	Upgrade,
	BuyTile,
	SelectNextShip
};

class USKW_Base;
class USKW_Game;
class USKW_GlobalMap;
class USKW_BuyTile;
class USKW_SelectNextShip;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API ASK_GameHUD : public AHUD
{
	GENERATED_BODY()
	
	public:
	virtual void BeginPlay() override;

	void SetNextWidget(EWidgetOnScreen WidgetOnScreen);
	USKW_Base* GetCurrentWidget();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widgets)
	TSubclassOf<USKW_Game> GameWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widgets)
	TSubclassOf<USKW_GlobalMap> GlobalMapWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widgets)
	TSubclassOf<USKW_BuyTile> BuyTileClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widgets)
	TSubclassOf<USKW_SelectNextShip> SelectNextShipClass;

	USKW_Game* GetGameWidget() { return GameWidget; }
	private:
	USKW_Game* GameWidget;
	USKW_GlobalMap* GlobalMapWidget;
	USKW_BuyTile* BuyTileWidget;
	USKW_SelectNextShip* SelectNextShipWidget;

	UPROPERTY()
	TMap<EWidgetOnScreen, USKW_Base*> GameWidgets;

	UPROPERTY()
	USKW_Base* CurrentWidget = nullptr;
};
