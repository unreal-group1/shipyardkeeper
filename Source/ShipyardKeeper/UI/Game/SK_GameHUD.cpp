// Created by Pavlov P.A.

#include "SK_GameHUD.h"

#include "SKW_Base.h"
#include "SKW_Game.h"
#include "SKW_GlobalMap.h"
#include "SKW_BuyTile.h"
#include "SKW_SelectNextShip.h"
#include "SK_GameMode.h"

void ASK_GameHUD::BeginPlay()
{
    Super::BeginPlay();

    if (!GetWorld())
    {
        UE_LOG(LogTemp, Error, TEXT("ASK_GameHUD:BeginPlay: World is empty"));
        return;
    }

    if (GameWidgetClass)
    {
        GameWidget = CreateWidget<USKW_Game>(GetWorld(), GameWidgetClass);
        if(GameWidget)  GameWidgets.Add(EWidgetOnScreen::Game, GameWidget);

        if (GetWorld())
        {
            const auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
            if (GM)
            {
                GM->InitTaskComponent();
            }
        }
    }
    if (GlobalMapWidgetClass)
    {
        GameWidgets.Add(EWidgetOnScreen::GlobalMap,
            CreateWidget<USKW_Base>(GetWorld(), GlobalMapWidgetClass));
    }

    if (BuyTileClass)
    {
        GameWidgets.Add(EWidgetOnScreen::BuyTile,
            CreateWidget<USKW_BuyTile>(GetWorld(), BuyTileClass));
    }
    if (SelectNextShipClass)
    {
        GameWidgets.Add(EWidgetOnScreen::SelectNextShip,
            CreateWidget<USKW_SelectNextShip>(GetWorld(), SelectNextShipClass));
    }

    for (auto GameWidgetPair : GameWidgets)
    {
        const auto CurrentGameWidget = GameWidgetPair.Value;
        if (!CurrentGameWidget)
        {
            continue;
        }
        CurrentGameWidget->SetHUD(this);
        CurrentGameWidget->AddToViewport();
        if (GameWidgetPair.Key == EWidgetOnScreen::Game)
        {
            CurrentGameWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
        }
        else
        {
            CurrentGameWidget->SetVisibility(ESlateVisibility::Hidden);
        }
    }
}

void ASK_GameHUD::SetNextWidget(EWidgetOnScreen WidgetOnScreen)
{
    if (GameWidgets.Contains(WidgetOnScreen))
    {
        CurrentWidget = GameWidgets[WidgetOnScreen];
    }

    if (CurrentWidget)
    {
        CurrentWidget->Show();
    }
}

USKW_Base* ASK_GameHUD::GetCurrentWidget()
{
    return CurrentWidget;
}
