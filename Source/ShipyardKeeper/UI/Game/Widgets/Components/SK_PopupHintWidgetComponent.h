// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"

#include "SK_PopupHintWidgetComponent.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USK_PopupHintWidgetComponent : public UWidgetComponent
{
	GENERATED_BODY()
	
	public:
	USK_PopupHintWidgetComponent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Popup)
	class USKW_BasePopup* PopupHint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Popup)
	TSubclassOf<class USKW_BasePopup> PopupWidgetClass;

	virtual void InitWidget() override;
	virtual void SetWidgetClass(TSubclassOf<class USKW_NPSWorkProgressBar> NewWidgetClass);
	virtual void SetWidgetClass(TSubclassOf<class USKW_NPSWorkProgressBar> NewWidgetClass,
		UTexture2D* Image);
	virtual void SetWidgetClass(TSubclassOf<class USKW_PopupWithProgressbar> NewWidgetClass);
	virtual void SetWidgetClass(TSubclassOf<class USKW_PopupWithButton> NewWidgetClass);
	void InitPopupHint();

	UFUNCTION()
	void Show();
	UFUNCTION()
	void Hide();
};
