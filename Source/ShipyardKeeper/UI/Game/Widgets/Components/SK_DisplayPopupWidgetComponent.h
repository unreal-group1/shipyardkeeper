// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "SK_DisplayPopupWidgetComponent.generated.h"

class USKW_BaseDisplayImageAndText;
class UTexture2D;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USK_DisplayPopupWidgetComponent : public UWidgetComponent
{
	GENERATED_BODY()
	
	public:
	USK_DisplayPopupWidgetComponent();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Popup)
	TSubclassOf<USKW_BaseDisplayImageAndText> DisplayImageAndTextWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Popup)
	USKW_BaseDisplayImageAndText* BaseDisplayImageAndTextWidget;

	UFUNCTION()
	virtual void SetImageAndText(UTexture2D* Image,
		FString Text);
	UFUNCTION()
	virtual void InitWidget() override;
	UFUNCTION()
	void Show();
	UFUNCTION()
	void Hide();

protected:
	virtual void BeginPlay() override;
};
