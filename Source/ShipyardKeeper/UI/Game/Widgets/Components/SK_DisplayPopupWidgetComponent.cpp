// Created by Pavlov P.A.

#include "SK_DisplayPopupWidgetComponent.h"
#include "SKW_BasePopup.h"
#include "SKW_BaseDisplayImageAndText.h"

#include "Engine/Texture2D.h"

USK_DisplayPopupWidgetComponent::USK_DisplayPopupWidgetComponent()
{
	this->CastShadow = false;
}

void USK_DisplayPopupWidgetComponent::SetImageAndText(UTexture2D* Image,
	FString Text)
{
	if (DisplayImageAndTextWidgetClass)
	{
		USKW_BasePopup* NewWidget = CreateWidget<USKW_BaseDisplayImageAndText>(GetWorld(),
			DisplayImageAndTextWidgetClass);
		SetWidget(NewWidget);
	}

	auto CurrentWidgetObject = GetUserWidgetObject();
	BaseDisplayImageAndTextWidget = Cast<USKW_BaseDisplayImageAndText>(CurrentWidgetObject);
	if (BaseDisplayImageAndTextWidget)
	{
		BaseDisplayImageAndTextWidget->SetImageFromTexture(Image);
		BaseDisplayImageAndTextWidget->SetText(FText::FromString(Text));
	}
}

void USK_DisplayPopupWidgetComponent::InitWidget()
{
	Super::InitWidget();

	if (BaseDisplayImageAndTextWidget)
	{
		BaseDisplayImageAndTextWidget->SetVisibility(ESlateVisibility::Hidden);
	}

	//Space = EWidgetSpace::Screen;
	//DrawSize = FIntPoint(50, 50);

	bDrawAtDesiredSize = true;
}

void USK_DisplayPopupWidgetComponent::Show()
{
	if (BaseDisplayImageAndTextWidget)
	{
		BaseDisplayImageAndTextWidget->Show();
	}
}

void USK_DisplayPopupWidgetComponent::Hide()
{
	if (BaseDisplayImageAndTextWidget)
	{
		BaseDisplayImageAndTextWidget->Hide();
	}
}

void USK_DisplayPopupWidgetComponent::BeginPlay()
{
	Super::BeginPlay();
}
