// Created by Pavlov P.A.

#include "SK_PopupHintWidgetComponent.h"
#include "SK_PlayerController.h"
#include "SK_ClickableAndTouchableActor.h"

#include "SKW_BaseDisplayImageAndText.h"
#include "SKW_PopupWithProgressbar.h"
#include "SKW_PopupWithButton.h"
#include "SKW_NPSWorkProgressBar.h"

USK_PopupHintWidgetComponent::USK_PopupHintWidgetComponent()
{
	this->CastShadow = false;
}

void USK_PopupHintWidgetComponent::InitWidget()
{
	Super::InitWidget();

	InitPopupHint();
	if (PopupHint)
	{
		PopupHint->SetVisibility(ESlateVisibility::Hidden);
	}

	//Space = EWidgetSpace::Screen;
	//DrawSize = FIntPoint(50, 50);

	bDrawAtDesiredSize = true;
}

void USK_PopupHintWidgetComponent::SetWidgetClass(TSubclassOf<class USKW_NPSWorkProgressBar> NewWidgetClass)
{
	if (NewWidgetClass)
	{
		USKW_BasePopup* NewWidget = CreateWidget<USKW_NPSWorkProgressBar>(GetWorld(),
			NewWidgetClass);
		SetWidget(NewWidget);

		InitPopupHint();
	}
}

void USK_PopupHintWidgetComponent::SetWidgetClass(TSubclassOf<class USKW_NPSWorkProgressBar> NewWidgetClass,
	UTexture2D* Image)
{
	if (NewWidgetClass)
	{
		USKW_BasePopup* NewWidget = CreateWidget<USKW_NPSWorkProgressBar>(GetWorld(),
			NewWidgetClass);
		SetWidget(NewWidget);

		InitPopupHint();

		if (PopupHint)
		{
			PopupHint->SetImageFromTexture(Image);
		}
	}
}

void USK_PopupHintWidgetComponent::SetWidgetClass(TSubclassOf<class USKW_PopupWithProgressbar> NewWidgetClass)
{
	if (NewWidgetClass)
	{
		USKW_BasePopup* NewWidget = CreateWidget<USKW_PopupWithProgressbar>(GetWorld(), 
			NewWidgetClass);
		SetWidget(NewWidget);

		InitPopupHint();
	}
}

void USK_PopupHintWidgetComponent::SetWidgetClass(TSubclassOf<class USKW_PopupWithButton> NewWidgetClass)
{
	if (NewWidgetClass)
	{
		USKW_BasePopup* NewWidget = CreateWidget<USKW_PopupWithButton>(GetWorld(),
			NewWidgetClass);
		SetWidget(NewWidget);

		InitPopupHint();
	}
}

void USK_PopupHintWidgetComponent::InitPopupHint()
{
	auto CurrentWidgetObject = GetUserWidgetObject();
	PopupHint = Cast<USKW_BasePopup>(CurrentWidgetObject);
	if (PopupHint)
	{
		auto CurrentOwner = Cast<ASK_ClickableAndTouchableActor>(GetOwner());
		PopupHint->SetOwner(CurrentOwner);
	}
}

void USK_PopupHintWidgetComponent::Show()
{
	if (PopupHint)
	{
		PopupHint->Show();
	}

	SetCollisionEnabled(ECollisionEnabled::QueryAndProbe);
}

void USK_PopupHintWidgetComponent::Hide()
{
	if (PopupHint)
	{
		PopupHint->Hide();
	}

	SetCollisionEnabled(ECollisionEnabled::NoCollision);
}
