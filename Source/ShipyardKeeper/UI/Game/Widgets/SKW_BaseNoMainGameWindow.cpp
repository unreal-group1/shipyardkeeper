// Created by Pavlov P.A.

#include "SKW_BaseNoMainGameWindow.h"
#include "SKW_Button.h"
#include "SKW_BaseElementMap.h"
#include "SK_GameHUD.h"

#include "Components/Button.h"
#include "GameFramework/GameUserSettings.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/Image.h"
#include "Components/CanvasPanel.h"

void USKW_BaseNoMainGameWindow::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (GameButton
        && GameButton->CustomizeButton)
    {
        //GameButton->CustomizeButton->OnClicked.AddDynamic(this, &USKW_BaseNoMainGameWindow::Hide);
        GameButton->CustomizeButton->OnReleased.AddDynamic(this, &USKW_BaseNoMainGameWindow::Hide);
    }
}

void USKW_BaseNoMainGameWindow::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
    Super::OnAnimationFinished_Implementation(Animation);

    if (Animation == HideAnimation)
    {
        auto GameHUD = Cast<ASK_GameHUD>(GetHUD());
        if (GameHUD)
        {
            SetVisibility(ESlateVisibility::Hidden);

            GameHUD->SetNextWidget(EWidgetOnScreen::Game);
        }
    }
}

void USKW_BaseNoMainGameWindow::Hide()
{
    Super::Hide();
}
