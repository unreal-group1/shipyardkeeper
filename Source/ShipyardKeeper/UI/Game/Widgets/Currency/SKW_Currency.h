// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "SKW_BaseDisplayImageAndText.h"
#include "SKW_Currency.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_Currency : public USKW_BaseDisplayImageAndText
{
	GENERATED_BODY()
	
public:
	UPROPERTY(meta = (BindWidgetAnim), Transient)
	UWidgetAnimation* AddCurrencyAnimation;

	UPROPERTY(meta = (BindWidgetAnim), Transient)
	UWidgetAnimation* SubCurrencyAnimation;

	UFUNCTION()
	void AddCurency();
	UFUNCTION()
	void SubCurrency();
};
