// Created by Pavlov P.A.

#include "SKW_Currency.h"

void USKW_Currency::AddCurency()
{
    if (AddCurrencyAnimation)
    {
        PlayAnimation(AddCurrencyAnimation);
    }
}

void USKW_Currency::SubCurrency()
{
    if (SubCurrencyAnimation)
    {
        PlayAnimation(SubCurrencyAnimation);
    }
}
