// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "SKW_BaseElementMap.h"
#include "SKW_ProductionElementMap.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_ProductionElementMap : public USKW_BaseElementMap
{
	GENERATED_BODY()	

	public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ElementMap")
	FVector2D EndRoad = FVector2D::Zero();
};
