// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "SKW_BaseElementMap.h"
#include "SKW_RoadElementMap.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_RoadElementMap : public USKW_BaseElementMap
{
	GENERATED_BODY()
};
