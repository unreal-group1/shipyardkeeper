// Created by Pavlov P.A.

#include "SKW_GlobalMap.h"
#include "SKW_Button.h"
#include "SKW_BaseElementMap.h"
#include "SK_GameHUD.h"

#include "Components/CanvasPanel.h"

void USKW_GlobalMap::NativeOnInitialized()
{
    Super::NativeOnInitialized();
}

void USKW_GlobalMap::NativeConstruct()
{
    Super::NativeConstruct();

    if (Log)
    {
        Log->SetGlobalMap(this);
    }

    if (LogRoad)
    {
        LogRoad->SetGlobalMap(this);
        LogRoad->Hide();
    }

    if (Hemp)
    {
        Hemp->SetGlobalMap(this);
    }

    if (HempRoad)
    {
        HempRoad->SetGlobalMap(this);
        HempRoad->Hide();
    }

    if (Wool)
    {
        Wool->SetGlobalMap(this);
    }

    if (WoolRoad)
    {
        WoolRoad->SetGlobalMap(this);
        WoolRoad->Hide();
    }
}

void USKW_GlobalMap::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
    Super::OnAnimationFinished_Implementation(Animation);
}

void USKW_GlobalMap::Show()
{
    Super::Show();
}
