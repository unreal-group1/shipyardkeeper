// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"

#include "SKW_BaseNoMainGameWindow.h"
#include "SKW_GlobalMap.generated.h"

class USKW_BaseElementMap;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_GlobalMap : public USKW_BaseNoMainGameWindow
{
	GENERATED_BODY()
	
#pragma region Logs
#pragma region Log
public:
	UPROPERTY(meta = (BindWidget))
	USKW_BaseElementMap* Log;
#pragma endregion Log
#pragma region LogRoad
public:
	UPROPERTY(meta = (BindWidget))
	USKW_BaseElementMap* LogRoad;
#pragma endregion LogRoad
#pragma endregion Logs


#pragma region Hemps
#pragma region Hemp
public:
	UPROPERTY(meta = (BindWidget))
	USKW_BaseElementMap* Hemp;
#pragma endregion Hemp
#pragma region HempRoad
public:
	UPROPERTY(meta = (BindWidget))
	USKW_BaseElementMap* HempRoad;
#pragma endregion HempRoad
#pragma endregion Hemps


#pragma region Wools
#pragma region Wool
public:
	UPROPERTY(meta = (BindWidget))
	USKW_BaseElementMap* Wool;
#pragma endregion Wool
#pragma region WoolRoad
public:
	UPROPERTY(meta = (BindWidget))
	USKW_BaseElementMap* WoolRoad;
#pragma endregion WoolRoad
#pragma endregion Wools

	virtual void NativeOnInitialized() override;
	virtual void NativeConstruct() override;
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;
	virtual void Show() override;
};
