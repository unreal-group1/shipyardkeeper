// Created by Pavlov P.A.

#include "SKW_BaseElementMap.h"
#include "SKW_Button.h"
#include "SKW_GlobalMap.h"

#include "SK_GameMode.h"
#include "SK_PlatformGameInstance.h"
#include "SK_Types.h"
#include "SK_StorageActor.h"
#include "SK_StorageTile.h"

#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

void USKW_BaseElementMap::UpgradeElement()
{
	if (!CurrentStorageActor)
	{
		CurrentStorageActor = GetCurrentStorageActor();
	}

	if (CurrentStorageActor)
	{
		if (CurrentStorageActor->LevelUp())
		{
			CurrentLevel++;

			UpdateLevelImage();
			UpdateCostText();
		}
	}
}

void USKW_BaseElementMap::SetGlobalMap(USKW_GlobalMap* NewGlobalMap)
{
	if (NewGlobalMap)
	{
		GlobalMap = NewGlobalMap;
	}
}

void USKW_BaseElementMap::NativeConstruct()
{
	Super::NativeConstruct();

	SetVisibility(ESlateVisibility::SelfHitTestInvisible);

	UpdateCostText();
}

void USKW_BaseElementMap::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (ElementMapButton
		&& ElementMapButton->CustomizeButton)
	{
		ElementMapButton->CustomizeButton->OnClicked.AddDynamic(this,
			&USKW_BaseElementMap::UpgradeElement);
	}

	if (!PlatformGameInstance)
	{
		PlatformGameInstance = Cast<USK_PlatformGameInstance>(GetGameInstance());
	}
	if (!PlatformGameInstance) return;

	if (!StorageType)
	{
		StorageType = PlatformGameInstance->GetStorageByName(NameElement);
	}

	if (!CurrentStorageActor)
	{
		CurrentStorageActor = GetCurrentStorageActor();
	}

	if (!GlobalMapElement)
	{
		GlobalMapElement = PlatformGameInstance->GetGlobalMapElementByName(NameElement);
	}

	UpdateLevelImage();
}

void USKW_BaseElementMap::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
	Super::OnAnimationFinished_Implementation(Animation);

	if (Animation == ShowAnimation)
	{

	}

	if (Animation == HideAnimation)
	{
		SetVisibility(ESlateVisibility::Hidden);
	}
}

void USKW_BaseElementMap::SetImage(UImage* NewImage)
{
}

void USKW_BaseElementMap::SetImageFromTexture(UTexture2D* NewImage)
{
	if (Background
		&& NewImage)
	{
		Background->SetBrushFromTexture(NewImage);
	}
}

void USKW_BaseElementMap::Show()
{
	Super::Show();
}

void USKW_BaseElementMap::UpdateLevelImage()
{
	if (GlobalMapElement)
	{
		if (GlobalMapElement->LevelImages.Num() > 0
			&& CurrentLevel < uint8(GlobalMapElement->LevelImages.Num()))
		{
			auto HasImageObject = Cast<ISKI_HasImage>(this);
			if (HasImageObject)
			{
				HasImageObject->SetImageFromTexture(GlobalMapElement->LevelImages[CurrentLevel]);
			}
		}
	}
}

ASK_StorageActor* USKW_BaseElementMap::GetCurrentStorageActor()
{
	if (!CurrentStorageActor)
	{
		if (!PlatformGameInstance)
		{
			PlatformGameInstance = Cast<USK_PlatformGameInstance>(GetGameInstance());

			if (!PlatformGameInstance) return nullptr;
		}

		auto Storages = PlatformGameInstance->GetStorageTiles();
		for (auto Storage : Storages)
		{
			auto StorageTile = Cast<ASK_StorageTile>(Storage);
			if (StorageTile)
			{
				auto StorageActor = StorageTile->GetCurrentStorageActor();
				if (StorageActor)
				{
					FString StorageName = TCHAR_TO_UTF8(*NameElement.ToString());
					if (StorageActor->GetStorageName().ToString() == NameElement.ToString())
					{
						CurrentStorageActor = StorageActor;
						break;
					}
				}
				else
				{
					FStorageType* CurrentStorageType = PlatformGameInstance->GetStorageByName(NameElement);
					if (CurrentStorageType)
					{
						StorageTile->CreateStorageActor(*CurrentStorageType);
						StorageType = CurrentStorageType;
					}

					StorageActor = StorageTile->GetCurrentStorageActor();
					if (StorageActor)
					{
						FString StorageName = TCHAR_TO_UTF8(*NameElement.ToString());
						if (StorageActor->GetStorageName().ToString() == NameElement.ToString())
						{
							CurrentStorageActor = StorageActor;
							break;
						}
					}
				}
			}
		}
		if (CurrentStorageActor)
		{
			CurrentLevel = CurrentStorageActor->GetCurrentLevel();
		}
	}

	return CurrentStorageActor;
}

void USKW_BaseElementMap::UpdateCostText()
{
	USK_PlatformGameInstance* GI = nullptr;
	if (GetWorld())
	{
		GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	}
	if (!GI) return;

	if (ElementMapButton)
	{
		if (!GI->GetStoragePrice().IsValidIndex(CurrentLevel + 1))
		{
			ElementMapButton->SetText(TEXT("MAX"));
		}
		else
		{
			ElementMapButton->SetText(FString::Printf(TEXT("%i"), GI->GetStoragePrice()[CurrentLevel]));
		}
	}
}
