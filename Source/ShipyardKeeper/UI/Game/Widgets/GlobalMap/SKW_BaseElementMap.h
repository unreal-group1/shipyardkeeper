// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Components/Image.h"

#include "SK_Types.h"

#include "SKI_HasImage.h"

#include "SKW_Base.h"
#include "SKW_BaseElementMap.generated.h"

class USKW_Button;
class USKW_GlobalMap;
class ASK_StorageActor;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_BaseElementMap : public USKW_Base,
	public ISKI_HasImage
{
	GENERATED_BODY()
	
public:
	UPROPERTY(meta = (BindWidget))
	USKW_Button* ElementMapButton;
	UPROPERTY(meta = (BindWidget))
	UImage* Background;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ElementMap")
	FName NameElement;

	UFUNCTION()
	void UpgradeElement();

	UFUNCTION()
	void SetGlobalMap(USKW_GlobalMap* NewGlobalMap);

	UFUNCTION()
	virtual void NativeConstruct() override;
	UFUNCTION()
	virtual void NativeOnInitialized() override;
	UFUNCTION()
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;
	virtual void SetImage(UImage* NewImage) override;
	virtual void SetImageFromTexture(UTexture2D* NewImage) override;

	virtual void Show() override;
private:
	UPROPERTY()
	uint8 CurrentLevel = 0;
	FGlobalMapElement* GlobalMapElement;
	FStorageType* StorageType;
	UPROPERTY()
	ASK_StorageActor* CurrentStorageActor;
	UPROPERTY()
	USKW_GlobalMap* GlobalMap;
	UPROPERTY()
	class USK_PlatformGameInstance* PlatformGameInstance;
	void UpdateLevelImage();

	UFUNCTION()
	ASK_StorageActor* GetCurrentStorageActor();

	void UpdateCostText();
};
