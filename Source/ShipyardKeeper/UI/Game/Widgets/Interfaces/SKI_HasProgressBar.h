// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "SKI_HasProgressBar.generated.h"

UINTERFACE(MinimalAPI)
class USKI_HasProgressBar : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SHIPYARDKEEPER_API ISKI_HasProgressBar
{
	GENERATED_BODY()

public:
	UFUNCTION()
	virtual void SetPercent(float NewPercent);
};
