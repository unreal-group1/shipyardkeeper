// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "SKI_ActorClick.generated.h"

UINTERFACE(MinimalAPI)
class USKI_ActorClick : public UInterface
{
	GENERATED_BODY()
};

/**
 * ��������� ������� �� ������
 */
class SHIPYARDKEEPER_API ISKI_ActorClick
{
	GENERATED_BODY()

	public:
	/**
	* ��������� � ������� AActor OnClick
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Clicked(UPrimitiveComponent* TouchedActor, FKey ButtonPressed);
	/**
	* ��������� � ������� AActor OnReleased
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Released(UPrimitiveComponent* TouchedActor, FKey ButtonPressed);
	/**
	* ��������� � ������� AActor OnInputTouchBegin
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void InputTouchBegin(ETouchIndex::Type FingerIndex,
		UPrimitiveComponent* TouchedComponent);
	/**
	* ��������� � ������� AActor OnInputTouchEnd
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void InputTouchEnd(ETouchIndex::Type FingerIndex,
		UPrimitiveComponent* TouchedComponent);
};
