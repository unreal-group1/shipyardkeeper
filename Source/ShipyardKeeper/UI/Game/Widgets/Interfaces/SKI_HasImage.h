// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "SKI_HasImage.generated.h"

class UImage;

UINTERFACE(MinimalAPI)
class USKI_HasImage : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SHIPYARDKEEPER_API ISKI_HasImage
{
	GENERATED_BODY()

public:
	UFUNCTION()
	virtual void SetImage(UImage* NewImage);
	UFUNCTION()
	virtual void SetImageFromTexture(UTexture2D* NewImage);
};
