// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "SKI_HasText.generated.h"

UINTERFACE(MinimalAPI)
class USKI_HasText : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SHIPYARDKEEPER_API ISKI_HasText
{
	GENERATED_BODY()

public:
	UFUNCTION()
	virtual void SetText(FText NewText);
};
