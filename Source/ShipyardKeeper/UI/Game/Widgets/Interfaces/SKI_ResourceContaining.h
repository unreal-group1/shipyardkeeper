// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "SKI_ResourceContaining.generated.h"

struct FResourceType;

UINTERFACE(MinimalAPI)
class USKI_ResourceContaining : public UInterface
{
	GENERATED_BODY()
};

/**
 *
 */
class SHIPYARDKEEPER_API ISKI_ResourceContaining
{
	GENERATED_BODY()

public:
	UFUNCTION()
	virtual void SetResourceQuantity(double NewQuantity);
	UFUNCTION()
	virtual void SetResourceType(FResourceType NewResourceType);
	UFUNCTION()
	virtual void UpdateResourceQuantity(FName NewResourceName, double NewQuantity);
};
