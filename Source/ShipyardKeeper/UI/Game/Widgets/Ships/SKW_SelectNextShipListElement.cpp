// Created by Pavlov P.A.

#include "SKW_SelectNextShipListElement.h"
#include "SKW_BaseNoMainGameWindow.h"

#include "SK_GameMode.h"
#include "SK_ShipBuilder.h"
#include "SK_PlatformGameInstance.h"

void USKW_SelectNextShipListElement::SetShipName(FText NewShipName)
{
    auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
    if (GM)
    {
        auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
        if (GI)
        {
            ShipName = GI->GetShipsBuyName(FName(NewShipName.ToString()))->ShowingData.RowName;
        }
    }
}

FText USKW_SelectNextShipListElement::GetShipName()
{
    return ShipName;
}

void USKW_SelectNextShipListElement::Do()
{
    auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
    if (GM)
    {
        if (!ShipName.IsEmpty())
        {
            GM->SetCurrentProject(FName(ShipName.ToString()));
            ParentWidget->Hide();
        }
    }
}

void USKW_SelectNextShipListElement::NativeConstruct()
{
	Super::NativeConstruct();
}
