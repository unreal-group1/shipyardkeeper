// Created by Pavlov P.A.

#include "SKW_SelectNextShip.h"
#include "SKW_SelectNextShipListElement.h"
#include "SKW_NeededResourceForShip.h"
#include "SKW_CurrentShipListElement.h"
#include "SKW_Button.h"

#include "SKI_HasImage.h"
#include "SKI_HasText.h"

#include "SK_ShipBuilder.h"
#include "SK_GameMode.h"
#include "SK_PlatformGameInstance.h"
#include "SK_TileActorBase.h"

void USKW_SelectNextShip::NativeOnInitialized()
{
	Super::NativeOnInitialized();
}

void USKW_SelectNextShip::NativePreConstruct()
{
	Super::NativePreConstruct();
}

void USKW_SelectNextShip::NativeConstruct()
{
	Super::NativeConstruct();

	CreateList();
}

void USKW_SelectNextShip::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
	Super::OnAnimationFinished_Implementation(Animation);
}

void USKW_SelectNextShip::CreateList()
{
	Super::CreateList();

	const auto& World = GetWorld();
	if (!World) return;

	auto GI = Cast<USK_PlatformGameInstance>(GetGameInstance());
	if (!GI) return;

	const auto GM = World->GetAuthGameMode<ASK_GameMode>();
	if (!GM) return;

	ShipBuilder = GM->GetShipBuilder();
	if (!ShipBuilder) return;

#pragma region Ships
#pragma region CurrentShip
	CurrentShipElement = CreateListElement(CurrentShipListElementClass);
	ShipBuilder->OnCurrentShipProjectChange.AddDynamic(this, &USKW_SelectNextShip::UpdateCurrentShipElement);
#pragma endregion CurrentShip

#pragma region NextShip
	NextShipElement = CreateListElement(NextShipElementClass);
	if (NextShipElement->CurrentBuildProgress)
	{
		NextShipElement->CurrentBuildProgress->SetVisibility(ESlateVisibility::Hidden);
	}
	GM->OnNextShipProjectChange.AddDynamic(this, &USKW_SelectNextShip::UpdateNextShipElement);
#pragma endregion NextShip

#pragma region SwitchShips
	auto ShipTypes = GI->GetShipTypesWithOriginalNames();
	for (auto ShipType : ShipTypes)
	{
		if (SelectNextShipListElementClass)
		{
			auto ListElement = CreateWidget<USKW_SelectNextShipListElement>(World, SelectNextShipListElementClass);
			if (ListElement)
			{
				NextShipList->AddChildToVerticalBox(ListElement);
				SelectNextShipListElements.Add(ListElement);

				auto HasImageObject = Cast<ISKI_HasImage>(ListElement);
				if (HasImageObject)
				{
					HasImageObject->SetImageFromTexture(ShipType->ShowingData.Image);
				}

				auto HasTextObject = Cast<ISKI_HasText>(ListElement);
				if (HasTextObject)
				{
					HasTextObject->SetText(ShipType->ShowingData.PrintName);
				}
				ListElement->SetShipName(ShipType->ShowingData.RowName);

				ListElement->SetParentWidget(this);

				CreateNeededResourceInElement(ShipType->NeededResources,
					ListElement);
			}
		}
	}

	GM->OnBuildNewMachine.AddDynamic(this, &USKW_SelectNextShip::CheckEnabledButtonShipElement);
	GM->OnSlipwayUpgrade.AddDynamic(this, &USKW_SelectNextShip::UpdateSlipwayLevel);

	CheckEnabledButtonShipElement();
#pragma endregion SwitchShips
#pragma endregion Ships

	CheckEnabledNextShipElement();
}

USKW_CurrentShipListElement* USKW_SelectNextShip::CreateListElement(TSubclassOf<class USKW_CurrentShipListElement> NewShipListElementClass)
{
	const auto& World = GetWorld();
	if (!World) return nullptr;

	auto GI = Cast<USK_PlatformGameInstance>(GetGameInstance());
	if (!GI) return nullptr;

	auto CurrentShip = ShipBuilder->GetCurrentShipName();
	if (NewShipListElementClass)
	{
		auto CurrentShipListElement = CreateWidget<USKW_CurrentShipListElement>(World, NewShipListElementClass);
		if (CurrentShipListElement)
		{
			NextShipList->AddChildToVerticalBox(CurrentShipListElement);
			SelectNextShipListElements.Add(CurrentShipListElement);

			FShipTypes* CurrentShipType = GI->GetShipsBuyName(CurrentShip);
			if (CurrentShipType)
			{
				auto HasImageObject = Cast<ISKI_HasImage>(CurrentShipListElement);
				if (HasImageObject)
				{
					HasImageObject->SetImageFromTexture(CurrentShipType->ShowingData.Image);
				}

				auto HasTextObject = Cast<ISKI_HasText>(CurrentShipListElement);
				if (HasTextObject)
				{
					HasTextObject->SetText(CurrentShipType->ShowingData.PrintName);
				}
				CreateNeededResourceInElement(CurrentShipType->NeededResources,
					CurrentShipListElement);

				CurrentShipListElement->SetShipName(CurrentShipType->ShowingData.RowName);
			}

			CurrentShipListElement->SetParentWidget(this);

			CheckEnabledShipElement(CurrentShipListElement);
		}
		return CurrentShipListElement;
	}
	return nullptr;
}

void USKW_SelectNextShip::CreateListElement()
{
	
}

void USKW_SelectNextShip::CreateNeededResourceInElement(TArray<FResource> InputNeededResources,
	USKW_SelectNextShipListElement* ShipListElement)
{
	if (!ShipListElement) return;
	if (InputNeededResources.Num() <= 0) return;

	const auto& World = GetWorld();
	if (!World) return;

	auto GI = Cast<USK_PlatformGameInstance>(GetGameInstance());
	if (!GI) return;

	bool IsFirstElement = true;
	USKW_NeededResourceForShip* NeededResourceViewElement;
	for (auto NeededResource : InputNeededResources)
	{
		NeededResourceViewElement = CreateWidget<USKW_NeededResourceForShip>(World, ShipListElement->NeededResourceViewClass);
		if (ShipListElement->NeededResourcesBox)
		{
			ShipListElement->NeededResourcesBox->AddChildToHorizontalBox(NeededResourceViewElement);
		}
		ShipListElement->NeededResourceElements.Add(NeededResourceViewElement);

		if (!NeededResource.ResourcesRowName.IsNone())
		{
			auto ResourceType = GI->GetResourceTypeByName(NeededResource.ResourcesRowName);
			if (ResourceType)
			{
				auto HasImageObject = Cast<ISKI_HasImage>(NeededResourceViewElement);
				if (HasImageObject)
				{
					HasImageObject->SetImageFromTexture(ResourceType->ShowingData.Image);
				}
			}
		}

		UpdateDistanceBetweenResourceElements(NeededResourceViewElement,
			IsFirstElement,
			DistanceBetweenResourceElements);

		IsFirstElement = false;
		
		FText NeddedResourceText = FText::FromString(FString::Printf(TEXT("%.0f"), NeededResource.ResourceQuantity));
		auto HasTextObject = Cast<ISKI_HasText>(NeededResourceViewElement);
		if (HasTextObject)
		{
			HasTextObject->SetText(NeddedResourceText);
		}

		NeededResourceViewElement->Show();
	}
}

void USKW_SelectNextShip::UpdateSlipwayLevel(TEnumAsByte<ESlipwayLevel> NewLevel)
{
	CheckEnabledButtonShipElement();
}

void USKW_SelectNextShip::UpdateCurrentShipElement(FName NewShipProject)
{
	if (CurrentShipElement)
	{
		UpdateShipElement(CurrentShipElement,
			NewShipProject);

		CheckEnabledNextShipElement();
	}
}

void USKW_SelectNextShip::UpdateNextShipElement(FName NewShipProject)
{
	if (NextShipElement)
	{
		UpdateShipElement(NextShipElement,
			NewShipProject);
	}

	for (auto SelectNextShipListElement : SelectNextShipListElements)
	{
		CheckEnabledShipElement(SelectNextShipListElement);
	}

	if (NextShipElement)
	{
		CheckEnabledNextShipElement();
	}
}

void USKW_SelectNextShip::UpdateShipElement(USKW_CurrentShipListElement* ShipElement,
	FName NewShipProject)
{
	auto GI = Cast<USK_PlatformGameInstance>(GetGameInstance());
	if (!GI) return;

	FShipTypes* CurrentShipType = GI->GetShipsBuyName(NewShipProject);
	if (CurrentShipType)
	{
		auto HasImageObject = Cast<ISKI_HasImage>(ShipElement);
		if (HasImageObject)
		{
			HasImageObject->SetImageFromTexture(CurrentShipType->ShowingData.Image);
		}

		auto HasTextObject = Cast<ISKI_HasText>(ShipElement);
		if (HasTextObject)
		{
			HasTextObject->SetText(CurrentShipType->ShowingData.PrintName);
		}
		ShipElement->SetShipName(CurrentShipType->ShowingData.RowName);

		if (ShipElement)
		{
			if (ShipElement->NeededResourcesBox)
			{
				ShipElement->NeededResourcesBox->ClearChildren();
			}
			CreateNeededResourceInElement(CurrentShipType->NeededResources,
				ShipElement);
		}
	}
}

void USKW_SelectNextShip::CheckEnabledNextShipElement()
{
	if (NextShipElement
		&& CurrentShipElement)
	{
		if (CurrentShipElement->GetShipName().ToString() 
			== NextShipElement->GetShipName().ToString())
		{
			NextShipElement->SetVisibility(ESlateVisibility::Collapsed);
		}
		else
		{
			NextShipElement->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
		}
	}
}

void USKW_SelectNextShip::CheckEnabledShipElement(USKW_SelectNextShipListElement* CurrentNextShipElement)
{
	if (NextShipElement
		&& CurrentNextShipElement
		&& CurrentNextShipElement != CurrentShipElement)
	{
		if (NextShipElement->GetShipName().ToString()
			== CurrentNextShipElement->GetShipName().ToString())
		{
			CurrentNextShipElement->SetVisibility(ESlateVisibility::Collapsed);
		}
		else
		{
			CurrentNextShipElement->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
		}
	}
}

void USKW_SelectNextShip::CheckEnabledButtonShipElement()
{
	const auto& World = GetWorld();
	if (!World) return;

	auto GI = Cast<USK_PlatformGameInstance>(GetGameInstance());
	if (!GI) return;

	const auto GM = World->GetAuthGameMode<ASK_GameMode>();
	if (!GM) return;

	ShipBuilder = GM->GetShipBuilder();
	if (!ShipBuilder) return;

	auto CurrentSlipwayLevel = GM->GetSlipwayLevel();

	for (auto SelectNextShipListElement : SelectNextShipListElements)
	{
		FShipTypes* CurrentShipType = GI->GetShipsBuyName(FName(SelectNextShipListElement->GetShipName().ToString()));
		if (CurrentShipType)
		{
			if (SelectNextShipListElement->DoButton)
			{
				//������� ������� ������������
				if (CurrentSlipwayLevel < CurrentShipType->RequiredSlipwayLevel)
				{
					SelectNextShipListElement->DoButton->SetIsEnabled(false);
				}
				else
				{
					SelectNextShipListElement->DoButton->SetIsEnabled(true);
				}
				//������ ��� ������� ���� ������� �� ��������
				if (SelectNextShipListElement->DoButton->GetIsEnabled())
				{
					bool IsFind = false;
					for (auto NeededResource : CurrentShipType->NeededResources)
					{
						IsFind = false;
						auto MachineTypes = GM->GetMachineActors();
						for (auto CurrentMachineType : MachineTypes)
						{
							if (CurrentMachineType.OutResourceRowName.IsEqual(NeededResource.ResourcesRowName))
							{
								IsFind = true;
								break;
							}
						}
						if (!IsFind)
						{
							break;
						}
					}

					if (IsFind)
					{
						SelectNextShipListElement->DoButton->SetIsEnabled(true);
					}
					else
					{
						SelectNextShipListElement->DoButton->SetIsEnabled(false);
					}
				}
			}

			CheckEnabledShipElement(SelectNextShipListElement);
		}
	}
}

void USKW_SelectNextShip::UpdateDistanceBetweenResourceElements(USKW_NeededResourceForShip* CurrentNeededResourceForShip,
	bool IsFirstElement, 
	float NewDistanceBetweenResourceElements)
{
	auto CurrentPadding = CurrentNeededResourceForShip->GetPadding();
	if (IsFirstElement)
	{
		CurrentPadding.Left = 0;
	}
	else
	{
		CurrentPadding.Left = DistanceBetweenResourceElements;
	}
	CurrentNeededResourceForShip->SetPadding(CurrentPadding);
}

void USKW_SelectNextShip::UpdateSwitchShipElement(TEnumAsByte<ESlipwayLevel> NewLevel)
{
	CheckEnabledButtonShipElement();
}