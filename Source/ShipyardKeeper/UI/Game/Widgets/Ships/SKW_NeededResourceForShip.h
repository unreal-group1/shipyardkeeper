// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"

#include "SK_Types.h"

#include "SKI_ResourceContaining.h"

#include "SKW_PopupWithImageAndText.h"
#include "SKW_NeededResourceForShip.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_NeededResourceForShip : public USKW_PopupWithImageAndText,
	public ISKI_ResourceContaining
{
	GENERATED_BODY()

public:
	USKW_NeededResourceForShip();

	virtual void NativeConstruct() override;

	virtual void SetResourceQuantity(double NewQuantity) override;
	virtual void SetResourceType(FResourceType NewResourceType) override;
	virtual void UpdateResourceQuantity(FName ResourceName, double NewQuantity) override;

private:
	FResourceType ResourceType;
	double Quantity = 0.0f;
	UPROPERTY()
	TObjectPtr<class USK_PlatformGameInstance> PlatformGameInstance;
	bool IsAlreadyBeenShown = false;
};
