// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Components/VerticalBox.h"
#include "Components/HorizontalBox.h"

#include "SK_Types.h"
#include "SKW_WithListAndCurrentResources.h"
#include "SKW_SelectNextShip.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_SelectNextShip : public USKW_WithListAndCurrentResources
{
	GENERATED_BODY()
#pragma region Ships
#pragma region CurrentShip
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<class USKW_CurrentShipListElement> CurrentShipListElementClass;
private:
	class USKW_CurrentShipListElement* CurrentShipElement;
	UPROPERTY()
	TObjectPtr<class USK_ShipBuilder> ShipBuilder;
#pragma endregion CurrentShip
#pragma region NextShip
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<class USKW_CurrentShipListElement> NextShipElementClass;
private:
	class USKW_CurrentShipListElement* NextShipElement;
#pragma endregion NextShip
#pragma region SwitchShips
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<class USKW_SelectNextShipListElement> SelectNextShipListElementClass;

	UPROPERTY(meta = (BindWidget))
	UVerticalBox* NextShipList;
private:
	TArray<class USKW_SelectNextShipListElement*> SelectNextShipListElements;
#pragma endregion SwitchShips
#pragma endregion Ships
public:
	virtual void NativeOnInitialized() override;
	virtual void NativePreConstruct() override;
	virtual void NativeConstruct() override;
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;
	virtual void CreateList() override;
	USKW_CurrentShipListElement* CreateListElement(TSubclassOf<class USKW_CurrentShipListElement> NewShipListElementClass);
	void CreateListElement();
	void CreateNeededResourceInElement(TArray<struct FResource> InputNeededResources,
		USKW_SelectNextShipListElement* ShipListElement);
	UFUNCTION()
	void UpdateSlipwayLevel(TEnumAsByte<ESlipwayLevel> NewLevel);

	UFUNCTION()
	void UpdateCurrentShipElement(FName NewShipProject);
	UFUNCTION()
	void UpdateNextShipElement(FName NewShipProject);
	UFUNCTION()
	void UpdateShipElement(class USKW_CurrentShipListElement* ShipElement,
		FName NewShipProject);
	UFUNCTION()
	void CheckEnabledNextShipElement();
	UFUNCTION()
	void CheckEnabledShipElement(USKW_SelectNextShipListElement* CurrentNextShipElement);

	UFUNCTION()
	void UpdateSwitchShipElement(TEnumAsByte<ESlipwayLevel> NewLevel);
	UFUNCTION()
	void CheckEnabledButtonShipElement();
#pragma region DistanceBetweenElements
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	float DistanceBetweenResourceElements = -35.0f;

	UFUNCTION()
	void UpdateDistanceBetweenResourceElements(USKW_NeededResourceForShip* CurrentNeededResourceForShip,
		bool IsFirstElement, 
		float NewDistanceBetweenResourceElements);
#pragma endregion DistanceBetweenElements
};
