// Created by Pavlov P.A.

#include "SKW_CurrentShipListElement.h"
#include "SKW_BaseNoMainGameWindow.h"

#include "SK_GameMode.h"
#include "SK_ShipBuilder.h"

void USKW_CurrentShipListElement::NativeConstruct()
{
    Super::NativeConstruct();

    if (GetWorld())
    {
        auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
        if (GM)
        {
            ShipBuilder = GM->GetShipBuilder();
            if (ShipBuilder)
            {
                ShipBuilder->OnEnduranceChange.AddDynamic(this,
                    &USKW_CurrentShipListElement::UpdateEndurancePercent);
            }
        }
    }
    CurrentBuildProgress->SetPercent(0);

    if (GlowAnimation)
    {
        PlayAnimation(GlowAnimation, 0, 0);
    }
}

void USKW_CurrentShipListElement::Do()
{
    if (ShipBuilder)
    {
        ShipBuilder->BuildCancel();
        ParentWidget->Hide();
    }
}

void USKW_CurrentShipListElement::UpdateEndurancePercent(double Current, double Max)
{
    if (Current != 0
        && Max != 0)
    {
        Percent = Current / Max;

        if (Percent >= MinPercentThreshold)
        {
            Show();
        }

        CurrentBuildProgress->SetPercent(Percent);
    }

    if (Percent >= MaxPercentThreshold)
    {
        CurrentBuildProgress->SetPercent(0);
    }
}
