// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Components/HorizontalBox.h"

#include "SKW_ListElement.h"
#include "SKW_SelectNextShipListElement.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_SelectNextShipListElement : public USKW_ListElement
{
	GENERATED_BODY()
public:
	UPROPERTY()
	TArray<class USKW_PopupWithImageAndText*> NeededResourceElements;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<class USKW_PopupWithImageAndText> NeededResourceViewClass;

	UPROPERTY(meta = (BindWidget))
	UHorizontalBox* NeededResourcesBox;

	void SetShipName(FText NewShipName);
	FText GetShipName();

	virtual void Do() override;
	virtual void NativeConstruct() override;

private:
	UPROPERTY()
	TObjectPtr<class USK_ShipBuilder> ShipBuilder;
	UPROPERTY()
	FText ShipName;
};
