// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Components/ProgressBar.h"

#include "SKW_Text.h"
#include "SKW_SelectNextShipListElement.h"
#include "SKW_CurrentShipListElement.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_CurrentShipListElement : public USKW_SelectNextShipListElement
{
	GENERATED_BODY()
	
public:
	UPROPERTY(meta = (BindWidget))
	UProgressBar* CurrentBuildProgress;

	UPROPERTY(meta = (BindWidgetAnim), Transient)
	UWidgetAnimation* GlowAnimation;

	virtual void NativeConstruct() override;
	virtual void Do() override;

	UFUNCTION()
	void UpdateEndurancePercent(double Current, double Max);

private:
	double Percent;
	const double MinPercentThreshold = 0.001f;
	const double MaxPercentThreshold = 0.99f;
	TObjectPtr<class USK_ShipBuilder> ShipBuilder;
};
