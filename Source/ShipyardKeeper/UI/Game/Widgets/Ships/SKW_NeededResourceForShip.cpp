// Created by Pavlov P.A.

#include "SKW_NeededResourceForShip.h"
#include "SKW_BasePopup.h"

#include "SK_PlatformGameInstance.h"
#include "SK_ShipBuilder.h"

#include "SKI_ResourceContaining.h"
#include "SKI_HasImage.h"
#include "SKI_HasText.h"

#include "Components/Image.h"

USKW_NeededResourceForShip::USKW_NeededResourceForShip()
{
}

void USKW_NeededResourceForShip::NativeConstruct()
{
    if (!PlatformGameInstance)
    {
        if (GetGameInstance())
        {
            PlatformGameInstance = Cast<USK_PlatformGameInstance>(GetGameInstance());
        }
    }

    if (PlatformGameInstance)
    {
        PlatformGameInstance->OnResourceCountChange.AddDynamic(this, &USKW_NeededResourceForShip::UpdateResourceQuantity);
    }

    if (Quantity <= 0)
    {
        Hide();
    }
    else
    {
        Show();
    }
}

void USKW_NeededResourceForShip::SetResourceQuantity(double NewQuantity)
{
    Quantity = NewQuantity;

    SetText(FText::FromString(FString::Printf(TEXT("%.0f"), NewQuantity)));

    if (NewQuantity <= 0)
    {
        if (!IsAlreadyBeenShown)
        {
            Hide();
            IsAlreadyBeenShown = true;
        }
    }
    else
    if (GetVisibility() != ESlateVisibility::SelfHitTestInvisible)
    {
        Show();
        IsAlreadyBeenShown = true;
    }
}

void USKW_NeededResourceForShip::SetResourceType(FResourceType NewResourceType)
{
    ResourceType = NewResourceType;

    UImage* NewImage = NewObject<UImage>(UImage::StaticClass());
    if (NewImage)
    {
        NewImage->SetBrushFromTexture(ResourceType.ShowingData.Image, true);

        SetImage(NewImage);
    }
}

void USKW_NeededResourceForShip::UpdateResourceQuantity(FName ResourceName, 
    double NewQuantity)
{
    if (ResourceType.ShowingData.RowName.EqualTo(FText::FromName(ResourceName)))
    {
        SetResourceQuantity(NewQuantity);
        SetText(FText::FromString(FString::Printf(TEXT("%.0f"), NewQuantity)));
    }
}
