// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"

#include "SKI_HasText.h"
#include "SKI_HasImage.h"

#include "SKW_Base.h"
#include "SKW_ListElement.generated.h"

class UImage;
class USKW_Text;
class USKW_Button;
class USKW_BaseNoMainGameWindow;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_ListElement : public USKW_Base,
	public ISKI_HasImage,
	public ISKI_HasText
{
	GENERATED_BODY()
	
public:
	UPROPERTY(meta = (BindWidget))
	UImage* Icon;

	UPROPERTY(meta = (BindWidget))
	USKW_Text* Description;

	UPROPERTY(meta = (BindWidget))
	USKW_Button* DoButton;

	virtual void NativeConstruct() override;

	virtual void SetImage(UImage* NewImage) override;
	virtual void SetImageFromTexture(UTexture2D* NewImage) override;
	virtual void SetText(FText NewText) override;
	UFUNCTION()
	virtual void Do();

	void SetParentWidget(USKW_BaseNoMainGameWindow* NewParentWidget);

protected:
	UPROPERTY()
	USKW_BaseNoMainGameWindow* ParentWidget;
};
