// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "SKW_BaseNoMainGameWindow.h"
#include "SKW_WindowWithList.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_WindowWithList : public USKW_BaseNoMainGameWindow
{
	GENERATED_BODY()
	
	public:
	virtual void NativeOnInitialized() override;
	virtual void NativePreConstruct() override;
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;
	virtual void CreateList();
};
