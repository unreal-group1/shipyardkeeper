// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Components/VerticalBox.h"
#include "Components/HorizontalBox.h"

#include "SKW_WindowWithList.h"
#include "SKW_WithListAndCurrentResources.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_WithListAndCurrentResources : public USKW_WindowWithList
{
	GENERATED_BODY()
#pragma region CurrentResources
public:
	UPROPERTY()
	TArray<class USKW_NeededResourceForShip*> CurrentResourcesElements;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<class USKW_NeededResourceForShip> CurrentResourcesViewClass;

	UPROPERTY(meta = (BindWidget))
	UHorizontalBox* CurrentRawResourcesBox;
	UPROPERTY(meta = (BindWidget))
	UHorizontalBox* CurrentResourcesBox;
#pragma endregion CurrentResources
public:
	virtual void NativeOnInitialized() override;
	virtual void NativePreConstruct() override;
	virtual void NativeConstruct() override;
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;
	virtual void CreateList() override;
};
