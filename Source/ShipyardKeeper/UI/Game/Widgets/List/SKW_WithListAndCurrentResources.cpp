// Created by Pavlov P.A.

#include "SKW_WithListAndCurrentResources.h"
#include "SKW_NeededResourceForShip.h"

#include "SK_ShipBuilder.h"
#include "SK_GameMode.h"
#include "SK_PlatformGameInstance.h"

void USKW_WithListAndCurrentResources::NativeOnInitialized()
{
	Super::NativeOnInitialized();
}

void USKW_WithListAndCurrentResources::NativePreConstruct()
{
	Super::NativePreConstruct();
}

void USKW_WithListAndCurrentResources::NativeConstruct()
{
	Super::NativeConstruct();
}

void USKW_WithListAndCurrentResources::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
	Super::OnAnimationFinished_Implementation(Animation);
}

void USKW_WithListAndCurrentResources::CreateList()
{
	const auto& World = GetWorld();
	if (!World) return;

	auto GI = Cast<USK_PlatformGameInstance>(GetGameInstance());
	if (!GI) return;

	const auto GM = World->GetAuthGameMode<ASK_GameMode>();
	if (!GM) return;

	auto ShipBuilder = GM->GetShipBuilder();
	if (!ShipBuilder) return;
#pragma region CurrentResources
	auto ResourceTypes = GI->GetResourceTypesWithOriginalNames();
	for (auto ResourceType : ResourceTypes)
	{
		USKW_NeededResourceForShip* CurrentResource;
		auto Result = GI->RawResourceNames.FindByPredicate(
			[&](const FText Item)
			{
				return Item.EqualTo(ResourceType->ShowingData.RowName);
			});
		if (Result)
		{
			CurrentResource = CreateWidget<USKW_NeededResourceForShip>(World, CurrentResourcesViewClass);
			if (CurrentRawResourcesBox
				&& CurrentResource)
			{
				CurrentRawResourcesBox->AddChildToHorizontalBox(CurrentResource);
			}
		}
		else
		{
			CurrentResource = CreateWidget<USKW_NeededResourceForShip>(World, CurrentResourcesViewClass);
			if (CurrentResourcesBox
				&& CurrentResource)
			{
				CurrentResourcesBox->AddChildToHorizontalBox(CurrentResource);
			}
		}

		if (CurrentResource)
		{
			//CurrentResourcesBox->AddChildToHorizontalBox(CurrentResource);
			CurrentResourcesElements.Add(CurrentResource);

			double ResourceQuantity = 0;
			if (GI->GetResourceQuantity(*ResourceType->ShowingData.RowName.ToString()) >= 0)
			{
				ResourceQuantity = GI->GetResourceQuantity(*ResourceType->ShowingData.RowName.ToString());
			}
			else
			{
				ResourceQuantity = 0;
			}
			CurrentResource->SetResourceQuantity(ResourceQuantity);
			CurrentResource->SetResourceType(*ResourceType);
		}
	}
#pragma endregion CurrentResources
}
