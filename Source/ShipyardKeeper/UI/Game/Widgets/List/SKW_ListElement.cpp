// Created by Pavlov P.A.

#include "SKW_ListElement.h"
#include "SKW_Button.h"
#include "SKW_Text.h"
#include "SKW_BaseNoMainGameWindow.h"

#include "Components/Button.h"
#include "Styling/SlateTypes.h"
#include "Components/Image.h"

void USKW_ListElement::NativeConstruct()
{
	if (DoButton
		&& DoButton->CustomizeButton)
	{
		DoButton->CustomizeButton->OnClicked.AddDynamic(this,
			&USKW_ListElement::Do);
	}
}

void USKW_ListElement::SetImage(UImage* NewImage)
{
	if (Icon
		&& NewImage)
	{
		Icon->SetBrush(NewImage->GetBrush());
	}
}

void USKW_ListElement::SetImageFromTexture(UTexture2D* NewImage)
{
	if (Icon
		&& NewImage)
	{
		Icon->SetBrushFromTexture(NewImage);
	}
}

void USKW_ListElement::SetText(FText NewText)
{
	if (Description)
	{
		Description->SetText(NewText.ToString());
	}
}

void USKW_ListElement::Do()
{
}

void USKW_ListElement::SetParentWidget(USKW_BaseNoMainGameWindow* NewParentWidget)
{
	if (NewParentWidget)
	{
		ParentWidget = NewParentWidget;
	}
}