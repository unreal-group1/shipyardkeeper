// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Game/Widgets/MiniGames/SK_MiniGame.h"
#include "SK_PlatformGameInstance.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "SK_CharacterBase.h"

void USK_MiniGame::Init(const FName& ResourceName)
{

}

void USK_MiniGame::ShowWidget()
{
	SetVisibility(ESlateVisibility::Visible);
	bIsMachine = true;
	ResourceImage->SetBrushFromTexture(MachineMiniGameIn);
}

void USK_MiniGame::HideWidget()
{
	SetVisibility(ESlateVisibility::Collapsed);
}

void USK_MiniGame::StartWork()
{
	const auto Player = Cast<ASK_CharacterBase>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if (!Player) return;

	if (InGame)
	{
		InGame = false;
		if (bIsMachine)
		{
			ButtonText->SetText(StartGameText);
			Player->UpdateMiniGameState(false);
			Player->StartMovement();
			ResourceImage->SetBrushFromTexture(MachineMiniGameIn);
		}
		else
		{
			ButtonText->SetText(StartSlipwayGameText);
			Player->UpdateSlipwayMiniGState(false);
			Player->StartMovement();
			ResourceImage->SetBrushFromTexture(SlipwayMiniGameIn);
		}
	}
	else
	{
		InGame = true;
		if (bIsMachine)
		{
			Player->StopMovement();
			Player->UpdateMiniGameState(true);
			ButtonText->SetText(CloseButtonText);
			ResourceImage->SetBrushFromTexture(MachineMiniGameOut);
		}
		else
		{
			ButtonText->SetText(CloseSlipwayGameText);
			Player->UpdateSlipwayMiniGState(true);
			Player->StopMovement();
			ResourceImage->SetBrushFromTexture(SlipwayMiniGameOut);
		}
		
	}
}

void USK_MiniGame::SetSlipwayMGEnd()
{
	ButtonText->SetText(StartSlipwayGameText);
	ResourceImage->SetBrushFromTexture(SlipwayMiniGameIn);
}

void USK_MiniGame::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	SetVisibility(ESlateVisibility::HitTestInvisible);

	if (WorkButton)
	{
		WorkButton->OnClicked.AddDynamic(this, &USK_MiniGame::StartWork);
	}
}


void USK_MiniGame::ShowSlipwayMiniGameButton()
{
	SetVisibility(ESlateVisibility::Visible);
	bIsMachine = false;

	ResourceImage->SetBrushFromTexture(SlipwayMiniGameIn);
}

void USK_MiniGame::HideSlipwayMiniGameButton()
{
	SetVisibility(ESlateVisibility::Collapsed);
}