// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SK_MiniGame.generated.h"

class UImage;
class UButton;
class UTextBlock;

UCLASS()
class SHIPYARDKEEPER_API USK_MiniGame : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void Init(const FName& ResourceName);

	void ShowWidget();
	void HideWidget();

	void ShowSlipwayMiniGameButton();
	void HideSlipwayMiniGameButton();

	UFUNCTION()
	void StartWork();

	void SetSlipwayMGEnd();
protected:
	virtual void NativeOnInitialized() override;

	bool InGame{ false };

	bool bIsMachine{ true };
protected:
	UPROPERTY(meta = (BindWidget))
	UImage* ResourceImage;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* ButtonText;

	UPROPERTY(meta = (BindWidget))
	UButton* WorkButton;

	UPROPERTY(EditDefaultsOnly,Category = "Text")
	FText CloseButtonText;

	UPROPERTY(EditDefaultsOnly, Category = "Text")
	FText StartGameText;

	UPROPERTY(EditDefaultsOnly, Category = "Text")
	FText StartSlipwayGameText;

	UPROPERTY(EditDefaultsOnly, Category = "Text")
	FText CloseSlipwayGameText;

	UPROPERTY(EditDefaultsOnly,Category = "Button")
	TObjectPtr<UTexture2D> MachineMiniGameIn;

	UPROPERTY(EditDefaultsOnly, Category = "Button")
	TObjectPtr<UTexture2D> MachineMiniGameOut;

	UPROPERTY(EditDefaultsOnly, Category = "Button")
	TObjectPtr<UTexture2D> SlipwayMiniGameIn;

	UPROPERTY(EditDefaultsOnly, Category = "Button")
	TObjectPtr<UTexture2D> SlipwayMiniGameOut;
};
