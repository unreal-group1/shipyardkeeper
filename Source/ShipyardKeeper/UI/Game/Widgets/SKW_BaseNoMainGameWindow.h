// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "SKW_Base.h"
#include "SKW_BaseNoMainGameWindow.generated.h"

class UCanvasPanel;
class UImage;
class USKW_BaseElementMap;
class USKW_Button;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_BaseNoMainGameWindow : public USKW_Base
{
	GENERATED_BODY()

public:
	UPROPERTY(meta = (BindWidget))
	UCanvasPanel* MainCanvas;

	UPROPERTY(meta = (BindWidget))
	UImage* Background;

	UPROPERTY(meta = (BindWidget))
	USKW_Button* GameButton;

	UFUNCTION()
	virtual void NativeOnInitialized() override;
	UFUNCTION()
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;
	virtual void Hide() override;
};
