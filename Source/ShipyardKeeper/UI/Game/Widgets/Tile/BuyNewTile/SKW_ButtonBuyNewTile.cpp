// Created by Pavlov P.A.

#include "SKW_ButtonBuyNewTile.h"

#include "SK_GameHUD.h"

#include "Kismet/GameplayStatics.h"

void USKW_ButtonBuyNewTile::Do_Implementation()
{
    ASK_GameHUD* GameHUD = Cast<ASK_GameHUD>(GetHUD());
    if (!GameHUD)
    {
        GameHUD = Cast<ASK_GameHUD>(UGameplayStatics::GetActorOfClass(GetWorld(),
            ASK_GameHUD::StaticClass()));
    }

    if (GameHUD)
    {
        SetVisibility(ESlateVisibility::Hidden);

        if (CurrentTilePosition.CurrentTile)
        {
            OnBuyNewTile.Broadcast(CurrentTilePosition);
        }

        GameHUD->SetNextWidget(EWidgetOnScreen::BuyTile);
    }
}

void USKW_ButtonBuyNewTile::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
    Super::OnAnimationFinished_Implementation(Animation);

    if (Animation == HideAnimation)
    {

    }
}

void USKW_ButtonBuyNewTile::SetTilePosition(FTilePositionData NewTilePosition)
{
    CurrentTilePosition = NewTilePosition;
}
