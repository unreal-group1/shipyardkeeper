// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"

#include "SK_TileActorBase.h"
#include "SKW_PopupWithButton.h"
#include "SKW_ButtonBuyNewTile.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBuyNewTile, 
	FTilePositionData, 
	TilePosition);
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_ButtonBuyNewTile : public USKW_PopupWithButton
{
	GENERATED_BODY()
	
public:
	FOnBuyNewTile OnBuyNewTile;
	void Do_Implementation() override;

	UFUNCTION()
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;
	UFUNCTION()
	void SetTilePosition(FTilePositionData NewTilePosition);

private:
	FTilePositionData CurrentTilePosition;
};
