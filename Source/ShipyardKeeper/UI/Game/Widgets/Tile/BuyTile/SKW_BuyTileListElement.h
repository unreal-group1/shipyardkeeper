// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Components/HorizontalBox.h"

#include "SKW_ListElement.h"
#include "SKW_BuyTileListElement.generated.h"

class ASK_MachineTile;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_BuyTileListElement : public USKW_ListElement
{
	GENERATED_BODY()
	
public:
	UPROPERTY()
	TArray<class USKW_PopupWithImageAndText*> ResourceElements;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<class USKW_PopupWithImageAndText> ResourceViewClass;

	UPROPERTY(meta = (BindWidget))
	UHorizontalBox* ResourcesBox;

	void SetMachineName(FName NewMachineName);
	FName GetMachineName();

	void SetMachineTile(ASK_MachineTile* NewMachineTile);
	ASK_MachineTile* GetMachineTile();

	virtual void Do() override;
	virtual void NativeConstruct() override;
	void HideButtons();

private:
	UPROPERTY()
	ASK_MachineTile* MachineTile;
	UPROPERTY()
	FName MachineName;
};
