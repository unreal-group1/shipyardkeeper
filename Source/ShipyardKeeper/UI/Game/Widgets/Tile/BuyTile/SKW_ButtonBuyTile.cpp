// Created by Pavlov P.A.

#include "SKW_ButtonBuyTile.h"
#include "SKW_BuyTile.h"

#include "SK_MachineTile.h"

#include "SK_GameHUD.h"
#include "Kismet/GameplayStatics.h"

void USKW_ButtonBuyTile::Do_Implementation()
{
    ASK_GameHUD* GameHUD = Cast<ASK_GameHUD>(GetHUD());
    if (!GameHUD)
    {
        GameHUD = Cast<ASK_GameHUD>(UGameplayStatics::GetActorOfClass(GetWorld(),
            ASK_GameHUD::StaticClass()));
    }

    SetVisibility(ESlateVisibility::Hidden);

    if (GameHUD)
    {
        GameHUD->SetNextWidget(EWidgetOnScreen::BuyTile);

        auto BuyTileWidget = Cast<USKW_BuyTile>(GameHUD->GetCurrentWidget());
        if (BuyTileWidget)
        {
            auto CurrentMachineTile = Cast<ASK_MachineTile>(GetOwner());
            if (CurrentMachineTile)
            {
                BuyTileWidget->SetMachineTile(CurrentMachineTile);
            }
        }
    }
}

void USKW_ButtonBuyTile::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
    Super::OnAnimationFinished_Implementation(Animation);

    if (Animation == HideAnimation)
    {

    }
}