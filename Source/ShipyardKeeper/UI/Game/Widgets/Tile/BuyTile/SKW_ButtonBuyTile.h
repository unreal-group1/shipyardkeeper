// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"

#include "SK_TileActorBase.h"
#include "SKW_PopupWithButton.h"
#include "SKW_ButtonBuyTile.generated.h"
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_ButtonBuyTile : public USKW_PopupWithButton
{
	GENERATED_BODY()
	
public:
	void Do_Implementation() override;

	UFUNCTION()
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;
};
