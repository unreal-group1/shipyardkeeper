// Created by Pavlov P.A.

#include "SKW_Resource.h"

#include "SKI_ResourceContaining.h"
#include "SKI_HasImage.h"
#include "SKI_HasText.h"

#include "SK_PlatformGameInstance.h"

#include "Components/Image.h"

USKW_Resource::USKW_Resource()
{
}

void USKW_Resource::NativeConstruct()
{
    if (!PlatformGameInstance)
    {
        if (GetGameInstance())
        {
            PlatformGameInstance = Cast<USK_PlatformGameInstance>(GetGameInstance());
        }
    }

    if (PlatformGameInstance)
    {
        PlatformGameInstance->OnResourceCountChange.AddDynamic(this, &USKW_Resource::UpdateResourceQuantity);
    }

    Show();
}

void USKW_Resource::SetResourceQuantity(double NewQuantity)
{
    Quantity = NewQuantity;

    SetText(FText::FromString(FString::Printf(TEXT("%.0f"), NewQuantity)));

    if (NewQuantity <= 0)
    {
        Hide();
    }
    else
    if (GetVisibility() != ESlateVisibility::SelfHitTestInvisible)
    {
        Show();
    }
}

void USKW_Resource::SetResourceType(FResourceType NewResourceType)
{
    ResourceType = NewResourceType;

    UImage* NewImage = NewObject<UImage>(UImage::StaticClass());
    if (NewImage)
    {
        NewImage->SetBrushFromTexture(ResourceType.ShowingData.Image, true);

        SetImage(NewImage);
    }
}

void USKW_Resource::UpdateResourceQuantity(FName ResourceName, double NewQuantity)
{
    if (ResourceType.ShowingData.RowName.EqualTo(FText::FromName(ResourceName)))
    {
        SetResourceQuantity(NewQuantity);
        SetText(FText::FromString(FString::Printf(TEXT("%.0f"), NewQuantity)));
    }
}
