// Created by Pavlov P.A.

#include "SKW_BuyTile.h"
#include "SKW_BuyTileListElement.h"
#include "SKW_Button.h"
#include "SKW_Resource.h"
#include "SKW_Game.h"

#include "SKI_HasImage.h"
#include "SKI_HasText.h"

#include "SK_ShipBuilder.h"
#include "SK_GameMode.h"
#include "SK_PlatformGameInstance.h"
#include "SK_MachineTile.h"
#include "SK_GameHUD.h"

void USKW_BuyTile::Hide()
{
	Super::Hide();

	HideButtons();
}

void USKW_BuyTile::NativeOnInitialized()
{
	Super::NativeOnInitialized();
}

void USKW_BuyTile::NativePreConstruct()
{
	Super::NativePreConstruct();
}

void USKW_BuyTile::NativeConstruct()
{
	Super::NativeConstruct();

	CreateList();
}

void USKW_BuyTile::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
	Super::OnAnimationFinished_Implementation(Animation);
}

void USKW_BuyTile::CreateList()
{
	Super::CreateList();

	const auto& World = GetWorld();
	if (!World) return;

	auto GI = Cast<USK_PlatformGameInstance>(GetGameInstance());
	if (!GI) return;

	auto MachineTypes = GI->GetMachineTypesWithOriginalNames();
	for (auto MachineType : MachineTypes)
	{
		if (BuyTileListElementClass)
		{
			auto ListElement = CreateWidget<USKW_BuyTileListElement>(World, BuyTileListElementClass);
			if (ListElement)
			{
				ListElementsBox->AddChildToVerticalBox(ListElement);
				BuyTileListElements.Add(ListElement);

				auto HasImageObject = Cast<ISKI_HasImage>(ListElement);
				if (HasImageObject)
				{
					HasImageObject->SetImageFromTexture(MachineType->ShowingData.Image);
				}

				auto HasTextObject = Cast<ISKI_HasText>(ListElement);
				if (HasTextObject)
				{
					HasTextObject->SetText(MachineType->ShowingData.PrintName);
				}

				ListElement->SetMachineName(FName(MachineType->MachineName.ToString()));
				ListElement->SetParentWidget(this);
				ListElement->SetHUD(GetHUD());

				IsFirstElement = true;
				CreateResourceElement(MachineType->ResourceInRowName,
					ListElement);
				IsFirstElement = false;
				CreateTransitionArrowElement(TransitionArrow,
					ListElement);
				CreateResourceElement(MachineType->ResourceOutRowName,
					ListElement);
			}
		}
	}
}

void USKW_BuyTile::CreateResourceElement(FName ResourceName, 
	USKW_BuyTileListElement* BuyTileListElement)
{
	const auto& World = GetWorld();
	if (!World) return;

	auto GI = Cast<USK_PlatformGameInstance>(GetGameInstance());
	if (!GI) return;

	auto ResourceViewElement = CreateWidget<USKW_Resource>(World, BuyTileListElement->ResourceViewClass);
	if (ResourceViewElement)
	{
		if (BuyTileListElement->ResourcesBox)
		{
			BuyTileListElement->ResourcesBox->AddChildToHorizontalBox(ResourceViewElement);
		}
		BuyTileListElement->ResourceElements.Add(ResourceViewElement);
	}

	if (!ResourceName.IsNone())
	{
		auto ResourceType = GI->GetResourceTypeByName(ResourceName);
		if (ResourceType)
		{
			auto HasImageObject = Cast<ISKI_HasImage>(ResourceViewElement);
			if (HasImageObject)
			{
				HasImageObject->SetImageFromTexture(ResourceType->ShowingData.Image);
			}
		}
	}

	UpdateDistanceBetweenResourceElements(ResourceViewElement,
		DistanceBetweenResourceElements);

	ResourceViewElement->Show();
}

void USKW_BuyTile::CreateTransitionArrowElement(UTexture2D* TransitionArrowImage,
	USKW_BuyTileListElement* BuyTileListElement)
{
	const auto& World = GetWorld();
	if (!World) return;

	auto GI = Cast<USK_PlatformGameInstance>(GetGameInstance());
	if (!GI) return;

	if (!TransitionArrowImage) return;

	auto ResourceViewElement = CreateWidget<USKW_Resource>(World, BuyTileListElement->ResourceViewClass);
	if (BuyTileListElement->ResourcesBox)
	{
		BuyTileListElement->ResourcesBox->AddChildToHorizontalBox(ResourceViewElement);
	}
	BuyTileListElement->ResourceElements.Add(ResourceViewElement);

	auto HasImageObject = Cast<ISKI_HasImage>(ResourceViewElement);
	if (HasImageObject)
	{
		HasImageObject->SetImageFromTexture(TransitionArrowImage);
	}

	UpdateDistanceBetweenResourceElements(ResourceViewElement,
		DistanceBetweenResourceElements);

	ResourceViewElement->Show();
}

void USKW_BuyTile::SetMachineTile(ASK_MachineTile* NewMachineTile)
{
	if (NewMachineTile)
	{
		MachineTile = NewMachineTile;

		for (auto BuyTileListElement : BuyTileListElements)
		{
			BuyTileListElement->SetMachineTile(MachineTile);
		}
	}
}

ASK_MachineTile* USKW_BuyTile::GetMachineTile()
{
	return MachineTile;
}

void USKW_BuyTile::HideButtons()
{
	//������ ������ ��� ��������
	auto GameHUD = Cast<ASK_GameHUD>(GetHUD());
	if (GameHUD)
	{
		auto GameWidget = Cast<USKW_Game>(GameHUD->GetGameWidget());
		if (GameWidget)
		{
			GameWidget->CloseUpgradeAndBuild();
		}
	}
}

void USKW_BuyTile::UpdateDistanceBetweenResourceElements(USKW_Resource* CurrentResource,
	float NewDistanceBetweenResourceElements)
{
	auto CurrentPadding = CurrentResource->GetPadding();
	if (IsFirstElement)
	{
		CurrentPadding.Left = 0;
	}
	else
	{
		CurrentPadding.Left = DistanceBetweenResourceElements;
	}
	CurrentResource->SetPadding(CurrentPadding);
}
