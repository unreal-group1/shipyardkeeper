// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Components/VerticalBox.h"

#include "SKW_WithListAndCurrentResources.h"
#include "SKW_BuyTile.generated.h"

class ASK_MachineTile;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_BuyTile : public USKW_WithListAndCurrentResources
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<class USKW_BuyTileListElement> BuyTileListElementClass;
	UPROPERTY(meta = (BindWidget))
	UVerticalBox* ListElementsBox;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	UTexture2D* TransitionArrow;

	virtual void Hide() override;
	virtual void NativeOnInitialized() override;
	virtual void NativePreConstruct() override;
	virtual void NativeConstruct() override;
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;
	virtual void CreateList() override;
	void CreateResourceElement(FName ResourceName,
		USKW_BuyTileListElement* BuyTileListElement);
	void CreateTransitionArrowElement(UTexture2D* TransitionArrowImage,
		USKW_BuyTileListElement* BuyTileListElement);

	void SetMachineTile(ASK_MachineTile* NewMachineTile);
	ASK_MachineTile* GetMachineTile();

	void HideButtons();
private:
	TArray<class USKW_BuyTileListElement*> BuyTileListElements;
	UPROPERTY()
	ASK_MachineTile* MachineTile;

#pragma region DistanceBetweenElements
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	float DistanceBetweenResourceElements = -35.0f;

	UFUNCTION()
	void UpdateDistanceBetweenResourceElements(USKW_Resource* CurrentResource,
		float NewDistanceBetweenResourceElements);

	bool IsFirstElement;
#pragma endregion DistanceBetweenElements
};
