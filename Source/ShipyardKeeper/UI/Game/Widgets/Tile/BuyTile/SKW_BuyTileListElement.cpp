// Created by Pavlov P.A.

#include "SKW_BuyTileListElement.h"
#include "SKW_BaseNoMainGameWindow.h"
#include "SKW_Game.h"

#include "SK_MachineTile.h"
#include "SK_GameMode.h"
#include "SK_PlatformGameInstance.h"
#include "SK_Types.h"
#include "SK_GameHUD.h"

void USKW_BuyTileListElement::SetMachineName(FName NewMachineName)
{
    auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
    if (GM)
    {
        auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
        if (GI)
        {
            FMachineType MachineType; 
            if (GI->GetMachineBuyName(NewMachineName, MachineType))
            {
                MachineName = FName(MachineType.MachineName.ToString());
            }
        }
    }
}

FName USKW_BuyTileListElement::GetMachineName()
{
	return MachineName;
}

void USKW_BuyTileListElement::SetMachineTile(ASK_MachineTile* NewMachineTile)
{
    if (NewMachineTile)
    {
        MachineTile = NewMachineTile;
    }
}

ASK_MachineTile* USKW_BuyTileListElement::GetMachineTile()
{
    return MachineTile;
}

void USKW_BuyTileListElement::Do()
{
    if (MachineTile)
    {
        MachineTile->SpawnMachine(MachineName);
        HideButtons();
    }
}

void USKW_BuyTileListElement::NativeConstruct()
{
    Super::NativeConstruct();
}

void USKW_BuyTileListElement::HideButtons()
{
    if (ParentWidget)
    {
        //������ ���� � ����������� ������ ������
        ParentWidget->Hide();
    }
    //������ ������ ��� ��������
    auto GameHUD = Cast<ASK_GameHUD>(GetHUD());
    if (GameHUD)
    {
        auto GameWidget = Cast<USKW_Game>(GameHUD->GetGameWidget());
        if (GameWidget)
        {
            GameWidget->CloseUpgradeAndBuild();
        }
    }
}
