// Created by Pavlov P.A.

#include "SKW_ButtonUpgradeMachine.h"

#include "SK_MachineTile.h"
#include "SK_PlatformGameInstance.h"

void USKW_ButtonUpgradeMachine::Do_Implementation()
{
	const auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	FMachineType Machine;
	if (GI)
	{
		if (GI->GetMachineBuyName(MachineTile->GetMachineTypeName(), Machine))
		{
			MachineTile->LevelUp();
		}
	}
}

void USKW_ButtonUpgradeMachine::SetMachineTile(ASK_MachineTile* NewMachineTile)
{
	if (NewMachineTile)
	{
		MachineTile = NewMachineTile;
	}
}

void USKW_ButtonUpgradeMachine::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
	Super::OnAnimationFinished_Implementation(Animation);
}
