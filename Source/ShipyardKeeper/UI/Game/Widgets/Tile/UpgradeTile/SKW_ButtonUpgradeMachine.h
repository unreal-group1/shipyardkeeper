// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"

#include "SKW_PopupWithButton.h"
#include "SKW_ButtonUpgradeMachine.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_ButtonUpgradeMachine : public USKW_PopupWithButton
{
	GENERATED_BODY()
	
public:
	void Do_Implementation() override;

	UFUNCTION()
	void SetMachineTile(ASK_MachineTile* NewMachineTile);

	UFUNCTION()
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;
private:
	ASK_MachineTile* MachineTile;
};
