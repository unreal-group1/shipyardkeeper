// Created by Pavlov P.A.

#include "SKW_DestroyMachineButton.h"
#include "SKW_Game.h"
#include "SKW_TwoButtonsWindow.h"

#include "SK_GameHUD.h"
#include "SK_MachineTile.h"
#include "SK_PlatformGameInstance.h"
#include "SK_PopupHintWidgetComponent.h"

#include "SK_MachineTile.h"
#include "Kismet/GameplayStatics.h"

void USKW_DestroyMachineButton::Do_Implementation()
{
	//if (GetOwner())
	//{
	//	auto CurrentOwner = Cast<ASK_MachineTile>(GetOwner());

	//	if (CurrentOwner)
	//	{
	//		auto AcceptWindow = Cast<USK_PopupHintWidgetComponent>(CurrentOwner->AcceptWindowComponent);
	//		if (AcceptWindow)
	//		{
	//			AcceptWindow->Show();
	//		}
	//	}
	//}
	DestroyTile();
}

void USKW_DestroyMachineButton::DestroyTile()
{
	const auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
	FMachineType Machine;
	if (GI)
	{
		if (GI->GetMachineBuyName(MachineTile->GetMachineTypeName(), Machine))
		{
			MachineTile->ClearMachines();
		}
	}

	ASK_GameHUD* GameHUD = Cast<ASK_GameHUD>(GetHUD());
	if (!GameHUD)
	{
		GameHUD = Cast<ASK_GameHUD>(UGameplayStatics::GetActorOfClass(GetWorld(),
			ASK_GameHUD::StaticClass()));
	}

	if (GameHUD)
	{
		auto Widget = Cast<USKW_Game>(GameHUD->GetCurrentWidget());
		Widget->CloseUpgradeAndBuild();
	}
}

void USKW_DestroyMachineButton::SetMachineTile(ASK_MachineTile* NewMachineTile)
{
	if (NewMachineTile)
	{
		MachineTile = NewMachineTile;
	}
}

ASK_MachineTile* USKW_DestroyMachineButton::GetMachineTile()
{
	return MachineTile;
}

void USKW_DestroyMachineButton::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
	Super::OnAnimationFinished_Implementation(Animation);
}


void USKW_DestroyMachineButton::SetPosition(FVector NewPosition)
{
	CurrentPosition = NewPosition;
}