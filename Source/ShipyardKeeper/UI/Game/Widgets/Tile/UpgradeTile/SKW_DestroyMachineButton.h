// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"

#include "SKW_PopupWithButton.h"
#include "SKW_DestroyMachineButton.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_DestroyMachineButton : public USKW_PopupWithButton
{
	GENERATED_BODY()
	
public:
	void Do_Implementation() override;

	UFUNCTION()
	void DestroyTile();

	UFUNCTION()
	void SetMachineTile(class ASK_MachineTile* NewMachineTile);
	UFUNCTION()
	ASK_MachineTile* GetMachineTile();
	UFUNCTION()
	void SetPosition(FVector NewPosition);

	UFUNCTION()
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;
private:
	class ASK_MachineTile* MachineTile;
	FVector CurrentPosition;
};
