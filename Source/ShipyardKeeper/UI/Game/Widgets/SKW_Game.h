// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "SK_GameHUD.h"
#include "SKW_Base.h"
#include "SKW_Game.generated.h"

class USK_PlatformGameInstance;
class USKW_Button;
class USK_MiniGame;
class USizeBox;
class USK_TaskWidget;

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_Game : public USKW_Base
{
	GENERATED_BODY()
#pragma region Tasks
protected:
	UPROPERTY(meta = (BindWidget))
	USizeBox* TasksBox;

	UPROPERTY(EditDefaultsOnly,Category = "WidgetClasses")
	TSubclassOf<USK_TaskWidget> TaskWidgetClass;

	UPROPERTY()
	TObjectPtr<USK_TaskWidget> TaskWidget;

public:
	void CreateTaskWidget();

	const TObjectPtr<USK_TaskWidget>& GetTaskWidget() { return TaskWidget; }
#pragma endregion Tasks
#pragma region WorkButton
protected:
	UPROPERTY(EditDefaultsOnly,Category = "Content")
	TSubclassOf<USK_MiniGame>  MiniGameClass;

	UPROPERTY(meta = (BindWidget))
	USizeBox* WorkButtonBox;

	UPROPERTY()
	TObjectPtr<USK_MiniGame> MiniGameWidget;
public:
	const TObjectPtr<USK_MiniGame>& GetMiniGameWidget() const { return MiniGameWidget; }

	void CreateMiniGameWidget();


#pragma endregion WorkButton
#pragma region GlobalMapButton
public:
	UPROPERTY(meta = (BindWidget))
	USKW_Button* GlobalMapButton;

	UFUNCTION()
	void OpenGlobalMap();
#pragma endregion GlobalMapButton

#pragma region UpgradeButton
public:
	UPROPERTY(meta = (BindWidget))
	USKW_Button* UpgradeButton;

	UFUNCTION()
	void OpenUpgradeAndBuild();
	UFUNCTION()
	void CloseUpgradeAndBuild();
private:
	bool bUpgradeAndBuildShowing = false;
#pragma endregion UpgradeButton

#pragma region BuyWorker
public:
	UPROPERTY(meta = (BindWidget))
	USKW_Button* BuyWorkerButton;

	UFUNCTION()
	void BuyWorker();

	float LastPrice{ -1.f };
#pragma endregion BuyWorker

#pragma region SelectNextShip
public:
	UPROPERTY(EditDefaultsOnly,Category = "Content")
	TSubclassOf<USKW_Button> SelectNextShipButtonClass;

	UPROPERTY(EditDefaultsOnly,Category = "Text")
	FText ShipButtonText;

	UPROPERTY(meta = (BindWidget))
	USizeBox* ShipButtonBox;

	UPROPERTY()
	TObjectPtr<USKW_Button> SelectNextShipButton;

	const TObjectPtr<USKW_Button>& GetNextShipButton() { return SelectNextShipButton;}

	void ShowShipButton();
	void HideShipButton();

	UFUNCTION()
	void OpenSelectNextShip();
#pragma endregion SelectNextShip

#pragma region Currency
public:
	UPROPERTY(meta = (BindWidget))
	class USKW_Currency* Currency;

	UFUNCTION()
	void AddAmountCurrency(int32 NewAmountCurrency);
	UFUNCTION()
	void SubAmountCurrency(int32 NewAmountCurrency);
	UFUNCTION()
	void SetAmountCurrency(uint32 NewAmountCurrency);
private:
	uint32 OldAmountCurrency;
#pragma endregion Currency
public:
	UFUNCTION()
	virtual void NativeOnInitialized() override;

	UFUNCTION()
	virtual void NativeConstruct() override;
	UFUNCTION()
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;

private:
	UPROPERTY()
	EWidgetOnScreen NextWidget = EWidgetOnScreen::GlobalMap;
	UPROPERTY()
	TObjectPtr<USK_PlatformGameInstance> PlatformGameInstance;
	UPROPERTY()
	TObjectPtr<class ASK_NPSSpawner> NPSSpawner;

	void CheckPlatformGameInstance();
};
