// Created by Pavlov P.A.

#include "SKW_Game.h"
#include "SKW_Button.h"
#include "SKW_Currency.h"

#include "SK_MachineTile.h"
#include "SK_PlatformGameInstance.h"
#include "SK_PopupHintWidgetComponent.h"
#include "UI/Game/Widgets/MiniGames/SK_MiniGame.h"
#include "Components/SizeBox.h"
#include "SK_GameMode.h"

#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

#include "UI/Widgets/SK_TaskWidget.h"

void USKW_Game::CreateTaskWidget()
{
    if (TasksBox)
    {
        TaskWidget = CreateWidget<USK_TaskWidget>(this, TaskWidgetClass);
        if (TaskWidget)
        {
            TasksBox->ClearChildren();
            TasksBox->AddChild(TaskWidget);
        }
    }
}

void USKW_Game::CreateMiniGameWidget()
{
    if (WorkButtonBox && MiniGameClass)
    {
        WorkButtonBox->ClearChildren();

        MiniGameWidget = CreateWidget<USK_MiniGame>(this, MiniGameClass);
        if (MiniGameWidget)
        {
            MiniGameWidget->HideWidget();
            WorkButtonBox->AddChild(MiniGameWidget);
        }
    }
}

void USKW_Game::OpenGlobalMap()
{
    NextWidget = EWidgetOnScreen::GlobalMap;
    Hide();
}

void USKW_Game::OpenUpgradeAndBuild()
{
    CheckPlatformGameInstance();

    if (PlatformGameInstance)
    {
        auto MachineTiles = PlatformGameInstance->GetMachineTiles();
        if (MachineTiles.Num() > 0)
        {
            for (auto MachineTile : MachineTiles)
            {
                if (MachineTile)
                {
                    auto CurrentMachineTile = Cast<ASK_MachineTile>(MachineTile);
                    if (CurrentMachineTile)
                    {
                        if (!bUpgradeAndBuildShowing)
                        {
                            CurrentMachineTile->Show();
                        }
                        else
                        {
                            CurrentMachineTile->Hide();
                        }
                    }
                }
            }
        }

        bUpgradeAndBuildShowing = !bUpgradeAndBuildShowing;
    }
}

void USKW_Game::CloseUpgradeAndBuild()
{
    CheckPlatformGameInstance();

    if (PlatformGameInstance)
    {
        auto MachineTiles = PlatformGameInstance->GetMachineTiles();
        if (MachineTiles.Num() > 0)
        {
            for (auto MachineTile : MachineTiles)
            {
                if (MachineTile)
                {
                    auto CurrentMachineTile = Cast<ASK_MachineTile>(MachineTile);
                    if (CurrentMachineTile)
                    {
                        CurrentMachineTile->Hide();
                    }
                }
            }
        }

        bUpgradeAndBuildShowing = false;
    }
}

void USKW_Game::BuyWorker()
{
    CheckPlatformGameInstance();

    if (!NPSSpawner)
    {
        NPSSpawner = PlatformGameInstance->GetNPSSpawner();
    }

    if (NPSSpawner
        && PlatformGameInstance)
    {
        auto CurrentSpawnPrice = PlatformGameInstance->GetCurrentSpawnPrice();
        if (CurrentSpawnPrice > 0)
        {
            if (PlatformGameInstance->TryToBuy(CurrentSpawnPrice))
            {
                PlatformGameInstance->SetNewIndexSpawnPrice();

                if (BuyWorkerButton)
                {
                    const float Price = PlatformGameInstance->GetNextPrice();
                    if (Price > 0)
                    {
                        LastPrice = Price;
                        FString ButtonText = FString::Printf(TEXT("%.0f"), Price);
                        BuyWorkerButton->SetText(ButtonText);
                    }
                    else
                    {
                        FString ButtonText = FString::Printf(TEXT("%.0f"), LastPrice);
                        BuyWorkerButton->SetText(ButtonText);
                    }
                    
                }
                NPSSpawner->SpawnNPS();
            }
        }
        else
        {
            BuyWorkerButton->SetText(TEXT("MAX"));
        }
    }
}

void USKW_Game::ShowShipButton()
{
    SelectNextShipButton->SetText(ShipButtonText.ToString());
    GetNextShipButton()->SetVisibility(ESlateVisibility::Visible);
}

void USKW_Game::HideShipButton()
{
    GetNextShipButton()->SetVisibility(ESlateVisibility::Collapsed);
}

void USKW_Game::OpenSelectNextShip()
{
    NextWidget = EWidgetOnScreen::SelectNextShip;
    Hide();
}

void USKW_Game::AddAmountCurrency(int32 NewAmountCurrency)
{
    if (Currency)
    {
        Currency->AddCurency();
        Currency->SetText(FText::FromString(FString::Printf(TEXT("%i"), NewAmountCurrency)));
    }
}

void USKW_Game::SubAmountCurrency(int32 NewAmountCurrency)
{
    if (Currency)
    {
        Currency->SubCurrency();
        Currency->SetText(FText::FromString(FString::Printf(TEXT("%i"), NewAmountCurrency)));
    }
}

void USKW_Game::SetAmountCurrency(uint32 NewAmountCurrency)
{
    if (Currency)
    {
        Currency->SetText(FText::FromString(FString::Printf(TEXT("%i"), NewAmountCurrency)));
    }
}

void USKW_Game::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (GlobalMapButton
        && GlobalMapButton->CustomizeButton)
    {
        GlobalMapButton->CustomizeButton->OnClicked.AddDynamic(this, 
            &USKW_Game::OpenGlobalMap);
    }

    if (UpgradeButton
        && UpgradeButton->CustomizeButton)
    {
        UpgradeButton->CustomizeButton->OnClicked.AddDynamic(this,
            &USKW_Game::OpenUpgradeAndBuild);
    }

    if (BuyWorkerButton
        && BuyWorkerButton->CustomizeButton)
    {
        BuyWorkerButton->CustomizeButton->OnClicked.AddDynamic(this,
            &USKW_Game::BuyWorker);
    }

    if(SelectNextShipButtonClass)
    {
        SelectNextShipButton = CreateWidget<USKW_Button>(this, SelectNextShipButtonClass);

        if (ShipButtonBox && SelectNextShipButton)
        {
            ShipButtonBox->ClearChildren();
            ShipButtonBox->AddChild(SelectNextShipButton);
            SelectNextShipButton->SetVisibility(ESlateVisibility::Collapsed);

            if (SelectNextShipButton->CustomizeButton)
            {
                SelectNextShipButton->CustomizeButton->OnClicked.AddDynamic(this,
                    &USKW_Game::OpenSelectNextShip);
            }
        }
    }

    CreateMiniGameWidget();
    if (Currency)
    {
        if (GetWorld())
        {
            auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
            if (GI)
            {
                GI->OnAddAmountCurrencyChange.AddDynamic(this, &USKW_Game::AddAmountCurrency);
                GI->OnSubAmountCurrencyChange.AddDynamic(this, &USKW_Game::SubAmountCurrency);
                SetAmountCurrency(GI->GetAmountCurrency());
            }
        }
        Currency->Show();
    }
}

void USKW_Game::NativeConstruct()
{
    CheckPlatformGameInstance();
    auto CurrentSpawnPrice = PlatformGameInstance->GetCurrentSpawnPrice();
    if (BuyWorkerButton)
    {
        FString ButtonText = FString::Printf(TEXT("%.0f"), CurrentSpawnPrice);
        BuyWorkerButton->SetText(ButtonText);
    }
}

void USKW_Game::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
    Super::OnAnimationFinished_Implementation(Animation);

    if (Animation == HideAnimation)
    {
        auto GameHUD = Cast<ASK_GameHUD>(GetHUD());
        if (GameHUD)
        {
            SetVisibility(ESlateVisibility::Hidden);

            GameHUD->SetNextWidget(NextWidget);
        }
    }
}

void USKW_Game::CheckPlatformGameInstance()
{
    if (!PlatformGameInstance)
    {
        if (GetGameInstance())
        {
            PlatformGameInstance = Cast<USK_PlatformGameInstance>(GetGameInstance());
        }
    }

    check(PlatformGameInstance);
}

