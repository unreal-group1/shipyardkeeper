// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "SKW_Base.h"
#include "SKW_OneTask.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_OneTask : public USKW_Base
{
	GENERATED_BODY()
	
};
