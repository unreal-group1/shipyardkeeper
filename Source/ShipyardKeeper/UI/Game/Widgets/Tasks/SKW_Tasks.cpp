// Created by Pavlov P.A.

#include "SKW_Tasks.h"
#include "SKW_Button.h"
#include "SKW_OneTask.h"
#include "SK_GameHUD.h"

#include "Components/CanvasPanel.h"

void USKW_Tasks::NativeOnInitialized()
{
    Super::NativeOnInitialized();
}

void USKW_Tasks::NativePreConstruct()
{
    Super::NativePreConstruct();

    if (MainCanvas)
    {
        if (Tasks.Num() > 0)
        {
            for (auto Task : Tasks)
            {
                if (Task)
                {
                    MainCanvas->AddChildToCanvas(Task);
                    Task->Show();
                }
            }
        }
    }
}

void USKW_Tasks::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
    Super::OnAnimationFinished_Implementation(Animation);
}

void USKW_Tasks::CreateList()
{
}
