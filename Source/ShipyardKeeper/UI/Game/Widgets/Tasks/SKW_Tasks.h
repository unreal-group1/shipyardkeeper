// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"

#include "SKW_WindowWithList.h"
#include "SKW_Tasks.generated.h"

class USKW_OneTask;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_Tasks : public USKW_WindowWithList
{
	GENERATED_BODY()
	
public:
	UPROPERTY(meta = (BindWidget))
	TArray<USKW_OneTask*> Tasks;

	virtual void NativeOnInitialized() override;
	virtual void NativePreConstruct() override;
	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;
	virtual void CreateList() override;
};
