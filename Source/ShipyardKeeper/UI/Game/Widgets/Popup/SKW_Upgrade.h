// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "SKW_PopupWithButton.h"
#include "SKW_Upgrade.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_Upgrade : public USKW_PopupWithButton
{
	GENERATED_BODY()
	
};
