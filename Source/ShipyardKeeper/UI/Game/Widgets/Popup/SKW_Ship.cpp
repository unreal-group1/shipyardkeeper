// Created by Pavlov P.A.

#include "SKW_Ship.h"

#include "SKI_HasImage.h"

#include "SK_PopupHintWidgetComponent.h"
#include "SK_ClickableAndTouchableActor.h"
#include "SK_PlatformGameInstance.h"
#include "SK_ShipBuilder.h"
#include "SK_GameMode.h"
#include "SK_Types.h"
#include "SK_GameMode.h"
#include "SK_ShipBuilder.h"

void USKW_Ship::Show()
{
    if (GetIsSlipwayNotBlocked())
    {
        UpdateImage();
        Super::Show();
    }
}

void USKW_Ship::Hide()
{
    Super::Hide();
}

void USKW_Ship::NativeConstruct()
{
	Super::NativeConstruct();

    if (GetWorld())
    {
        auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
        if (GM)
        {
            ShipBuilder = GM->GetShipBuilder();
            if (ShipBuilder)
            {
                if (!ShipBuilder->OnEnduranceChange.IsAlreadyBound(this, &USKW_Ship::UpdateEndurancePercent))
                {
                    ShipBuilder->OnEnduranceChange.AddDynamic(this,
                        &USKW_Ship::UpdateEndurancePercent);
                }
            }
        }
    }
    Hide();
    UpdateImage();

    IsSlipwayNotBlocked = false;
}

void USKW_Ship::UpdateEndurancePercent(double Current, double Max)
{
    if (GetIsSlipwayNotBlocked())
    {
        if (Current != 0
            && Max != 0)
        {
            Percent = Current / Max;

            if (Percent <= MinPercentThreshold)
            {
                SetPercent(Percent);
                Hide();
            }
            else
            if (Percent < MaxPercentThreshold)
            {
                SetPercent(Percent);
                Show();
            }
            else
            if (Percent >= MaxPercentThreshold)
            {
                Hide();
            }
            UE_LOG(LogTemp, Error, TEXT("Ship done on %f percent"), Percent)
        }
        else
        {
            Hide();
        }
    }
}

void USKW_Ship::UpdateImage()
{
    auto CurrentOwner = GetOwner();
    if (CurrentOwner)
    {
        auto HasImageObject = Cast<ISKI_HasImage>(CurrentOwner->PopupHintWidgetComponent->GetWidget());
        if (HasImageObject)
        {
            const auto GM = GetWorld()->GetAuthGameMode<ASK_GameMode>();
            if (GM)
            {
                auto SM = GM->GetShipBuilder();
                if (SM)
                {
                    auto ShipName = SM->GetCurrentShipName();
                    auto GI = Cast<USK_PlatformGameInstance>(GetGameInstance());
                    if (GI)
                    {
                        FShipTypes ShipType;
                        auto IsFindShip = GI->GetShipsBuyName(ShipName, ShipType);
                        if (IsFindShip)
                        {
                            HasImageObject->SetImageFromTexture(ShipType.ShowingData.Image);
                        }
                    }
                }
            }
        }
    }
}

void USKW_Ship::SetIsSlipwayNotBlocked(bool NewIsSlipwayNotBlocked)
{
    IsSlipwayNotBlocked = NewIsSlipwayNotBlocked;
}

bool USKW_Ship::GetIsSlipwayNotBlocked()
{
    return IsSlipwayNotBlocked;
}
