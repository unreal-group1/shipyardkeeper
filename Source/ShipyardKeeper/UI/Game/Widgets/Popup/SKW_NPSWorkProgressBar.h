// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"

#include "SK_Types.h"

#include "SKI_ResourceContaining.h"

#include "SKW_PopupWithProgressBarSimulate.h"
#include "SKW_NPSWorkProgressBar.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_NPSWorkProgressBar : public USKW_PopupWithProgressBarSimulate,
	public ISKI_ResourceContaining
{
	GENERATED_BODY()
	
public:
	virtual void SetResourceQuantity(double NewQuantity) override;
	virtual void SetResourceType(FResourceType NewResourceType) override;
	virtual void UpdateResourceQuantity(FName NewResourceName, 
		double NewQuantity) override;

	virtual void NativeConstruct() override;

private:
	FResourceType ResourceType;
	double Quantity = 0.0f;
};
