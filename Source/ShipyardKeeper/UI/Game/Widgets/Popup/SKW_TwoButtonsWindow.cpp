// Created by Pavlov P.A.

#include "SKW_TwoButtonsWindow.h"
#include "SKW_DestroyMachineButton.h"

void USKW_TwoButtonsWindow::Hide()
{
	Super::Hide();
}

void USKW_TwoButtonsWindow::Show()
{
	Super::Show();
}

void USKW_TwoButtonsWindow::NativeConstruct()
{
	Super::NativeConstruct();
}

void USKW_TwoButtonsWindow::DoYes_Implementation()
{
	OwnerWidget->DestroyTile();
}

void USKW_TwoButtonsWindow::DoNo_Implementation()
{
	Hide();
}

void USKW_TwoButtonsWindow::SetOwnerWidget(USKW_DestroyMachineButton* NewOwnerWidget)
{
	if (NewOwnerWidget)
	{
		OwnerWidget = NewOwnerWidget;
	}
}
