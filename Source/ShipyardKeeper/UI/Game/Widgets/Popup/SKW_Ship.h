// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "SKW_PopupWithProgressbar.h"
#include "SKW_Ship.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_Ship : public USKW_PopupWithProgressbar
{
	GENERATED_BODY()
	
public:
	virtual void Show() override;
	virtual void Hide() override;
	virtual void NativeConstruct() override;
	UFUNCTION()
	void UpdateEndurancePercent(double Current, double Max);
	UFUNCTION()
	void UpdateImage();

	UFUNCTION()
	void SetIsSlipwayNotBlocked(bool NewIsSlipwayNotBlocked);
	UFUNCTION()
	bool GetIsSlipwayNotBlocked();

private:
	TObjectPtr<class USK_ShipBuilder> ShipBuilder;
	double Percent;
	const double MinPercentThreshold = 0.01f;
	const double MaxPercentThreshold = 0.99f;
	
	bool IsSlipwayNotBlocked = false;
};
