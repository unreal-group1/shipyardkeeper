// Created by Pavlov P.A.

#include "SKW_NPSWorkProgressBar.h"

#include "SK_PopupHintWidgetComponent.h"
#include "SK_PlatformGameInstance.h"

#include "SKI_HasImage.h"
#include "SKI_HasText.h"

#include "Components/Image.h"

void USKW_NPSWorkProgressBar::SetResourceQuantity(double NewQuantity)
{
	Quantity = NewQuantity;
}

void USKW_NPSWorkProgressBar::SetResourceType(FResourceType NewResourceType)
{
	ResourceType = NewResourceType;

    //�������� �������� � ������
    UImage* NewImage = NewObject<UImage>(UImage::StaticClass());
    NewImage->SetBrushFromTexture(ResourceType.ShowingData.Image, true);

    SetImage(NewImage);
}

void USKW_NPSWorkProgressBar::UpdateResourceQuantity(FName NewResourceName,
    double NewQuantity)
{
    if (ResourceType.ShowingData.RowName.EqualTo(FText::FromName(NewResourceName)))
    {
        SetResourceQuantity(NewQuantity);
    }
}

void USKW_NPSWorkProgressBar::NativeConstruct()
{
    Super::NativeConstruct();
}
