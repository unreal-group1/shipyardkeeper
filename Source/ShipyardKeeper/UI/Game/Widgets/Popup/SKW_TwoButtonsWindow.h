// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "SKW_BasePopup.h"
#include "SKW_TwoButtonsWindow.generated.h"

class USKW_Button;
class USKW_DestroyMachineButton;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_TwoButtonsWindow : public USKW_BasePopup
{
	GENERATED_BODY()
	
public:
	UPROPERTY(meta = (BindWidget))
	USKW_Button* YesButton;

	UPROPERTY(meta = (BindWidget))
	USKW_Button* NoButton;

	virtual void Hide() override;
	virtual void Show() override;
	virtual void NativeConstruct() override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DoYes();
	UFUNCTION()
	virtual void DoYes_Implementation();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DoNo();
	UFUNCTION()
	virtual void DoNo_Implementation();

	UFUNCTION()
	void SetOwnerWidget(USKW_DestroyMachineButton* NewOwnerWidget);

private:
	UPROPERTY()
	USKW_DestroyMachineButton* OwnerWidget;
	FVector CurrentPosition;
	class ASK_MachineTile* MachineTile;
};
