// Created by Pavlov P.A.

#include "SKW_GetResourceProgressBar.h"

void USKW_GetResourceProgressBar::NativeConstruct()
{
	if (!World)
	{
		if (GetWorld())
		{
			World = GetWorld();
		}
	}
}

void USKW_GetResourceProgressBar::SetTimeSimulate(float NewTimeSimulate)
{
	if (NewTimeSimulate > 0)
	{
		EndSimulateTime = NewTimeSimulate;
	}
	else
	{
		EndSimulateTime = DefaultSimulateTime;
	}
}

void USKW_GetResourceProgressBar::ResetTimeSimulate()
{
	CurrentSimulateTime = 0;
}

void USKW_GetResourceProgressBar::StartSimulate()
{
	if (World)
	{
		if (!World->GetTimerManager().IsTimerActive(SimulateProgressHandle))
		{
			World->GetTimerManager().SetTimer(SimulateProgressHandle,
				this,
				&USKW_GetResourceProgressBar::TickSimulate,
				TickRate,
				true);
		}
	}
	CurrentSimulateTime = 0;
}

void USKW_GetResourceProgressBar::StopSimulate()
{
	if (World)
	{
		World->GetTimerManager().ClearTimer(SimulateProgressHandle);
	}

	CurrentSimulateTime = 0;
}

void USKW_GetResourceProgressBar::TickSimulate()
{
	CurrentSimulateTime += TickRate;
	if (CurrentSimulateTime > EndSimulateTime)
	{
		CurrentSimulateTime = EndSimulateTime;
	}

	SetPercent(CurrentSimulateTime / EndSimulateTime);
}
