// Created by Pavlov P.A.

#include "SKW_BaseDisplayImageAndText.h"
#include "SKW_Text.h"

#include "Components\Image.h"

void USKW_BaseDisplayImageAndText::SetImage(UImage* NewImage)
{
	if (DisplayedImage)
	{
		DisplayedImage->SetBrush(NewImage->GetBrush());
	}
}

void USKW_BaseDisplayImageAndText::SetImageFromTexture(UTexture2D* NewImage)
{
	if (DisplayedImage)
	{
		DisplayedImage->SetBrushFromTexture(NewImage, true);
	}
}

void USKW_BaseDisplayImageAndText::SetText(FText NewText)
{
	DisplayedText->SetText(NewText.ToString());
}
