// Created by Pavlov P.A.

#include "SKW_PopupWithButton.h"
#include "SKW_Button.h"

#include "Components/Button.h"
#include "Styling/SlateTypes.h"
#include "Components/Image.h"

void USKW_PopupWithButton::NativeConstruct()
{
	if (PopupButton
		&& PopupButton->CustomizeButton)
	{
		PopupButton->CustomizeButton->OnClicked.AddDynamic(this, 
			&USKW_PopupWithButton::Do_Implementation);
	}
}

void USKW_PopupWithButton::SetImage(UImage* NewImage)
{
	if (PopupButton
		&& PopupButton->CustomizeButton)
	{
		FButtonStyle ButtonStyle;
		ButtonStyle.SetNormal(NewImage->GetBrush());

		PopupButton->CustomizeButton->WidgetStyle = ButtonStyle;
	}
}

void USKW_PopupWithButton::SetText(FText NewText)
{
	if (PopupButton)
	{
		PopupButton->SetText(NewText.ToString());
	}
}

void USKW_PopupWithButton::Do_Implementation()
{
	OnPurchased.Broadcast();
}
