// Created by Pavlov P.A.

#include "SKW_BaseClickablePopup.h"
#include "SK_ClickableAndTouchableActor.h"

void USKW_BaseClickablePopup::Clicked_Implementation(UPrimitiveComponent* TouchedActor,
	FKey ButtonPressed)
{
	if (GetVisibility() == ESlateVisibility::Hidden)
	{
		Show();
	}
}

void USKW_BaseClickablePopup::Released_Implementation(UPrimitiveComponent* TouchedActor,
	FKey ButtonPressed)
{

}

void USKW_BaseClickablePopup::InputTouchBegin_Implementation(ETouchIndex::Type FingerIndex,
	UPrimitiveComponent* TouchedComponent)
{
	if (GetVisibility() == ESlateVisibility::Hidden)
	{
		Show();
	}
}

void USKW_BaseClickablePopup::InputTouchEnd_Implementation(ETouchIndex::Type FingerIndex,
	UPrimitiveComponent* TouchedComponent)
{

}

void USKW_BaseClickablePopup::SetOwner(ASK_ClickableAndTouchableActor* NewOwner)
{
	Owner = NewOwner;

	if (Owner
		&& Owner->StaticMeshComponent)
	{
		Owner->StaticMeshComponent->OnClicked.AddDynamic(this, 
			&USKW_BaseClickablePopup::Clicked_Implementation);
		Owner->StaticMeshComponent->OnReleased.AddDynamic(this, 
			&USKW_BaseClickablePopup::Released_Implementation);
		Owner->StaticMeshComponent->OnInputTouchEnter.AddDynamic(this, 
			&USKW_BaseClickablePopup::InputTouchBegin_Implementation);
		Owner->StaticMeshComponent->OnInputTouchLeave.AddDynamic(this, 
			&USKW_BaseClickablePopup::InputTouchEnd_Implementation);
	}
}