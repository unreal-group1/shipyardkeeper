// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"

#include "SKI_HasText.h"

#include "SKW_BasePopup.h"
#include "SKW_BaseDisplayText.generated.h"

class USKW_Text;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_BaseDisplayText : public USKW_BasePopup
{
	GENERATED_BODY()
	
	public:
	UPROPERTY(meta = (BindWidget))
	USKW_Text* DisplayedText;

	UFUNCTION()
	virtual void SetText(FText NewText) override;
};
