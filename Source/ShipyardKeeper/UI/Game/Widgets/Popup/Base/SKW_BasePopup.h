// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "SKI_HasText.h"
#include "SKI_HasImage.h"
#include "SKI_HasProgressBar.h"
#include "SKI_ActorClick.h"
#include "SKW_Base.h"
#include "SKW_BasePopup.generated.h"

class  ASK_ClickableAndTouchableActor;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_BasePopup : public USKW_Base,
	public ISKI_HasImage,
	public ISKI_HasText,
	public ISKI_HasProgressBar
{
	GENERATED_BODY()
	
protected:
	ASK_ClickableAndTouchableActor* Owner;
public:
	UFUNCTION()
	virtual bool Initialize() override;

	UFUNCTION()
	ASK_ClickableAndTouchableActor* GetOwner();
	UFUNCTION()
	virtual void SetOwner(ASK_ClickableAndTouchableActor* NewOwner);

	virtual void SetImage(class UImage* NewImage) override;
	virtual void SetImageFromTexture(UTexture2D* NewImage) override;
	virtual void SetText(FText NewText) override;
	virtual void SetPercent(float NewPecent) override;
};
