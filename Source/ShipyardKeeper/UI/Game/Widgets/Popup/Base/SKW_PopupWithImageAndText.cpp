// Created by Pavlov P.A.

#include "SKW_PopupWithImageAndText.h"
#include "SKW_Text.h"

#include "Components/Image.h"

bool USKW_PopupWithImageAndText::Initialize()
{
	return Super::Initialize();
}

void USKW_PopupWithImageAndText::SetImage(UImage* NewImage)
{
	if (PopupImage
		&& NewImage)
	{
		PopupImage->SetBrush(NewImage->GetBrush());
	}
}

void USKW_PopupWithImageAndText::SetImageFromTexture(UTexture2D* NewImage)
{
	if (PopupImage
		&& NewImage)
	{
		PopupImage->SetBrushFromTexture(NewImage);
	}
}

void USKW_PopupWithImageAndText::SetText(FText NewText)
{
	if (PopupText)
	{
		PopupText->SetText(NewText.ToString());
	}
}
