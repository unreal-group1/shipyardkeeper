// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "SKW_BasePopup.h"
#include "SKW_BaseClickablePopup.generated.h"

class ASK_ClickableAndTouchableActor;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_BaseClickablePopup : public USKW_BasePopup,
	public ISKI_ActorClick
{
	GENERATED_BODY()

	public:
	UFUNCTION()
	void Clicked_Implementation(UPrimitiveComponent* TouchedActor,
		FKey ButtonPressed) override;
	UFUNCTION()
	void Released_Implementation(UPrimitiveComponent* TouchedActor,
		FKey ButtonPressed) override;
	UFUNCTION()
	void InputTouchBegin_Implementation(ETouchIndex::Type FingerIndex,
		UPrimitiveComponent* TouchedComponent) override;
	UFUNCTION()
	void InputTouchEnd_Implementation(ETouchIndex::Type FingerIndex,
		UPrimitiveComponent* TouchedComponent) override;

	virtual void SetOwner(ASK_ClickableAndTouchableActor* NewOwner) override;
};
