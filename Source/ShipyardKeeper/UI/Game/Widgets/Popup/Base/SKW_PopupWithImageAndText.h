// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "SKW_BasePopup.h"
#include "SKW_PopupWithImageAndText.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_PopupWithImageAndText : public USKW_BasePopup
{
	GENERATED_BODY()
	
public:
	UPROPERTY(meta = (BindWidget))
	class UImage* PopupImage;
	UPROPERTY(meta = (BindWidget))
	class USKW_Text* PopupText;

	virtual bool Initialize() override;
	virtual void SetImage(UImage* NewImage) override;
	virtual void SetImageFromTexture(UTexture2D* NewImage) override;
	virtual void SetText(FText NewText) override;
};
