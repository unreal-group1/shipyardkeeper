// Created by Pavlov P.A.

#include "SKW_PopupWithProgressBarSimulate.h"

void USKW_PopupWithProgressBarSimulate::NativeConstruct()
{
	if (!World)
	{
		if (GetWorld())
		{
			World = GetWorld();
		}
	}
}

void USKW_PopupWithProgressBarSimulate::SetTimeSimulate(float NewTimeSimulate)
{
	if (NewTimeSimulate > 0)
	{
		EndSimulateTime = NewTimeSimulate;
	}
	else
	{
		EndSimulateTime = DefaultSimulateTime;
	}
}

void USKW_PopupWithProgressBarSimulate::StartSimulate()
{
	if (World)
	{
		World->GetTimerManager().SetTimer(SimulateProgressHandle,
			this,
			&USKW_PopupWithProgressBarSimulate::TickSimulate,
			TickRate,
			true);
	}
	CurrentSimulateTime = 0;
}

void USKW_PopupWithProgressBarSimulate::StopSimulate()
{
	if (World)
	{
		World->GetTimerManager().ClearTimer(SimulateProgressHandle);
	}

	CurrentSimulateTime = 0;
}

void USKW_PopupWithProgressBarSimulate::TickSimulate()
{
	CurrentSimulateTime += TickRate;
	if (CurrentSimulateTime > EndSimulateTime)
	{
		CurrentSimulateTime = EndSimulateTime;
	}

	SetPercent(CurrentSimulateTime / EndSimulateTime);
}
