// Created by Pavlov P.A.

#include "SKW_BasePopup.h"
#include "SK_ClickableAndTouchableActor.h"

#include "Components/Image.h"

bool USKW_BasePopup::Initialize()
{
	auto Result = Super::Initialize();

	SetVisibility(ESlateVisibility::Hidden);
	
	return Result;
}

ASK_ClickableAndTouchableActor* USKW_BasePopup::GetOwner()
{
	return Owner;
}

void USKW_BasePopup::SetOwner(ASK_ClickableAndTouchableActor* NewOwner)
{
	Owner = NewOwner;
}

void USKW_BasePopup::SetImage(UImage* NewImage)
{
}

void USKW_BasePopup::SetImageFromTexture(UTexture2D* NewImage)
{
}

void USKW_BasePopup::SetText(FText NewText)
{
}

void USKW_BasePopup::SetPercent(float NewPecent)
{
}
