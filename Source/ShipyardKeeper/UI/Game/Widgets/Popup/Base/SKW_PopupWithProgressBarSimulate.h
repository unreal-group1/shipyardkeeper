// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "SKW_PopupWithProgressBar.h"
#include "SKW_PopupWithProgressBarSimulate.generated.h"

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_PopupWithProgressBarSimulate : public USKW_PopupWithProgressbar
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;

	UFUNCTION()
	void SetTimeSimulate(float NewTimeSimulate);
	UFUNCTION()
	void StartSimulate();
	UFUNCTION()
	void StopSimulate();
	UFUNCTION()
	void TickSimulate();
private:
	FTimerHandle SimulateProgressHandle;
	float TickRate = 0.1f;
	float CurrentSimulateTime = 0.0f;
	float EndSimulateTime = 1.0f;
	float DefaultSimulateTime = 1.0f;
	UWorld* World;
};
