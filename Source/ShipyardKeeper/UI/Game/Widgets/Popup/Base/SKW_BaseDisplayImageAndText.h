// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"

#include "SKI_HasImage.h"

#include "SKW_BaseDisplayText.h"
#include "SKW_BaseDisplayImageAndText.generated.h"

class UImage;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_BaseDisplayImageAndText : public USKW_BaseDisplayText
{
	GENERATED_BODY()

	public:
	UPROPERTY(meta = (BindWidget))
	UImage* DisplayedImage;

	virtual void SetImage(UImage* NewImage) override;
	virtual void SetImageFromTexture(UTexture2D* NewImage) override;
	virtual void SetText(FText NewText) override;
	
};
