// Created by Pavlov P.A.

#include "SKW_PopupWithProgressbar.h"

#include "Components/ProgressBar.h"
#include "Components/Image.h"
#include "Materials/Material.h"
#include "Materials/MaterialInstanceDynamic.h"

void USKW_PopupWithProgressbar::NativeConstruct()
{
	Super::NativeConstruct();

    if (ProgressBarImage)
    {
        ProgressBarImage->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
    }

    if (ImageInProgressBar)
    {
        ImageInProgressBar->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
    }

    TryCreateProgressBarMaterial(M_ProgressBar);

    SetPercent(0);
}

void USKW_PopupWithProgressbar::SetImage(UImage* NewImage)
{
    if (!ImageInProgressBar)
    {
        return;
    }

    ImageInProgressBar->SetBrush(NewImage->GetBrush());
}

void USKW_PopupWithProgressbar::SetImageFromTexture(UTexture2D* NewImage)
{
    if (!ImageInProgressBar)
    {
        return;
    }

    ImageInProgressBar->SetBrushFromTexture(NewImage, true);
}


void USKW_PopupWithProgressbar::SetText(FText NewText)
{

}

void USKW_PopupWithProgressbar::SetPercent(float Percent)
{
    TryCreateProgressBarMaterial(M_ProgressBar);

    if (MI_ProgressBar)
    {
        MI_ProgressBar->SetScalarParameterValue("Percent", Percent);
    }
}

void USKW_PopupWithProgressbar::TryCreateProgressBarMaterial(UMaterialInterface* NewMaterialProgressBar)
{
    if (!MI_ProgressBar)
    {
        if (M_ProgressBar)
        {
            MI_ProgressBar = UMaterialInstanceDynamic::Create(M_ProgressBar, this);
            if (ProgressBarImage)
            {
                ProgressBarImage->SetBrushFromMaterial(MI_ProgressBar);
            }
        }
    }
}
