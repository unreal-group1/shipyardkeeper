// Created by Pavlov P.A.

#include "SKW_BaseDisplayText.h"
#include "SKW_Text.h"

void USKW_BaseDisplayText::SetText(FText NewText)
{
    if (!NewText.IsEmpty())
    {
        DisplayedText->SetText(NewText.ToString());
    }
}
