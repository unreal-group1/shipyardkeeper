// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "SKW_BasePopup.h"
#include "SKW_PopupWithProgressbar.generated.h"

class UProgressBar;
class UImage;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_PopupWithProgressbar : public USKW_BasePopup
{
	GENERATED_BODY()
	
	public:
	UPROPERTY(meta = (BindWidget))
	UImage* ProgressBarImage;

	UPROPERTY(meta = (BindWidget))
	UImage* ImageInProgressBar;

	virtual void NativeConstruct() override;

	virtual void SetImage(UImage* NewImage) override;
	virtual void SetImageFromTexture(UTexture2D* NewImage) override;
	virtual void SetText(FText NewText) override;
	virtual void SetPercent(float Percent) override;

	void TryCreateProgressBarMaterial(UMaterialInterface* NewMaterialProgressBar);

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = ProgerssBar)
	UMaterialInterface* M_ProgressBar = nullptr;
private:
	UPROPERTY()
	UMaterialInstanceDynamic* MI_ProgressBar = nullptr;
};
