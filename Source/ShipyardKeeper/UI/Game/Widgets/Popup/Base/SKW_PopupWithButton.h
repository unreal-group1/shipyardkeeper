// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"

#include "SKW_BasePopup.h"
#include "SKW_PopupWithButton.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPurchased);

class UImage;
class USKW_Button;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_PopupWithButton : public USKW_BasePopup
{
	GENERATED_BODY()
	
	public:
	FOnPurchased OnPurchased;

	UPROPERTY(meta = (BindWidget))
	USKW_Button* PopupButton;

	virtual void NativeConstruct() override;

	virtual void SetImage(UImage* NewImage) override;
	virtual void SetText(FText NewText) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Do();
	UFUNCTION()
	virtual void Do_Implementation();
};
