// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Widgets/Slipway/SK_UpSlipwayWidget.h"
#include "Components/Image.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "SK_GameMode.h"

void USK_UpSlipwayWidget::SetPrice(const int32 Price)
{
	if (PriceText)
	{
		PriceText->SetText(FText::FromString(FString::FromInt(Price)));
	}
}

void USK_UpSlipwayWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();


}
