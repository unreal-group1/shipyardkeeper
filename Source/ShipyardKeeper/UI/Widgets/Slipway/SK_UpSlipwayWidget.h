// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SK_UpSlipwayWidget.generated.h"

class UImage;
class UButton;
class UTextBlock;

UCLASS()
class SHIPYARDKEEPER_API USK_UpSlipwayWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void SetPrice(const int32 Price);
protected:
	UPROPERTY(meta = (BindWidget))
	UImage* ButtonImage;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* PriceText;

protected:
	virtual void NativeOnInitialized() override;

};
