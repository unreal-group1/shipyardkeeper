// Created by Pavlov P.A.

#include "SKW_Text.h"

#include "Components/TextBlock.h"

void USKW_Text::SetText(FString NewText)
{
	if (NewText.IsEmpty())
	{
		NewText = " ";
	}
	if (CustomizeTextBlock)
	{
		CustomizeTextBlock->SetText(FText::FromString(NewText));
	}
}

void USKW_Text::NativePreConstruct()
{
	SetText(Text);
}
