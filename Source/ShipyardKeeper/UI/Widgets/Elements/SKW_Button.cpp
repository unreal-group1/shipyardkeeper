// Created by Pavlov P.A.

#include "SKW_Button.h"
#include "SKW_Text.h"

#include "Components/Button.h"

void USKW_Button::NativeOnInitialized()
{
	if (CustomizeButton)
	{
		CustomizeButton->AddChild(CustomizeText);
	}
}

void USKW_Button::NativePreConstruct()
{
	if (CustomizeText)
	{
		CustomizeText->SetText(TextButton);
	}
}

void USKW_Button::SetText(FString NewText)
{
	if (CustomizeText)
	{
		CustomizeText->SetText(NewText);
	}
}

