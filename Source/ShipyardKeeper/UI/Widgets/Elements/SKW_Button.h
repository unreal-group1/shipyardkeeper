// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "SKW_Button.generated.h"

class USKW_Text;
class UButton;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_Button : public UUserWidget
{
	GENERATED_BODY()
	
	public:
	UPROPERTY(meta = (BindWidget))
	UButton* CustomizeButton;

	UPROPERTY(meta = (BindWidget))
	USKW_Text* CustomizeText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Button")
	FString TextButton;

	virtual void NativeOnInitialized() override;
	virtual void NativePreConstruct() override;
	void SetText(FString NewText);
};
