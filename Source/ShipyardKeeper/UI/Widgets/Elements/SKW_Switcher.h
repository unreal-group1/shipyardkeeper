// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "SKW_Switcher.generated.h"

class USKW_Button;
class USKW_Text;
/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_Switcher : public UUserWidget
{
	GENERATED_BODY()

	public:
	UPROPERTY(meta = (BindWidget))
	USKW_Button* PreviousElementButton;

	UPROPERTY(meta = (BindWidget))
	USKW_Text* Element;

	UPROPERTY(meta = (BindWidget))
	USKW_Button* NextElementButton;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TextBlock")
	FString PreviousElementButtonText;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TextBlock")
	FString ElementText;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TextBlock")
	FString NextElementButtonText;

	virtual void NativePreConstruct() override;
};
