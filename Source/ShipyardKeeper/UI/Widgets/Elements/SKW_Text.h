// Created by Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "SKW_Text.generated.h"

class UTextBlock;

/**
 * 
 */
UCLASS()
class SHIPYARDKEEPER_API USKW_Text : public UUserWidget
{
	GENERATED_BODY()
	
	public:
	UPROPERTY(meta = (BindWidget))
	UTextBlock* CustomizeTextBlock;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TextBlock")
	FString Text;

	UFUNCTION()
	void SetText(FString NewText);
	virtual void NativePreConstruct() override;
};
