// Created by Pavlov P.A.

#include "SKW_Switcher.h"
#include "SKW_Text.h"
#include "SKW_Button.h"

void USKW_Switcher::NativePreConstruct()
{
	if (PreviousElementButton)
	{
		PreviousElementButton->SetText(PreviousElementButtonText);
	}
	if (Element)
	{
		Element->SetText(ElementText);
	}
	if (NextElementButton)
	{
		NextElementButton->SetText(NextElementButtonText);
	}
}
