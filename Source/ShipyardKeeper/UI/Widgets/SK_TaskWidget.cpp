// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Widgets/SK_TaskWidget.h"
#include "Components/TextBlock.h"
#include "SK_PlatformGameInstance.h"
#include "Components/Button.h"
#include "Actors/Components/SK_TaskComponent.h"

DEFINE_LOG_CATEGORY_STATIC(USK_TaskWidgetLog, All, All);

void USK_TaskWidget::TaskCompleted(const int32 Reward)
{
	if (RewardTextBlock)
	{
		TaskTextBlock->SetText(AwardText);
		RewardTextBlock->SetText(FText::FromString(FString::Printf(TEXT(" %i"), Reward)));
		bIsCompleted = true;
		CurrentReward = Reward;
	}
	PlayAnimationForward(CompleteAnim);
}

void USK_TaskWidget::SetNewTask(const int32 Value, const FText& TaskText, const FText& ObjectText)
{
	MaxValue = Value;

	if (TaskTextBlock && ObjectTextBlock && TextBlock)
	{
		TaskTextBlock->SetText(TaskText);
		FString NewString = ObjectText.ToString() + FString::Printf(TEXT(" 0/%i"), Value);
		ObjectTextBlock->SetText(FText::FromString(NewString));
	}
}

void USK_TaskWidget::ObjectTextUpdate(const int32 InMaxValue, const int32 NewValue, const FText& ObjectText, const FText& TaskText)
{
	if (ObjectTextBlock && TaskTextBlock)
	{
		TaskTextBlock->SetText(TaskText);
		FString NewString = ObjectText.ToString() + FString::Printf(TEXT(" %i/%i"), NewValue, InMaxValue);
		ObjectTextBlock->SetText(FText::FromString(NewString));
	}
}

void USK_TaskWidget::ShowTextF(const FText& Text)
{
	TextBlock->SetText(Text);
	PlayAnimationForward(ShowText);
}

void USK_TaskWidget::ShowTextR()
{
	PlayAnimationReverse(ShowText);
}

void USK_TaskWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	WidgetAnimationDynamicEvent.BindDynamic(this, &USK_TaskWidget::OnCompleteAnimFinished);

	BindToAnimationFinished(CompleteAnim, WidgetAnimationDynamicEvent);

	if (CompleteButton)
	{
		CompleteButton->OnClicked.AddDynamic(this, &USK_TaskWidget::OnCliked);
	}
}

void USK_TaskWidget::OnCompleteAnimFinished()
{
	UE_LOG(USK_TaskWidgetLog, Error, TEXT("OnCompleteAnimFinished"));
	//
}

void USK_TaskWidget::OnCliked()
{
	if (!bIsCompleted) return;

	if (GetWorld())
	{
		const auto GI = GetWorld()->GetGameInstance<USK_PlatformGameInstance>();
		GI->AddToBank(CurrentReward);

		bIsCompleted = false;

		PlayAnimationForward(Reverse);
		TaskComponent->TrySetNextTask();
	}
}
