// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SK_TaskWidget.generated.h"

class UTextBlock;
class UWidgetAnimation;
class UButton;
class USK_TaskComponent;

UCLASS()
class SHIPYARDKEEPER_API USK_TaskWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void TaskCompleted(const int32 Reward);
	void SetNewTask(const int32 Value,const FText& TaskText,const FText& ObjectText);

	void ObjectTextUpdate(const int32 MaxValue, const int32 NewValue, const FText& ObjectText, const FText& TaskText);

	void SetComponent(USK_TaskComponent* Component) { TaskComponent = Component; }

	void ShowTextF(const FText& Text);
	void ShowTextR();
protected:

	virtual void NativeOnInitialized() override;

	FWidgetAnimationDynamicEvent WidgetAnimationDynamicEvent;

	UPROPERTY(meta = (BindWidgetAnim), Transient)
	UWidgetAnimation* CompleteAnim;

	UPROPERTY(meta = (BindWidgetAnim), Transient)
	UWidgetAnimation* Reverse;

	UPROPERTY(meta = (BindWidgetAnim), Transient)
	UWidgetAnimation* ShowText;

	UFUNCTION()
	void OnCompleteAnimFinished();

	UFUNCTION()
	void OnCliked();
	UPROPERTY()
	TObjectPtr<USK_TaskComponent> TaskComponent;

	UPROPERTY(EditDefaultsOnly,Category = "Text")
	FText AwardText;
protected:
	UPROPERTY(meta =(BindWidget))
	UTextBlock* TaskTextBlock;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* ObjectTextBlock;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* RewardTextBlock;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* TextBlock;

	UPROPERTY(meta = (BindWidget))
	UButton* CompleteButton;

	int32 CurrentReward{ 0 };

	bool bIsCompleted{ false };
private:
	int32 MaxValue{ 0 };
};
