// Created by Pavlov P.A.

#include "SKW_Base.h"

#include "Kismet/GameplayStatics.h"

void USKW_Base::Show()
{
	if (GetVisibility() == ESlateVisibility::Hidden)
	{
		SetVisibility(ESlateVisibility::SelfHitTestInvisible);

		if (ShowAnimation)
		{
			PlayAnimation(ShowAnimation);
		}

		if (GetWorld()
			&& ShowSound)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), 
				ShowSound);
		}
	}
}

void USKW_Base::Hide()
{
	if (GetVisibility() != ESlateVisibility::Hidden)
	{
		if (HideAnimation)
		{
			PlayAnimation(HideAnimation);
		}

		if (GetWorld()
			&& HideSound)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), 
				HideSound);
		}
	}
}

void USKW_Base::SetHUD(AHUD* NewHUD)
{
	HUD = NewHUD;
}

AHUD* USKW_Base::GetHUD()
{
	return HUD;
}

void USKW_Base::NativeOnInitialized()
{
	Super::NativeOnInitialized();
}

void USKW_Base::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
	if (!Animation)
	{
		UE_LOG(LogTemp, Error, TEXT("USKW_Base: Animation is null"));
		return;
	}

	if (Animation == HideAnimation)
	{
		SetVisibility(ESlateVisibility::Hidden);
	}
}